<?php

###########################DB Settings
$GLOBALS['db_host'] = 'localhost';
$GLOBALS['db_user'] = 'root';
$GLOBALS['db_pass'] = 'root';
$GLOBALS['db_name'] = 'amazonbot';

###########################Site Settings
$GLOBALS['site']['title'] = 'Amazon Bots';
$GLOBALS['site']['url'] = 'http://localhost:8888/';
$GLOBALS['site']['redirect'] = 'http://localhost:8888/';
$GLOBALS['site']['defaultpage'] = 'products';
$GLOBALS['site']['signature'] = "<br><br>Thanks,<br>{$GLOBALS['site']['title']} team";


##########################System Settings
$debug = false;
$debug = true;
if($debug){
	error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT & ~E_DEPRECATED);
	ini_set('display_errors', '1');		
}

$GLOBALS['system']['path'] 					= '/vagrant/src/';
$GLOBALS['system']['js_path'] 				= 'js';
$GLOBALS['system']['template_path'] 		= 'template';
$GLOBALS['system']['email_template_path'] 	= $GLOBALS['system']['path'].$GLOBALS['system']['template_path'].'/emails';
$GLOBALS['system']['script_path'] 			= 'php';
$GLOBALS['system']['href_base'] 			= '/site/';
$GLOBALS['system']['lib_path'] 				= $GLOBALS['system']['path'].'php/lib';
$GLOBALS['system']['util_path'] 			= 'php/util';
$GLOBALS['system']['helper_path'] 			= 'php/helpers';
$GLOBALS['system']['cache_path'] 			= 'cache';
$GLOBALS['system']['tmp_path']				= $GLOBALS['system']['path'] .'tmp';
$GLOBALS['system']['upload_path']			= $GLOBALS['system']['path'] .'uploads';
$GLOBALS['system']['upload_href']			= $GLOBALS['system']['href_base'] .'uploads';
$GLOBALS['system']['public_path'] 			= 'public';
$GLOBALS['system']['images_path']			= $GLOBALS['system']['path'] .'snapshots/';
$GLOBALS['system']['images_href']			= $GLOBALS['system']['href_base'] .'snapshots/';
$GLOBALS['system']['phantom_path']			= $GLOBALS['system']['path'] .'phantomjs/';


$GLOBALS['system']['font_path']				= $GLOBALS['system']['path'] .'fonts';

##########################Emails Settings
$GLOBALS['emails']['from'] 					= $GLOBALS['site']['title'];
$GLOBALS['emails']['from_email'] 			= '';
$GLOBALS['emails']['smtp_host'] 			= '';
$GLOBALS['emails']['smtp_username'] 		= '';
$GLOBALS['emails']['smtp_password'] 		= '';
$GLOBALS['emails']['admins']				= array('gontham.inc@gmail.com');
$GLOBALS['emails']['defaultTemplate']		= $GLOBALS['system']['email_template_path'] . '/default.phtml'; 

##########################Widgets
$GLOBALS['widgets']['js_path'] 				= 'js/widgets/';
$GLOBALS['widgets']['js_href'] 				= '../'.$GLOBALS['widgets']['js_path'];
$GLOBALS['widgets']['script_path']		 	= $GLOBALS['system']['script_path'].'/widgets';
$GLOBALS['widgets']['template_path'] 		= $GLOBALS['system']['template_path'].'/widgets';

##########################Page Settings
$GLOBALS['pages']['public']					= array('login','resetpassword');

##########################Admin Settings
$GLOBALS['superusers'] = array(1);
$GLOBALS['SETTINGS'] = array();


##########################Admin Settings
$GLOBALS['SETTINGS']['amazon_aws_key'] = 'AKIAIS4VROTI4D246OKA';
$GLOBALS['SETTINGS']['amazon_aws_secret'] = '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';
$GLOBALS['SETTINGS']['amazon_associate_tag'] = 'renewed0a-20';

