var testindex = 0;
var wait = false;
var loadInProgress = false;//This is set to true when a page is still loading
 
/*********SETTINGS*********************/
phantom.injectJs('./jquery.min.js');
var webPage = require('webpage');
var page = webPage.create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = false;//Script is much faster with this field set to false
page.settings.resourceTimeout = 60*1000;
//page.customHeaders={'Proxy-Authorization': 'Basic '+btoa('amazonbot:a8Ei8fTl9a6!')};
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;

var server = 'http://107.170.234.124/util/server';
var snapshotDir = '/var/www/html/snapshots/';
var state = {};
var debug = {
	request: false,
	response: false,
	pause: false,
	msgs: false,
	nosleep: true,
};
var jq = {
	'ok': false,
	'retries': 0
};
var maxPages = 20;
page.viewportSize = {
		width: 1920-getRandomInt(10,100),
		height: 1080-getRandomInt(10,100),
};
var args = require('system').args;
try{
	var input = JSON.parse(args[1]);
}catch(e){ console.log(e); phantom.exit(); }
/*********SETTINGS END*****************/
 
console.log('All settings loaded, start with execution');
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

/**********DEFINE STEPS THAT FANTOM SHOULD DO***********************/
steps = [          
    function init(){
    	if(typeof input.ranker == 'undefined')input.ranker=0;
    	
    	var pip= input.proxy.split(':');    	
    	phantom.setProxy(pip[0],pip[1]);
    	//page.open('http://icanhazip.com/');    	
    	page.settings.userAgent = input.useragent;    	
    	//page.open('http://www.whoishostingthis.com/tools/user-agent/');
    	
    	input.page_end = parseInt(input.page_end);
    	if(!isNaN(input.page_end) && input.page_end>0){    		
    		maxPages=input.page_end;
    	}       	
    },        
    function visit(){        
        page.open("https://amazon.com", function(status){        	
		});
    },       
    
	function fillSearchInput(){        
    	
    	console.log(input.search_term);
    	
		page.evaluate(function(q){ document.getElementById('twotabsearchtextbox').value = q; },input.search_term);		
    },
    
    
    
    function(){
    	debugSave();
    },
    function(){
    	phantom.exit();
    },
    
    
    function wait(){
    	pause(getRandomFloat(2,10));
    },
    function search(){
    	page.evaluate(function(){ document.querySelector("#nav-search input[type='submit']").click(); });
    	state.page=0;
    	state.rank=0;
    },
           
      
    function(){    	
    	save();    	
    },
    function parseResults(){
    	console.log("Checking Page: "+state.page);
    	findProduct();     	
    	if(state.index<0 || state.id.length<=0){ state.found=false; }
    	else{ state.found=true; }
    },
    function(){    	
    	var curPage = page.evaluate(function(){ return document.querySelector('.pagnCur').textContent; });  
    	if(!isNaN(curPage) && curPage>0)state.page=curPage;
    	console.log('Current Page: '+curPage);
    	
    	if(!state.found && state.page<maxPages){
    		var found = page.evaluate(function(){ return document.querySelectorAll('#pagnNextLink').length; });
    		found = parseInt(found);
    		
    		/*
    		if(!isNaN(found) && found>0)
    			page.evaluate(function(){ document.querySelector('#pagnNextLink').click(); });    		
    		else{
    			console.log("Next page button not found");
    			state.page = maxPages+10;
    		} 
    		*/
    		
    		if(!isNaN(found) && found>0){
    			page.evaluate(function(start){
    				start = parseInt(start);
    				if(!isNaN(start) && start>0){    					
    					console.log('Setting page...');    					
    					var next = document.querySelector('#pagnNextLink').getAttribute('href');
        				next = next.replace(/pg_(\d+)/,'pg_'+start).replace(/page=(\d+)/,'page='+start);
        				document.querySelector('#pagnNextLink').setAttribute('href',next);
    				}    				    				    			    				    				
    				
    				document.querySelector('#pagnNextLink').click();
    			},input.page_start);      			
    			input.page_start = 0;
    		}
    		else{
    			console.log("Next page button not found or exceeded allowed pages");
    			state.page = maxPages+10;
    		}
    		
    	}
    },
    function wait(){
    	pause(5);
    },
    function (){
    	//save();
    },
    function (){
    	if(!state.found && state.page<maxPages){
    		state.page++;
    		goToStep('parseResults');    		
    	}
    },
    

    function(){
    	if(!state.found){
    		save();
    	}
    },
    function(){
    	if(!state.found){
    		updateDB({
    			'snapshot': input.id+'.png',
    			'error': 'Product not found',
    			'status': 'Error'
    		});
    	}
    },   
    function(){
    	if(!state.found){
    		phantom.exit();
    	}
    }, 
    function wait(){
    	pause(getRandomFloat(2,30));
    },
    
    function getBadges(){
    	state.badge = page.evaluate(function(id){    		
    		var badge = document.querySelector('#'+id+' .sx-badge-region a .sx-badge-text');
    		if(badge)badge = badge.textContent;
    		else badge = '';

    		return badge;
    	},state.id);
    },
    
    function goToProduct(){
    	if(input.ranker){
    		goToStep('update');
    	}
    	else{
    		console.log("Profuct Index: "+state.index+", ID: "+state.id);
        	page.evaluate(function(id){
        		//console.log('#'+id);
        		//$('#result_'+index+' a').click();
        		document.querySelector('#'+id+' .s-access-detail-page').click();
        	},state.id);
    	}    	      
    },
    function wait(){
    	pause(getRandomFloat(10,20));
    },
    function selectOneTime(){
    	page.evaluate(function(){
    		if(document.querySelector('#onetimeOption input')){ document.querySelector('#onetimeOption input').click(); }   		
    	});    	
    },
    function wait(){
    	pause(1);
    },
    function cart(){
    	page.evaluate(function(){
    		document.querySelector('#add-to-cart-button').click();    		
    	});
    },
    function wait(){
    	pause(getRandomFloat(2,10));
    },
    function snapshot(){
    	save();
    },
    function update(){
    	var rank = parseInt(state.id.replace(/[^0-9]/g,""))+1+state.rank;
    	
    	updateDB({    		
    		'rank': rank,
    		'snapshot': input.id+'.png',
    		'session': input.id,
    		'keyword': input.term_id,
    		'proxy': input.proxy,
    		'badge': state.badge,
    		'status': 'Done'
    	});
    },
    function done(){
    	
    }        
];
/**********END STEPS THAT FANTOM SHOULD DO***********************/
 
//Execute steps one by one
interval = setInterval(executeRequestsStepByStep,50);
 
function executeRequestsStepByStep(){	
    if (loadInProgress == false && typeof steps[testindex] == "function" && !wait) {
    	console.log("step " + (testindex + 1) + ": " + functionName(steps[testindex]));
        steps[testindex]();
        testindex++;
    }
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}
function functionName(fun) {
	var ret = fun.toString();
	ret = ret.substr('function '.length);
	ret = ret.substr(0, ret.indexOf('('));
	return ret;
}
function pause(secs){
	if(debug.nosleep){
		console.log("Sleeping is OFF");
		setTimeout(function(){ wait=false },1000);
		return;
	}
	console.log('Sleeping for '+secs+' seconds');
	wait = true;
	
	if(debug.pause){
		if(secs>0)
			setTimeout(function(){ pause(--secs); },1000);
		else
			wait=false;
	}
	else{
		setTimeout(function(){ wait=false },secs*1000);
	}
}
function addJquery(){	
	wait = true;
	page.injectJs('jquery.min.js');
	setTimeout(function(){wait = false;},500);
	
	/*
	page.evaluate(function(){
		javascript:(function(){function l(u,i){var d=document;if(!d.getElementById(i)){var s=d.createElement('script');s.src=u;s.id=i;d.body.appendChild(s);}}l('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js','jquery')})();
	});
	*/
}
function save(){	
	//page.render(snapshotDir+'/'+input.id+'.png');
	//console.log(snapshotDir+'/'+input.id+'.png');
}
function debugSave(){	
	page.render(snapshotDir+'/'+input.id+'.png');
	console.log(snapshotDir+'/'+input.id+'.png');
}
function updateDB(data){
	var dfd = $.Deferred();
	
	data['id'] = input.id;
	data['ranker'] = input.ranker;	
		
	loadInProgress = true;
	var postdata = new Array();
	for(var k in data)postdata.push(k+'='+data[k]);	
	postdata = postdata.join('&');
		
	console.log("Updating DB");
	var page = webPage.create();
	page.open(server, 'post', postdata, function (status) {
		console.log('Ajax Content: '+page.content);
		loadInProgress = false;
		dfd.resolve(true);
		console.log("Done updating DB");
	});
	return dfd.promise();
} 
function getRandomFloat(min, max) {
  return Math.random() * (max - min) + min;
}
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}
function findProduct(){	
	var res = page.evaluate(function(target,debug){
		target = target.toUpperCase();
		var index = -1;
		var id = '';
		var sponsoredCount = 0;
		//var items = document.querySelectorAll('#resultsCol .s-result-item[data-asin="'+target+'"]');		
		var items = document.querySelectorAll('#resultsCol .s-result-item[data-asin]');
		
		for(n in items){
			//if(debug.msgs)console.log(n);
			var el = items[n];    					
			if(typeof el == 'object'){
				var sponsored = el.innerHTML.indexOf('sponsored');
				
				if(sponsored>0){ sponsoredCount++; }
				
				//console.log('-----------------------------------------------Sponsored Item: '+sponsored);
				if(el.getAttribute('data-asin').trim()==target.trim() && sponsored<0){
					index = n;
					id = el.getAttribute('id');				
					el.scrollIntoView();
					//$('html, body').animate({ scrollTop: el.offset().top }, 2000);
					break;
				}
			}
		} 
		return {index: index, id: id, sponsoredCount: sponsoredCount};
	},input.asin,debug);
	
	/*
	var res = page.evaluate(function(target,debug){
		target = target.toUpperCase();
		var index = -1;
		var id = '';
		var items = document.querySelectorAll('#resultsCol .s-result-item[data-asin="'+target+'"]');		
		
		for(n in items){
			//if(debug.msgs)console.log(n);
			var el = items[n];    					
			if(typeof el == 'object'){
				var sponsored = el.innerHTML.indexOf('sponsored');
				//console.log('-----------------------------------------------Sponsored Item: '+sponsored);
				if(sponsored<0){
					index = n;
					id = el.getAttribute('id');				
					el.scrollIntoView();
					//$('html, body').animate({ scrollTop: el.offset().top }, 2000);
					break;
				}
			}
		} 
		return {index: index, id: id};
	},input.asin,debug);
	*/
	
	state.index = res.index;
	state.id = res.id;	
	state.rank = state.rank-res.sponsoredCount;
}
function findProductBytextDeprecated(){
	var res = page.evaluate(function(target,debug){
		var index = -1;
		var id = '';
		var items = document.querySelectorAll('#resultsCol .s-result-item');
		for(n in items){
			if(debug.msgs)console.log(n);
			var el = items[n];    					
			if(typeof el == 'object' && el.querySelectorAll('.s-sponsored-list-header').length<=0){
				var title = el.querySelector('.s-access-title');								
				title = title.textContent;
				
				if(debug.msgs)console.log(title);
				if(debug.msgs)console.log(target);
				if(debug.msgs)console.log("--------------");
				//if(title.trim() == target.trim()){    				
				if(target.trim().indexOf(title.trim().replace(/[^\x00-\x7F]$/,''))>=0 || title.trim() == target.trim()){
					index = n;
					id = el.getAttribute('id'); 
					el.scrollIntoView();
					//$('html, body').animate({ scrollTop: el.offset().top }, 2000);
					break;
				}
			}						    	
		} 
		return {index: index, id: id};
	},input.product,debug);
	
	state.index = res.index;
	state.id = res.id;	
}
function goToStep(name){
	for(x in steps){
		var funcName = functionName(steps[x]);		
		if(funcName == name){
			console.log("Going back to: Step "+x+", "+funcName);
			testindex = x-1;
			return false;
		}
	}
}

/**
 * These listeners are very important in order to phantom work properly. Using these listeners, we control loadInProgress marker which controls, weather a page is fully loaded.
 * Without this, we will get content of the page, even a page is not fully loaded.
 */
page.onResourceRequested = function(request) {
	if(debug.request)
		console.log('Request ' + JSON.stringify(request, undefined, 4));
};
page.onResourceReceived = function(response) {
	if(debug.response)
		console.log('Response ' + JSON.stringify(response, undefined, 4));
};
page.onLoadStarted = function() {
    loadInProgress = true;
    console.log('Loading started');
};
page.onLoadFinished = function() {
    loadInProgress = false;
    console.log('Loading finished');
};
page.onConsoleMessage = function(msg) {
    console.log(msg);
}
page.onError = function(msg, trace) {
	var msgStack = ['PHANTOM ERROR: ' + msg];
	if (trace && trace.length) {
	    msgStack.push('TRACE:');
	    trace.forEach(function(t) {
	      msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function +')' : ''));
	    });
	}
	console.error(msgStack.join('\n'));
	
	
	wait = true;
	$.when( 			
		updateDB({
			'snapshot': input.id+'.png',
			'error': msgStack.join('\n'),
			'status': 'Error'
		})
	).done(function(){ phantom.exit(); });	
	
};
