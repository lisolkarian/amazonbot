<?php 

function buildResponse($response,$status,$data){
	return $response->withStatus($status)
		->withHeader('Content-Type', 'application/json')
		->write($data);	
	
}
function t($v,$live=false){
	print '<pre>';
	var_dump($v);
	print '</pre>';
	if(!$live)exit;
}

?>