<?php
class Scraper{ 
	private $timeout,
			$script,
			$start,
			$pid,
			$vars; 		   
    public function __construct($vars){
    	$this->vars = $vars;
    	$this->pid = 0;
    	$this->timeout = 60*60*1;
    	$this->script = $GLOBALS['settings']['path_phantom'] . '/api.js';
    }
    
    function launch($debug=false){       	
    	$cmd = "/usr/local/bin/phantomjs '{$this->script}' '{$this->vars}'";
    	
    	if($debug)t($cmd);
    	//passthru($cmd,$res);
    	//t(exec($cmd),1);
    	//t($res);    	

    	$process = new Process($cmd);
    	$process->start();
    	$this->start = microtime();    	
    	$this->pid = $process->getPid();
    	
    	$cmd = base64_encode($cmd);
    	mysql_query("INSERT INTO log (`pid`,`cmd`) VALUES ('{$this->pid}','$cmd')");
    	$uId = mysql_insert_id();

    	$this->wait();  

    	$res = mysql_fetch_assoc(mysql_query("SELECT * FROM log WHERE id='$uId'"));
    	return $res;
    }
    function wait(){
    	do{
    		$time = microtime() - $this->start;
    		sleep(1);			    		
    	}while($time<$this->timeout && $this->isActive());
    }
    function isActive(){    	
    	$process = new Process();
    	$process->setPid($this->pid);
    	$status = $process->status();
    	$status = ($status==true)?1:0;    
   		return $status;
    }
}