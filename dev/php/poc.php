<?php 	
	include 'main.php';
		
	if($_REQUEST['submit']){						
		if(!isActive()){		
			$data = array();
			$cols = array('direction','way','date','hour','minute','ap','location');
			foreach($cols as $k)$data[$k] = $_REQUEST[$k];
			
			$data = implode(",",$data);
			
			mysql_query("UPDATE shuttle SET data='$data', pid=0");
		}		
		$message = launch();
	}
?>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script
</head>
<script>
	$(document).ready(function(){
		checkActive();
		checkResults();
		
		function checkActive(){
			$.post('http://162.216.6.133/~axsmotor/nodejs/shuttle/main.php?action=isActive&ajax=1',function(res){
				var status = (res==1)?'Searching...':'Idle';
				$('.status').text(status);
				setTimeout(function(){ checkActive(); },1000);
			});					
		}
		function checkResults(){
			$.post('http://162.216.6.133/~axsmotor/nodejs/shuttle/main.php?action=getResults&ajax=1',function(res){				
				$('.results').html(res);
				setTimeout(function(){ checkResults(); },1000);
			});					
		}
	});
</script>

<form method="post">
	<input type="hidden" name="submit" value="submit"/>
	<div>
	<label>Direction</label>
	<select name="direction">
		<option value="from" selected>Outbound</option>
		<option value="to">Inbound</option>
	</select>
	</div>
		
	<div>
	<input type="radio" name="way" value="1" checked> One way
	<input type="radio" name="way" value="2"> Two ways
	</div> 		
	
	<div>
	<label>Date</label>
	<input type="text" name="date" value="<?=date('m/d/Y',strtotime('now'))?>" placeholder="mm/dd/yyyy">
	</div>
	
	<div>
	<label>Time</label>	
	<select name="hour">
		<option value="1">1</option>
		<option value="2">2</option>
		<option value="3">3</option>
		<option value="4">4</option>
		<option value="5">5</option>
		<option value="6">6</option>
		<option value="7">7</option>
		<option value="8">8</option>
		<option value="9">9</option>
		<option value="10">10</option>
		<option value="11">11</option>
		<option value="12">12</option>
	</select>
	
	<select name="minute">
		<option value="0">00
		</option><option value="5">05
		</option><option value="10">10
		</option><option value="15">15
		</option><option value="20" selected="">20
		</option><option value="25">25
		</option><option value="30">30
		</option><option value="35">35
		</option><option value="40">40
		</option><option value="45">45
		</option><option value="50">50
		</option><option value="55">55
		</option>
	</select>
	<select name="ap">
		<option value="AM" selected>AM</option>
		<option value="PM">PM</option>
	</select>

	
	</div>
	
	<div>
	<label>Location</label>
	<input type="location" name="location" value="Hampton Inn-PRC" placeholder="Hampton Inn-PRC">
	</div>
	
	<input type="submit" value="submit">
</form>
<hr>

<div>Scraper Status: <span class="status"></span></div>
<div>System Message: <b><?=$message?></b></div> 
<div>Results: <br><span class="results"></span></div>