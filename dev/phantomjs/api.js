var steps=[];
var testindex = 0;
var loadInProgress = false;//This is set to true when a page is still loading
var wait = false;
var croak = false;
var debug = {
	'request'	: false,
	'response'	: false
}
/*********SETTINGS*********************/
phantom.injectJs('./jquery.min.js');

var system = require('system');
var pid = system.pid;

var webPage = require('webpage');
var page = webPage.create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.viewportSize = {
		width: 1920,
		height: 1080
};
page.settings.javascriptEnabled = true;
page.settings.loadImages = false;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;

var args = require('system').args;
var output = '/var/www/html/AmazonBot/phantomjs/test2.html';
var server = '';
var snapshotDir = '/var/www/html/AmazonBot/snapshots/';
try{
	var input = JSON.parse(args[1]);
}catch(e){ saveErrors(new Array({'system':e})); }

/*********SETTINGS END*****************/

console.log('All settings loaded, start with execution');
page.onConsoleMessage = function(msg) {
    console.log(msg);
};

/**********DEFINE STEPS THAT FANTOM SHOULD DO***********************/
var results = {};
/*
switch(input.action){	
	default:
		phantom.injectJs('./session.js');
		break;
}
*/
phantom.injectJs('./session.js');
/**********END STEPS THAT FANTOM SHOULD DO***********************/

function timeDiff(time,minutes){
	var newtime = {
		'hour': time.hour,	
		'minute': time.minute,
		'ap': time.ap,
	};
	var changeHour = changeAP = false;
	if(newtime.minute < minutes){		
		changeHour = true;
		newtime.minute = 60+(newtime.minute-minutes);
	}
	else{
		newtime.minute = newtime.minute-minutes
	}
	
	if(changeHour){
		if(newtime.hour == 12){
			changeAP = true;
		}
		newtime.hour--; 
	}
	if(changeAP){
		if(newtime.ap.toLowerCase() == 'pm'){ newtime.ap = 'AM'; }
		else{ newtime.ap = 'PM'; }
	}	
	return newtime;
}
function compareTimes(option,time){
	var target = time.hour.replace(/^0/,'')+':'+time.minute+' '+time.ap;
	
	var op='';
	var res = option.pickupTime.match(/(\d+):(\d+) (AM|PM)/g);
	if(res.length>0){op = res[0];}
	
	/*
	console.log(target);
	console.log(op);
	console.log("---------------------------------");
	*/
	
	return (target == op)?true:false;	
}
function cleanupText(text){
	text = text.replace(/^"|"$/g,'').replace(/[^\x00-\x7F]/g,'').replace(/^[\s\t\n]+|[\s\t\n]+$/g,'').replace(/\t+/g,'').replace(/\n+/g,"\n");	
	return text;
}
function parseTimes(date){
	var result = page.evaluate(function(date){
		var times = new Array();
		$.each($('#SVC_INFO5 .controls label'),function(){
			var el = $(this);
			
			var timelabel1 = el.find('.lbl:eq(0)').text();
			var timelabel2 = el.find('.lbl:eq(1)').text();
			var time1 = time2 = '';
			
			var res1 = timelabel1.match(/(\d+):(\d+) (AM|PM)/g);
			if(res1.length>0){time1 = res1[0];}
			
			var res2 = timelabel2.match(/(\d+):(\d+) (AM|PM)/g);
			if(res2.length>0){time2 = res2[0];}
			
			if(time1.length>0 && time2.length>0)
				times.push({'time1':date+' '+time1, 'time2': date+' '+time2});			
		});
		return times;
	},date);
	return result;	
	
	
}
function parseErrors(){
	var result = page.evaluate(function(){
		var errors = new Array();
		$.each($('.alert-error, .control-group.error .help-block, .control-group.error .help-inline'),function(){
			var err = $(this).text();
			if($(this).is(':visible') && err.length>0)
				errors.push($(this).text());
		});
		return errors;
	});
	for(x in result){ result[x] = cleanupText(result[x]); }
	return result;		
}
function fillForm(data){
	var result = page.evaluate(function(data){		
		$.each(data,function(selector,value){
			$(selector).val(value);
			console.log(selector+': '+$(selector).val());
		});
	},data);
	return result;	
}
function click(selector){
	var result = page.evaluate(function(selector) {
		$(selector).click();
	},selector);
	return result;
}
function findLink(text){
	var result = page.evaluate(function(text) {		
		var links = $('a').filter(function(){ console.log('sd'); return $(this).text().indexOf(text)>=0; });						
		if(links.length>0) return links[0];
		return false;
	},text);
	return result;	
}
function findLinks(text){	
	var result = page.evaluate(function(text) {
		var links = $('a').filter(function(){ return $(this).text().indexOf(text)>=0; })
		return links;
	},text);
	return result;
}
function pause(secs){
	wait = true;
	setTimeout(function(){ wait=false },secs);
}
function load(url){
	console.log('Loading: '+url);
    page.open(url, function(status){});
}

function updateDB(data){
	var dfd = $.Deferred();
	
	data['pid'] = pid;	

	loadInProgress = true;
	var postdata = new Array();
	for(var k in data)postdata.push(k+'='+data[k]);	
	postdata = postdata.join('&');	

	console.log("Updating DB");
	var page = webPage.create();
	page.open(server, 'post', postdata, function (status) {
		console.log(page.content);
		loadInProgress = false;
		dfd.resolve(true);
		console.log("Done updating DB");
	});
	return dfd.promise();
} 
function save(){
	console.log(snapshotDir+'/'+pid+'.png');
	page.render(snapshotDir+'/'+pid+'.png');
	updateDB({		
		'html': encodedHTML()
	});    	 
}
function saveLocal(){
	var fs = require('fs');
	var result = page.evaluate(function() {
		return document.querySelectorAll("html")[0].outerHTML;
	});
   fs.write(output,result,'w');
}
function encodedHTML(){
	var html = '';
	try{ html = Base64.encode(page.content); }catch(e){};
	return html;
}
function saveErrors(errors){			
	var html = '';
	
	page.render(snapshotDir+'/'+pid+'.png');
	
	//errors = JSON.stringify(errors);	
	$.when( 			
		updateDB({
			//'html'	: encodedHTML(),
			'errors': errors
		}) 
	).done(function(){ page.onError(JSON.stringify(errors)); });			
}
function validateProgress(){
	errors = new Array();
	try{    		
		errors = parseErrors();
	}catch(e){ errors.push({'system':e}); }
	
	if(errors.length>0){
		saveErrors(errors);
	}
}

/*********************** Command Iterator ***********************/
interval = setInterval(executeRequestsStepByStep,50);

function executeRequestsStepByStep(){
	if(croak){ phantom.exit(); }
	try{
		if (loadInProgress == false && typeof steps[testindex] == "function" && !wait) {
			console.log("step " + (testindex + 1) + ": " + functionName(steps[testindex]));
			steps[testindex]();
			testindex++;
		}
	}catch(e){ page.onError(e); }
	
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}
function functionName(fun) {
  var ret = fun.toString();
  ret = ret.substr('function '.length);
  ret = ret.substr(0, ret.indexOf('('));
  return ret;
}


/**
 * These listeners are very important in order to phantom work properly. Using these listeners, we control loadInProgress marker which controls, weather a page is fully loaded.
 * Without this, we will get content of the page, even a page is not fully loaded.
 */
page.onResourceRequested = function(request) {
	if(debug.request)
		console.log('Request ' + JSON.stringify(request, undefined, 4));
};
page.onResourceReceived = function(response) {
	if(debug.response)
		console.log('Response ' + JSON.stringify(response, undefined, 4));
};
page.onLoadStarted = function() {
    loadInProgress = true;
    console.log('Loading started');
};
page.onLoadFinished = function() {
    loadInProgress = false;
    console.log('Loading finished');
};
page.onConsoleMessage = function(msg) {
    console.log('System Message: '+msg);
};
page.onError = function(msg, trace) {
	var msgStack = ['PHANTOM ERROR: ' + msg];
	if (trace && trace.length) {
	    msgStack.push('TRACE:');
	    trace.forEach(function(t) {
	      msgStack.push(' -> ' + (t.file || t.sourceURL) + ': ' + t.line + (t.function ? ' (in function ' + t.function +')' : ''));
	    });
	}
	console.error(msgStack.join('\n'));
	phantom.exit();
};

/************* HTML Encoding functions *************/
var Base64 = {
    // private property
    _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
    // public method for encoding
    encode: function(input) {
    	var output = "";	       
        var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
        var i = 0;
        input = Base64._utf8_encode(input);
        while (i < input.length) {
            chr1 = input.charCodeAt(i++);
            chr2 = input.charCodeAt(i++);
            chr3 = input.charCodeAt(i++);
            enc1 = chr1 >> 2;
            enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
            enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
            enc4 = chr3 & 63;
            if (isNaN(chr2)) {
                enc3 = enc4 = 64;
            } else if (isNaN(chr3)) {
                enc4 = 64;
            }
            output = output +
                this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) +
                this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);
        }
        return output;
    },
    // public method for decoding
    decode: function(input) {
        var output = "";
        var chr1, chr2, chr3;
        var enc1, enc2, enc3, enc4;
        var i = 0;
        input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");
        while (i < input.length) {
            enc1 = this._keyStr.indexOf(input.charAt(i++));
            enc2 = this._keyStr.indexOf(input.charAt(i++));
            enc3 = this._keyStr.indexOf(input.charAt(i++));
            enc4 = this._keyStr.indexOf(input.charAt(i++));
            chr1 = (enc1 << 2) | (enc2 >> 4);
            chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
            chr3 = ((enc3 & 3) << 6) | enc4;
            output = output + String.fromCharCode(chr1);
            if (enc3 != 64) {
                output = output + String.fromCharCode(chr2);
            }
            if (enc4 != 64) {
                output = output + String.fromCharCode(chr3);
            }
        }
        output = Base64._utf8_decode(output);
        return output;
    },
    // private method for UTF-8 encoding
    _utf8_encode: function(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    },
    // private method for UTF-8 decoding
    _utf8_decode: function(utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;
        while (i < utftext.length) {
            c = utftext.charCodeAt(i);
            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            } else if ((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i + 1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            } else {
                c2 = utftext.charCodeAt(i + 1);
                c3 = utftext.charCodeAt(i + 2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }
        }
        return string;
    }
}	