var testindex = 0;
var wait = false;
var loadInProgress = false;//This is set to true when a page is still loading
 
/*********SETTINGS*********************/
var webPage = require('webpage');
var page = webPage.create();
page.settings.userAgent = 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.157 Safari/537.36';
page.settings.javascriptEnabled = true;
page.settings.loadImages = false;//Script is much faster with this field set to false
phantom.cookiesEnabled = true;
phantom.javascriptEnabled = true;

var snapshotDir = '/var/www/html/AmazonBot/snapshots/';
var pid = 'test';
var state = {};
var debug = {
	request: false,
	response: false,
};
/*********SETTINGS END*****************/
 
console.log('All settings loaded, start with execution');
page.onConsoleMessage = function(msg) {
    console.log(msg);
};
/**********DEFINE STEPS THAT FANTOM SHOULD DO***********************/
steps = [ 
    function visit(){        
        page.open("https://amazon.com", function(status){
			
		});
    },
    function jq(){
    	addJquery();
    },
	function fillSearchInput(){        
		page.evaluate(function(){ $("#twotabsearchtextbox").val('scar cream'); });		
    },
    function search(){
    	page.evaluate(function(){ $('.nav-search-submit input').click(); });
    },
    function jq(){
    	addJquery();
    },
    function findProduct(){
    	state.index = page.evaluate(function(){
    		var index = -1;
    		var items = $('#resultsCol .s-result-item');
    		$.each(items,function(n){
    			console.log(n);
    			var el = $(this);    			
    			if(el.find('.s-sponsored-list-header').length>0) return;
    			
    			var title = el.find('.s-access-title').text();
    			if(title == 'Best Scar Cream and Stretch Mark Removal Cream - Huge 4 Oz. - Breakthrough Treatment for Acne & Other Scars'){    				
    				index = n;
    				 $('html, body').animate({ scrollTop: el.offset().top }, 2000);
    				 return false;
    			}    			
    		}); 
    		return index;
    	});    	
    },
    function(){
    	pause(2);    	
    },
    function goToProduct(){
    	console.log("Profuct Index: "+state.index);
    	page.evaluate(function(index){
    		console.log('#result_'+index+' a');
    		//$('#result_'+index+' a').click();
    		document.querySelector('#result_'+index+' a').click();
    	},state.index);      	
    },
    function(){
    	pause(10*1000);
    },
    function cart(){
    	page.evaluate(function(){
    		document.querySelector('#add-to-cart-button').click();    		
    	});
    },
    function(){
    	pause(1);
    },
    function(){
    	save();
    }
];
/**********END STEPS THAT FANTOM SHOULD DO***********************/
 
//Execute steps one by one
interval = setInterval(executeRequestsStepByStep,50);
 
function executeRequestsStepByStep(){	
    if (loadInProgress == false && typeof steps[testindex] == "function" && !wait) {
    	console.log("step " + (testindex + 1) + ": " + functionName(steps[testindex]));
        steps[testindex]();
        testindex++;
    }
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}
function functionName(fun) {
	var ret = fun.toString();
	ret = ret.substr('function '.length);
	ret = ret.substr(0, ret.indexOf('('));
	return ret;
}
function pause(secs){
	wait = true;
	setTimeout(function(){ wait=false },secs);
}
function addJquery(){	
	wait = true;
	page.injectJs('jquery.min.js');
	setTimeout(function(){wait = false;},500);
	
	/*
	page.evaluate(function(){
		javascript:(function(){function l(u,i){var d=document;if(!d.getElementById(i)){var s=d.createElement('script');s.src=u;s.id=i;d.body.appendChild(s);}}l('https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js','jquery')})();
	});
	*/
}
function save(){
	console.log(snapshotDir+'/'+pid+'.png');
	page.render(snapshotDir+'/'+pid+'.png');	 
}
 

/**
 * These listeners are very important in order to phantom work properly. Using these listeners, we control loadInProgress marker which controls, weather a page is fully loaded.
 * Without this, we will get content of the page, even a page is not fully loaded.
 */
page.onResourceRequested = function(request) {
	if(debug.request)
		console.log('Request ' + JSON.stringify(request, undefined, 4));
};
page.onResourceReceived = function(response) {
	if(debug.response)
		console.log('Response ' + JSON.stringify(response, undefined, 4));
};
page.onLoadStarted = function() {
    loadInProgress = true;
    console.log('Loading started');
};
page.onLoadFinished = function() {
    loadInProgress = false;
    console.log('Loading finished');
};
page.onConsoleMessage = function(msg) {
    console.log(msg);
}