<?php
session_write_close();
	
usehelper("ajax::dispatch");

function getSummary(){	
}
function loadKeywords(){
	$pId = $_REQUEST['pId'];
	
	if($pId)
		$keywords = getProductKeywords($pId);
	else
		$keywords = getUserKeywords($pId);
	
	json(array('keywords'=>$keywords));
}
function loadSession(){
	$id = (int)$_REQUEST['id'];
	
	$sql = "SELECT * FROM products_sessions WHERE id='$id'";
	$q = mysql_query($sql);	
	$item = mysql_fetch_assoc($q);		
	if(!$item)err("Item not found!");
	
	$item = formatSession($item);	
	json(array('item'=>$item));
}
function clearData(){
	sql("TRUNCATE TABLE `products_sessions`");
}

function loadSessions(){
	$sortColumns = array('timestamp','schedule','search_term','product','page_start','','status','');
	$array = array();

	$offset = (int)$_REQUEST['start'];
	$length = (int)$_REQUEST['length'];

	if($_REQUEST['order'])$orderby = array('col'=>$sortColumns[$_REQUEST['order'][0]['column']],'dir'=>$_REQUEST['order'][0]['dir']);

	$offset = $offset;
	$length = (int) $length;
	if ($length)
		$limit = "LIMIT $offset,$length";
	else
		$limit = "";

	if (!$orderby)
		$orderby = array('timestamp DESC');
	else
		$orderby = array($orderby['col'] . " " . $orderby['dir']);


	$wheresql = array();
	$wheresql[] = "user_id='{$_SESSION['user']->id}'";
	$filter = $_REQUEST['filter'];
	if($filter){
		foreach($filter as $k=>$v){
			if(is_array($v) || trim($v)){
				switch($k){
					case 'pId':
						$wheresql[] = "product_id = '$v'";
						break;
					case 'date':
						$range = explode("-",$v);
						$wheresql[] = "schedule BETWEEN '".dbDate($range[0])." 00:00:00' AND '".dbDate($range[1])." 23:59:59'";
						break;
					case 'search_term':
						$wheresql[] = "term_id = '$v'";
						break;
					case 'status':
						$wheresql[] = "status = '$v'";
						break;
					default:
						break;
				}
			}
		}
	}


	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM products_sessions WHERE ".implode(" AND ",$wheresql)." ORDER BY  " . implode(' ', $orderby) . " $limit";
	//t($sql);
	$q = mysql_query($sql);
	list($total) = mysql_fetch_array(mysql_query("SELECT FOUND_ROWS();"));
	while($r = mysql_fetch_assoc($q)){
		$r = formatSession($r);		
		$array[] = $r;
	}
	json(array(
	'data'=> $array,
	'total' => $total,
	'page' => $offset,
	'sort'	=> ($sortby)?$sortby['col']:$_REQUEST['order'][0]['column'],
	'sortDir' => ($sortby)?$sortby['dir']:$_REQUEST['order'][0]['dir'],
	'length' => $length,
	));
}
function removeSession(){
	$id = (int)$_REQUEST['id'];

	sql("DELETE FROM products_sessions WHERE id='$id'");
}
function formatSession($r){
	$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
	$r['schedule'] = date('m/d/Y h:i:s a',strtotime($r['schedule']));
	
	if($r['status'] == 'Pending'){
		$r['duration'] = '-';
	}
	else{
		$end = ($r['end'])?$r['end']:'now';
		$r['duration'] = xTimeAgo($r['start'],$end,'');
	}	
	$r['product'] = strshorten($r['product'],50);
	
	return (object)$r;
}

function getSessionsChartData(){
	$id = (int)$_REQUEST['id'];		

	$range = split(" - ",$range);
	$stats = (object)array(
			'labels'	=> array(),
			'data'		=> array(),
			'series'	=> array()
	);

	$dates = $total = $done = $errors = $queued = $series = $labels = array();

	$wheresql = array("1=1");
	if($id)$wheresql[] = "product_id = '$id'";
	if($range)$wheresql[] = "schedule BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' and '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";
	
	$wheresql = array();
	$wheresql[] = "user_id='{$_SESSION['user']->id}'";
	
	$filter = $_REQUEST['filter'];	
	if(!$filter['date']) $filter['date'] = date('m/d/y',strtotime("-30 days")).' - '.date('m/d/y',strtotime("+10 day"));
	
	if($filter){
		foreach($filter as $k=>$v){
			if(is_array($v) || trim($v)){
				switch($k){
					case 'pId':
						$wheresql[] = "product_id = '$v'";
						break;
					case 'date':
						$range = explode("-",$v);						
						$wheresql[] = "schedule BETWEEN '".dbDate($range[0])." 00:00:00' AND '".dbDate($range[1])." 23:59:59'";
						break;
					case 'search_term':
						$wheresql[] = "term_id = '$v'";
						break;
					case 'status':
						$wheresql[] = "status = '$v'";
						break;
					default:
						break;
				}
			}
		}
	}
	

	$sql = "SELECT * FROM products_sessions AS s
				WHERE ".implode(" AND ",$wheresql)."
				ORDER BY timestamp ASC";
	//t($sql);
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
		$date = strtotime(date('y-m-d 00:00:00',strtotime($r['schedule'])));
		$dates[] = $date;
		if(!$total[$date])$total[$date]=0;
		if(!$done[$date])$done[$date]=0;
		if(!$errors[$date])$errors[$date]=0;
		if(!$pending[$date])$pending[$date]=0;

		$total[$date]++;
		if($r['status'] == 'Done' )$done[$date]++;
		if($r['status'] == 'Pending' || $r['status'] == 'Queued')$pending[$date]++;
		if($r['status'] == 'Error' )$errors[$date]++;

		$dates = array_unique($dates);
		sort($dates);
	}
	foreach($dates as $key){
		$label = date('m/d/y',$key);
		$stats->labels[] = $label;
		if(!$stats->data[$label])$stats->data[$label]=array('total'=>0,'done'=>0,'errors'=>0,'pending'=>0);

		if($total[$key])$stats->data[$label]['total']+=$total[$key];
		if($done[$key])$stats->data[$label]['done']+=$done[$key];
		if($errors[$key])$stats->data[$label]['errors']+=$errors[$key];
		if($pending[$key])$stats->data[$label]['pending']+=$pending[$key];
	}


	$series = array();
	foreach($stats->labels as $i=>$l){
		$total = ($stats->data[$l]['total'])?$stats->data[$l]['total']:0;
		$done = ($stats->data[$l]['done'])?$stats->data[$l]['done']:0;
		$errors = ($stats->data[$l]['errors'])?$stats->data[$l]['errors']:0;
		$pending = ($stats->data[$l]['pending'])?$stats->data[$l]['pending']:0;

		$series[] = array($l,$total,$done,$errors,$pending);
	}

	$stats->series = $series;

	json($stats);
}