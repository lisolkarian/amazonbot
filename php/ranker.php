<?php
session_write_close();

usehelper("ajax::dispatch");

function getUniqueProducts(){
	$ajax=(int)$_REQUEST['ajax'];
	$items = array();
	
	$q = mysql_query("SELECT * FROM ranker_keywords WHERE user_id='{$_SESSION['user']->id}' GROUP BY asin ORDER BY title ASC");
	while($r = mysql_fetch_assoc($q)){
		$items[] = (object)$r;
	}
	if($ajax)
		json(array('items'=>$items));
	else
		return $items;
}
function addKeywords(){	
	$asin = $_REQUEST['asin'];
	$schedule = $_REQUEST['schedule'];
	$keywords = array_unique(array_filter(explode("\\r\\n",$_REQUEST['keywords'])));
	$userId = $_SESSION['user']->id;
	
	if(!$keywords)err('No valid keywords found!');
	if(!$schedule)err('Schedule is missing!');
	
	$product = getAsinProduct($asin);
	if(!$product)err("Product not found!");

	foreach($keywords as $k){		
		mysql_query("INSERT INTO ranker_keywords (`asin`,`title`,`keyword`,`user_id`,`schedule`) VALUES ('$asin','{$product->title}','$k','$userId','$schedule')");
	}
	json();
}
function getAsinProduct($asin=false){	
	if(!$asin)return false;
		
	uselib('amazon::ecs');
	$api = new ECS();
	
	$error = '';
	try{
		$p = $api->getProduct($asin,'ASIN');
	} catch (Exception $e) {
		$error = $e->getMessage();
	}	
	if($error)return false;
	return $p;
}
function loadProductKeywords(){
	$asin = $_REQUEST['asin'];

	$_REQUEST['filter']['product'] = array($asin);
	$_REQUEST['start'] = 0;
	$_REQUEST['length'] = 1000000;
	
	$items = loadKeywords(false);
	
	json(array('items'=>$items));
}
function loadKeywords($ajax=true){		
	$sortColumns = array('','k.timestamp','r.timestamp','k.keyword','k.schedule','k.asin','k.title','r.rank','','','');	

	$array = array();
	$byproduct = array();

	$offset = (int)$_REQUEST['start'];
	$length = (int)$_REQUEST['length'];

	if($_REQUEST['order'] && $sortColumns[$_REQUEST['order'][0]['column']])$orderby = array('col'=>$sortColumns[$_REQUEST['order'][0]['column']],'dir'=>$_REQUEST['order'][0]['dir']);

	$offset = $offset;
	$length = (int) $length;
	if ($length)
		$limit = "LIMIT $offset,$length";
	else
		$limit = "";

	if (!$orderby)
		$orderby = array('k.timestamp DESC');
	else
		$orderby = array($orderby['col'] . " " . $orderby['dir']);
		
	$wheresql = array();
	$wheresql[] = "user_id='{$_SESSION['user']->id}'";
	$filter = $_REQUEST['filter'];	
	if($filter){
		foreach($filter as $k=>$v){			
			if(is_array($v) || trim($v)){
				switch($k){
					case 'keyword':
						$wheresql[] = "k.keyword like '%$v%'";
						break;
					case 'asin':
						$wheresql[] = "k.asin like '%$v%'";
						break;
					case 'product':						
						$wheresql[] = "k.asin IN ('".implode("','",$v)."')";						
						break;
					default:
						$wheresql[] = "k.$k = '$v'";
						break;
				}
			}
		}
	}

	$sql = "SELECT SQL_CALC_FOUND_ROWS k.*,r.rank AS rank, r.timestamp AS rank_timestamp, ro.rank as latest_pos, ro.timestamp AS latest_pos_timestamp FROM ranker_keywords AS k				
				LEFT JOIN ranker_keywords_ranks AS r ON r.id=(SELECT id FROM ranker_keywords_ranks WHERE keyword_id=k.id ORDER BY id DESC LIMIT 1)
				LEFT JOIN ranker_keywords_ranks AS ro ON ro.id=(SELECT id FROM ranker_keywords_ranks WHERE keyword_id=k.id AND rank!=r.rank ORDER BY id DESC LIMIT 1)
				WHERE ".implode(" AND ",$wheresql)." ORDER BY  " . implode(' ', $orderby) . " $limit";
	//t($sql);
	$q = mysql_query($sql);
	list($total) = mysql_fetch_array(mysql_query("SELECT FOUND_ROWS();"));
	while($r = mysql_fetch_assoc($q)){	
		if(!$byproduct[$r['asin']])$byproduct[$r['asin']] = array();
		
		$r = format($r);
		$byproduct[$r->asin][] = $r;
		$array[] = $r;
	}
	
	if(!$ajax){
		return $array;
	}	
	json(array(
		//'data'=> $array,
		'data'=> $byproduct,	
		'total' => $total,
		'page' => $offset,
		'sort'	=> ($sortby)?$sortby['col']:$_REQUEST['order'][0]['column'],
		'sortDir' => ($sortby)?$sortby['dir']:$_REQUEST['order'][0]['dir'],
		'length' => $length,
	));
}
function format($r){
	$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
	$r['latest_pos_timestamp'] = date('m/d/Y h:i:s a',strtotime($r['latest_pos_timestamp']));
	
	$r['badges_found'] = explode(',',str_replace('?',"'",$r['badges_found']));
		
	$r['history'] = getRankerHistory($r['id'],array(),7);
	$r['line'] = array();
	foreach($r['history'] as $h)$r['line'][] = $h->rank;
	$r['line'] = implode(",",$r['line']);
	
	if($r['latest_pos'])$r['rank_delta']=$r['latest_pos']-$r['rank'];
	else $r['rank_delta']='-';

	//$r['badges'] = getRankerBadgeLatest($r['id']);

	if($r['history']) $r['latest_rank_timestamp'] = date('m/d/Y h:i:s a',strtotime(end($r['history'])->timestamp));
	else  $r['latest_rank_timestamp'] = "-";

	$r['title_short'] = strshorten($r['title'],50);
	return (object)$r;
}
function saveKeyword(){
	$id = (int)$_REQUEST['id'];
	$keyword = trim($_REQUEST['keyword']);
	$schedule = trim($_REQUEST['schedule']);

	if(!$keyword)err("Keyword cannot be empty!");
	if(!$schedule)err('Schedule is missing!');
	
	sql("UPDATE ranker_keywords SET keyword='$keyword', schedule='$schedule' WHERE id='$id'");
}
function loadKeyword(){
	$id = (int)$_REQUEST['id'];

	$sql = "SELECT k.*, r.rank AS rank, r.timestamp AS rank_timestamp, ro.rank as latest_pos, ro.timestamp AS latest_pos_timestamp FROM ranker_keywords AS k				
				LEFT JOIN ranker_keywords_ranks AS r ON r.id=(SELECT id FROM ranker_keywords_ranks WHERE keyword_id=k.id ORDER BY id DESC LIMIT 1)
				LEFT JOIN ranker_keywords_ranks AS ro ON ro.id=(SELECT id FROM ranker_keywords_ranks WHERE keyword_id=k.id AND rank!=r.rank ORDER BY id DESC LIMIT 1)
				WHERE k.id='$id'";
	$q = mysql_query($sql);
	$item = mysql_fetch_assoc($q);
	if(!$item)err("Item not found!");

	$item = format($item);
	json(array('item'=>$item));
}
function removeKeyword(){
	$id = (int)$_REQUEST['id'];
	mysql_query("DELETE FROM ranker_keywords_ranks WHERE keyword_id='$id'");
	mysql_query("DELETE FROM ranker_sessions WHERE keyword_id='$id'");	
	sql("DELETE FROM ranker_keywords WHERE id='$id'");
}
function importKeywords(){
	global $policies;
	$csv_mimetypes = array(
			'text/csv',
			'text/plain',
			'application/csv',
			'text/comma-separated-values',
			'application/excel',
			'application/vnd.ms-excel',
			'application/vnd.msexcel',
			'text/anytext',
			'application/octet-stream',
			'application/txt',
	);

	$error = '';
	if ($_FILES['file']['error'] !== UPLOAD_ERR_OK) $error = "Upload failed with error code " . $_FILES['file']['error'];
	else if ($info === FALSE) $error = "Unable to determine image type of uploaded file";
	else if (!in_array($_FILES['file']['type'], $csv_mimetypes)) $error = "Invalid file uploaded";
	else if ($_FILES['file']['error'] != 0) $error = "Unkown Error";

	if($error){
		if(file_exists($_FILES["file"]["tmp_name"]))unlink($_FILES["file"]["tmp_name"]);
		err($error);
	}

	$items = array();
	if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
		while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
			$items[] = $row[0];
		}
	}
	fclose($handle);
	unlink($_FILES["file"]["tmp_name"]);

	json(array('items'=>$items));
}
function rankKeyword(){
	$id = (int)$_REQUEST['id'];
	if(!$id)err("Keyword not found");

	$keyword = mysql_fetch_assoc(mysql_query("SELECT * FROM ranker_keywords WHERE id='$id'"));
	if(!$keyword)err("Keyword not found!");

	$sId = createRankerSession(array(
		'provider' 	=> '',		
		'auto'		=> 0,
		'kId' 		=> $keyword['id']		
	));
	if(!$sId)err("Unknown error");
	
	uselib('ranker');
	$l = new Ranker($sId);
	$l->start();
	$l->wait();

	json();
}

function getBadgesHistory(){
	$id = $_REQUEST['id'];
	
	$range = $_REQUEST['range'];
	if(!$range) $range = date('m/d/y',strtotime("-30 days")).' - '.date('m/d/y',strtotime("now"));
	$range = split(" - ",$range);
		
	$items = getRankerBadgeHistory($id,$range);
	
	$chart = (object)array(
		'series'	=> array()
	);
	
	if($items){
		$found = 0;
		$total = 0;
		$chart->series[] = array("Badge","Count");
		foreach($items as $r){
			$total = $r->total;
			$chart->series[] = array($r->title,$r->count);
			$found+=$r->count;
		}
		$chart->series[] = array("No Badge",$total-$found);
	}
	
	json($chart);
	return;
}
function getHistory(){
	$id = $_REQUEST['id'];
	
	$range = $_REQUEST['range'];
	if(!$range) $range = date('m/d/y',strtotime("-30 days")).' - '.date('m/d/y',strtotime("now"));
	$range = split(" - ",$range);
	
	//$items = getRankerRanks($id,$range);
	$lineItems = getRankerHistory($id,$range);		
	
	$chart = (object)array(
		'labels'	=> array(),
		'series'	=> array()
	);
	
	if($lineItems){		
		foreach($lineItems as $r){
			$date = strtotime(date('y-m-d H:i:s',strtotime($r->timestamp)));
			$rank=$r->rank;					
			
			$chart->labels[] = $date;
			$chart->series[] = array(date('m/d/y H:i:s',$date), (int)$rank);
				
		}
	}
	json($chart);
	return;
}

function getRankerBadgeLatest($kId,$range=false){
	$badges = array();

	$wheresql = array();
	$wheresql[] = "keyword_id='$kId'";
	$wheresql[] = "badge <> ''";

	$q = mysql_query("SELECT * FROM ranker_keywords_ranks WHERE ".implode(" AND ",$wheresql)." GROUP BY badge ORDER BY timestamp DESC");
	while($r = mysql_fetch_assoc($q)){
		$badge = trim($r['badge']);
		if(!$badge)continue;

		$badges[] = (object)array('title'=>$badge,'timestamp'=>date('m/d/Y',strtotime($r['timestamp'])));
	}

	return $badges;
}
function getRankerHistory($kId,$range=array(),$limit=false){
	$items = array();
	$limit = ($limit)?"LIMIT $limit":"";

	$wheresql = array();
	$wheresql[] = "r.keyword_id='$kId'";
	if($range)$wheresql[] = "r.timestamp BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' AND '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";

	$sql = "SELECT * FROM ranker_keywords_ranks AS r
				INNER JOIN (SELECT MAX(timestamp) latest FROM ranker_keywords_ranks WHERE keyword_id='$kId' GROUP BY date(`timestamp`)) AS t ON r.timestamp = t.latest
				WHERE ".implode(" AND ",$wheresql)."
				ORDER BY r.id ASC
				$limit";
	//t($sql);
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
		$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
		$r['date'] = date('m/d/Y',strtotime($r['timestamp']));
		$items[] = (object)$r;
	}
	
	return $items;
}
function getRankerRanks($kId,$range=array(),$limit=false){
	$items = array();
	$limit = ($limit)?"LIMIT $limit":"";

	$wheresql = array();
	$wheresql[] = "r.keyword_id='$kId'";
	if($range)$wheresql[] = "r.timestamp BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' AND '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";

	$sql = "SELECT * FROM ranker_keywords_ranks AS r
				WHERE ".implode(" AND ",$wheresql)."
				ORDER BY r.id ASC
				$limit";
	//t($sql);
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
		$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
		$r['date'] = date('m/d/Y',strtotime($r['timestamp']));
		$items[] = (object)$r;
	}
	return $items;
}


