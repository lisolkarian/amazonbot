<?php	
session_write_close();
usehelper("ajax::dispatch");

function getSystemStats(){
	$stats = array();
	
	$exec_loads = sys_getloadavg();
		
	$exec_cores = trim(shell_exec("grep -P '^processor' /proc/cpuinfo|wc -l"));
	//$stats['cpu'] = round($exec_loads[1]/($exec_cores + 1)*100, 2);	
	$stats['cpu'] = round($exec_loads[1], 2);
	
	$exec_free = explode("\n", trim(shell_exec('free')));		
	$get_mem = preg_split("/[\s]+/", $exec_free[2]);
	$used = $get_mem[2];
	$free = $get_mem[3];
	$total = $used + $free;
	
		
	$stats['mem'] = round($used/$total*100, 2);		
	$stats['mem_gb'] = number_format(round($used/1024/1024, 2), 2) . '/' . number_format(round($total/1024/1024, 2), 2);
		
	json(array('stats'=>$stats));
}
function getSessionsStats(){
	$stats = array();

	$phantoms = trim(shell_exec('pgrep phantom | wc -l'));
	list($pending) = mysql_fetch_array(mysql_query("SELECT count(id) FROM products_sessions WHERE status='pending'"));
	list($due) = mysql_fetch_array(mysql_query("SELECT count(id) FROM products_sessions WHERE status='pending' AND schedule<NOW()"));
		
	$stats['threads'] = $phantoms;
	$stats['pending'] = number_format($pending,0);
	$stats['due'] = number_format($due,0);

	json(array('stats'=>$stats));
}
function get24Stats(){
	$stats = array(
		'total'		=> 0,
		'hourly'	=> array(),
	);
	
	$q = mysql_query("SELECT COUNT(`id`) as num,DATE(`start`) as d,HOUR(`start`) as h FROM  `products_sessions` WHERE `start` > DATE_SUB(NOW(), INTERVAL 24 HOUR) GROUP BY DATE(`start`),HOUR(`start`)");
	while($r = mysql_fetch_assoc($q)){
		$key = date('m/d hA',strtotime("{$r['d']} {$r['h']}:00:00"));
		$stats['hourly'][$key] = number_format($r['num'],0);
		
		$stats['total']+=$r['num'];
	}	
	$stats['avg'] = number_format($stats['total']/24,2);
	$stats['total'] = number_format($stats['total'],0);	
	
	json(array('stats'=>$stats));
}
function getUsersStats(){
	$stats = array(
		'users'		=> array()
	);

	$q = mysql_query("SELECT COUNT( s.id ) AS num, s.user_id, s.status, u.username FROM `products_sessions` AS s
						LEFT JOIN users AS u ON u.id=s.user_id 
						WHERE s.`timestamp` > DATE_SUB(NOW(), INTERVAL 30 DAY) 
						GROUP BY s.user_id,s.status");
	while($r = mysql_fetch_assoc($q)){
		$uId = $r['user_id'];
		if(!$stats['users'][$uId])$stats['users'][$uId]=array('username'=>$r['username'],'sessions'=>array());
		
		$stats['users'][$uId]['sessions'][$r['status']] = $r['num'];		 
	}	
	json(array('stats'=>$stats));
}