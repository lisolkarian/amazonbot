<?php	
session_write_close();
usehelper("ajax::dispatch");

function getSalesrankHistory(){
	$id = $_REQUEST['id'];

	$range = $_REQUEST['range'];
	if(!$range) $range = date('m/d/y',strtotime("-30 days")).' - '.date('m/d/y',strtotime("now"));
	$range = split(" - ",$range);

	$wheresql = array();
	$wheresql[] = "product_id = '$id'";
	if($range)$wheresql[] = "timestamp BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' AND '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";
	
	$items = array();
	$sql = "SELECT DATE(timestamp), MAX(value) FROM products_salesrank WHERE ".implode(" AND ",$wheresql)." GROUP BY DATE(timestamp)";
	$q = mysql_query($sql);	
	while(list($date,$rank) = mysql_fetch_array($q)){
		$items[] = (object)array('date'=>$date, 'rank'=>$rank);
	}

	$chart = (object)array(
			'labels'	=> array(),
			'series'	=> array()
	);

	if($items){
		foreach($items as $r){
			$date = strtotime(date('Y-m-d',strtotime($r->date)));
			$rank=$r->rank;
				
			$chart->labels[] = $date;
			$chart->series[] = array(date('m/d/y H:i:s',$date), (int)$rank);

		}
	}
	json($chart);
	return;
}
function getRankerHistory($kId,$range=array(),$limit=false){
	$items = array();
	$limit = ($limit)?"LIMIT $limit":"";

	$wheresql = array();
	$wheresql[] = "r.keyword_id='$kId'";
	

	$sql = "SELECT * FROM ranker_keywords_ranks AS r
	INNER JOIN (SELECT MAX(timestamp) latest FROM ranker_keywords_ranks WHERE keyword_id='$kId' GROUP BY date(`timestamp`)) AS t ON r.timestamp = t.latest
	WHERE ".implode(" AND ",$wheresql)."
	ORDER BY r.id ASC
	$limit";
	//t($sql);
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
	$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
		$r['date'] = date('m/d/Y',strtotime($r['timestamp']));
		$items[] = (object)$r;
	}

	return $items;
	}
	
	
function removeProduct(){
	sql("DELETE FROM products WHERE id='{$_REQUEST['id']}' AND user_id='{$_SESSION['user']->id}'");
}
function loadProduct(){
	$id = (int)$_REQUEST['id'];
	
	$wheresql[] = "user_id='{$_SESSION['user']->id}'";
	$wheresql[] = "id='$id'";
	$q = mysql_query("SELECT * FROM products WHERE ".implode(" AND ",$wheresql)."");
	$product = mysql_fetch_assoc($q);
	
	if($product)$product = formatProduct($product);

	json(array('product'=>$product));	
}
function updateSalesrank(){
	$id = $_REQUEST['id'];
	list($asin) = mysql_fetch_array(mysql_query("SELECT asin FROM products WHERE id='$id' AND user_id='{$_SESSION['user']->id}'"));
	if(!$asin) err("ASIN not found for this product");
	
	uselib('amazon::ecs');
	$api = new ECS();
				
	$res = $api->updateSalesrank($asin);
	if(!$res) error("Unexpected Error");
	
	json();				
}
function addProduct(){
	$asin = $_REQUEST['asin'];
	if(!$asin) err("Please enter an ASIN number and try again.");
	
	list($found) = mysql_fetch_array(mysql_query("SELECT id FROM products WHERE asin='$asin' AND user_id='{$_SESSION['user']->id}'"));
	if($found) err("ASIN already exists in the system");
	
	uselib('amazon::ecs');
	$api = new ECS();
	
	$error = '';
	try{
		$p = $api->getProduct($asin,'ASIN');
	} catch (Exception $e) {
		$error = $e->getMessage();
	}
	if($error) err($error);
	if(!$p) err("Product not found");
	
	$title = mysql_real_escape_string($p->title);
	$img = mysql_real_escape_string($p->image);
	$rank = mysql_real_escape_string($p->rank);
	sql("INSERT INTO products (`asin`,`title`,`img`,`salesrank`,`user_id`) VALUES ('$asin','$title','$img','$rank','{$_SESSION['user']->id}')");
		
	return;
	
	
	
	/*
	$url = $_REQUEST['url'];
	$title = $_REQUEST['title'];
	$asin = $_REQUEST['asin'];
	$img = $_REQUEST['image'];
	
	if((!$title || !$asin) && $url){
	
		if (!filter_var($url, FILTER_VALIDATE_URL))err("Invalid URL");
	
		list($proxy) = mysql_fetch_array(mysql_query("SELECT proxy FROM proxies_rotating ORDER BY rand()"));

			
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,$url);
		curl_setopt($ch, CURLOPT_PROXY, $proxy);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');	
		$html = curl_exec($ch);
		curl_close($ch);
				
		//$html = file_get_contents($url);
				
		$matches=array();
		preg_match('/\{"hiRes":"(.*?)"/',$html,$matches);
		if($matches && $matches[1]) $img = $matches[1];
		
		$matches=array();
		$html = preg_replace("/\r|\n/","",$html);
		
		preg_match('/<span id="productTitle" class="a-size-large">(.*?)<\/span>/',$html,$matches);
		//t($matches);
		if($matches && $matches[1]) $title = trim($matches[1]);	
			
	//	$matches=array();
	// 	#preg_match('/desktopWidgetLazyLoader\.html\?asin=(.*?)"/',$html,$matches);	
	// 	#if($matches && $matches[1]) $asin = trim($matches[1]);
	
		$matches=array();
		preg_match('/dp\/(.*?)\//',$url,$matches);
		if($matches && $matches[1]) $asin = trim($matches[1]);		
	}

	if(!$asin || !$title)err("Unable to find product! Make sure the product URL has the asin in this format ../dp/AsinGoesHere/..");
	
	$title = mysql_real_escape_string($title);
	sql("INSERT INTO products (`asin`,`title`,`img`,`user_id`) VALUES ('$asin','$title','$img','{$_SESSION['user']->id}')");
	*/
}
function getProducts(){	
	$items = getUserProducts();
	//t($items);
	json(array('products'=>$items));
}