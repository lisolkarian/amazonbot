<?php	
session_write_close();
usehelper("ajax::dispatch");

function selectKeywordsFromUpload(){
	global $policies;
	$csv_mimetypes = array(			
			'text/plain',
			'text/anytext',
			'application/octet-stream',
			'application/txt',
	);

	$error = '';
	if ($_FILES['file']['error'] !== UPLOAD_ERR_OK) $error = "Upload failed with error code " . $_FILES['file']['error'];	
	else if (!in_array($_FILES['file']['type'], $csv_mimetypes)) $error = "Invalid file uploaded";
	else if ($_FILES['file']['error'] != 0) $error = "Unkown Error";

	if($error){
		if(file_exists($_FILES["file"]["tmp_name"]))unlink($_FILES["file"]["tmp_name"]);
		err($error);
	}

	$items = array();
	if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
		while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
			$items[] = $row[0];
		}
	}
	fclose($handle);
	unlink($_FILES["file"]["tmp_name"]);
	
	$kIds = array();
	foreach($items as $kw){
		$kw = mysql_real_escape_string($kw);
		list($kId) = mysql_fetch_array(mysql_query("SELECT id from products_keywords WHERE term = '$kw'"));
		
		$kIds[] = $kId;
	}	
	$kIds = array_values(array_unique(array_filter($kIds)));			
	
	json(array('items'=>$kIds));
}
function cancelCampaign(){
	uselib('launcher');	
	$cId = $_REQUEST['cId'];
	
	$ids = array();	
	$q = mysql_query("SELECT * FROM products_sessions WHERE campaign_id='$cId' AND (status='Pending' OR status='Working')");
	while($r = mysql_fetch_assoc($q)){
		$id = $r['id'];
		
		$ids[] = $id;
		$l = new Launcher($id);
		$l->kill();		
	}	
	sql("UPDATE products_sessions SET status='Stopped', error='Stopped by user' WHERE id IN ('".implode("','",$ids)."')"); 
}
function createCampaigns(){
	$num = (int)$_REQUEST['sessions'];
	$start = $_REQUEST['start'];
	$period = (int)$_REQUEST['period'];
	$provider = $_REQUEST['provider'];
	$pageStart = (int)$_REQUEST['page_start'];
	$pageEnd = (int)$_REQUEST['page_end'];
	$kIds = $_REQUEST['kIds'];
	
	if($pageStart<0)$pageStart=0;
	if($pageEnd<0)$pageEnd=0;
	if($pageStart > $pageEnd){ $tmp = $pageStart; $pageStart=$pageEnd; $pageEnd=$tmp; }
	
	if(!$kIds || empty(array_filter($kIds))) err('You did not select any keywords');
	$kIds = array_unique(array_filter($kIds));
	
	$campaigns = array();
	foreach($kIds as $kId){		
		$keyword = mysql_fetch_assoc(mysql_query("SELECT * FROM products_keywords WHERE id='$kId'"));		
		$product = mysql_fetch_assoc(mysql_query("SELECT * FROM products WHERE id='{$keyword['product_id']}'"));		

		$error = '';
		if(!$product || !$keyword ) $error = "Missing search term or product!";
		if(!$num) $error = "Enter a number of sessions and try again.";
		if(!$product['asin'] || !$product['title'] ) $err = "Missing product title or ASIN!";
		
		$campaigns[] = (object)array(
			'keyword'		=> $keyword,
			'product'		=> $product,
			'sessions'		=> array(),
			'error'			=> $error
		);
	}	
	
	$sessionCount = 0;
	foreach($campaigns as $i=>$campaign){
		if($campaign->error) continue;
		
		$keyword = $campaign->keyword;
		$pId = $campaign->product['id'];
		mysql_query("INSERT INTO products_campaigns (`user_id`,`product_id`) VALUES ('{$_SESSION['user']->id}','$pId')");
		$error = mysql_error();
		if($error) { $campaigns[$i]->error = "System error!"; continue; }
		$cId = mysql_insert_id();				

		for($j=0;$j<$num;$j++){
			$updateSql = array();
	
			switch($provider){
				case 'RP':
					$proxy = getProxyRotating();
					break;
				case 'Microleaves':
					$proxy = getProxyMicroleaves();
					break;
				default:
					$proxy = getProxy();
					break;
			}
			$useragent = getUseragent();
	
			$seconds = mt_rand(10,$period*60*60);			
			if($start){
				$schedule = date('Y-m-d H:i:s',strtotime("+{$seconds} seconds",strtotime($start)));
			}
			else{
				$schedule = date('Y-m-d H:i:s',strtotime("+{$seconds} seconds"));
			}
	
			$updateSql[] = "`proxy_provider`='".$provider."'";
			$updateSql[] = "`proxy`='".$proxy->proxy."'";
			$updateSql[] = "`useragent`='$useragent'";
			$updateSql[] = "`search_term`='$keyword[term]'";
			$updateSql[] = "`product`='".mysql_real_escape_string($product['title'])."'";
			$updateSql[] = "`asin`='$product[asin]'";			
			$updateSql[] = "`schedule`='".$schedule."'";
			
			$updateSql[] = "`page_start`='".$pageStart."'";
			$updateSql[] = "`page_end`='".$pageEnd."'";
	
			$updateSql[] = "`user_id`='".$_SESSION['user']->id."'";
			$updateSql[] = "`product_id`='".$product['id']."'";
			$updateSql[] = "`campaign_id`='".$cId."'";
			$updateSql[] = "`term_id`='".$keyword['id']."'";						
	
			mysql_query("INSERT INTO products_sessions SET ".implode(",",$updateSql));
			
			$sId = mysql_insert_id();
			if($sId){
				$sessionCount++;
				$campaigns[$i]->sessions[] = $sId; 
			}
		}
	}
	
	json(array('campaigns'=>$campaigns, 'campaignsCount'=>count($campaigns), 'sessions'=>$sessionCount));
}