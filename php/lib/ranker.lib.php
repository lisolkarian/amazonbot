<?php
uselib('process');

class Ranker{ 
	private $timeout,
			$script,
			$start,
			$pid,
			$sid,
			$vars; 		   
    public function __construct($sid){    	
    	$this->sid = $sid;
    	$this->vars = $this->getVars();
    	$this->pid = (int)$this->vars->pid;
    	$this->timeout = 60*20*1;
    	$this->script = $GLOBALS['system']['phantom_path'] . '/scraper.js';
    	$this->auto = ($auto)?1:0;
    }    
    function start($debug=false){
    	$vars = $this->vars;           	
    	$cmd = "/usr/bin/phantomjs '{$this->script}' '".json_encode($this->vars)."'";
       	    	
    	if($debug)die($cmd);
     	        	
    	$process = new Process($cmd);
    	$process->start();
    	$this->start = microtime(true);    	
    	$this->pid = $process->getPid();
    	
    	$cmd = base64_encode($cmd);
    	mysql_query("UPDATE ranker_sessions SET `start`=NOW(), `status`='Working', `pid`='{$this->pid}',`cmd`='$cmd' WHERE id='{$this->sid}'");    	

    	//$this->wait();      	
    }
    function getVars(){       	 
    	$data = mysql_fetch_assoc(mysql_query("SELECT * FROM ranker_sessions WHERE id='{$this->sid}'"));
    	
    	unset($data['cmd']);
    	unset($data['product']);
    	unset($data['error']);
    	
    	$data['term_id'] = $data['keyword_id'];    	
    	$data['ranker'] = 1;
    	
    	return (object)$data;
    }
    
    function wait($debug=false){
    	if(!$this->start)$this->start = microtime(true);
    	do{
    		$time = abs(microtime(true) - $this->start);
    		sleep(1);			    		
    		
    		if($debug)print "PID: ".$this->pid.", Waiting: $time, Timeout: ".$this->timeout."\n";
    	}while($time<$this->timeout && $this->isActive());
    	
    	if($this->isActive()) $this->stop();    	    	
    }
    function isActive(){
    	return self::checkPid($this->pid);
    }
    public function stop(){    	
    	$process = new Process();
    	$process->setPid($this->pid);
    	$process->stop();
    		
    	if(!$this->isActive())mysql_query("UPDATE ranker_sessions SET `status`='Error', `error`='Timeout' WHERE id='{$this->sid}'");    	
    }
    public function kill(){
    	if(!$this->pid)return;
    	$process = new Process();    	
    	$process->setPid($this->pid);
    	$process->stop();        	
    }
    static public function checkPid($pid){
    	$process = new Process();
    	$process->setPid($pid);
    	$status = $process->status();    	
    	$status = ($status==true)?1:0;
    	return $status;
    }
}