<?php
define('_PIC_PATH_', $GLOBALS['system']['tmp_path']);
require_once( $GLOBALS['system']['path'] . $GLOBALS['system']['lib_path'] . '/decaptcha/src/api_consts.inc.php' );
require_once( $GLOBALS['system']['path'] . $GLOBALS['system']['lib_path'] . '/decaptcha/src/poster.inc.php' );

Class Decaptcha{
	private $settings;
	
	static public function solve($pic){
		$this->settings = (object)array(
			'host'			=> "poster.de-captcher.com",
			'port'			=> 80,
			'username'		=> "joseph4176",
			'password'		=> "gofishgo4176"			
		);		
		$time1 = microtime(true);
		$return = poster_curl( $this->settings->host, $this->settings->port, $this->settings->username, $this->settings->password, $pic , NULL, ptUNSPECIFIED );
		$time2 = microtime(true);
		$benchmark = $time2 - $time1;
		
		$error = '';		
		if( $return === FALSE ) {
			$error = "We've got an error, please check parameters";
		} else if( is_int( $return ) ) {
			if( $return < 0 ) {
				$error = "Can't process ";
				switch( $return ) {
					case ccERR_GENERAL:
						$error .= "general internal error";
						break;
					case ccERR_STATUS:
						$error .= "status is not correct";
						break;
					case ccERR_NET_ERROR:
						$error .= "network data transfer error";
						break;
					case ccERR_TEXT_SIZE:
						$error .= "text is not of an appropriate size";
						break;
					case ccERR_OVERLOAD:
						$error .= "server's overloaded";
						break;
					case ccERR_BALANCE:
						$error .= "not enough funds to complete the request";
						notify($error);
						break;
					case ccERR_TIMEOUT:
						$error .= "request timed out";
						break;
					case ccERR_BAD_PARAMS:
						$error .= "provided parameters are not good for this function";
						break;
					default:
						$error .= "unknown error";
						break;
				}
			} else if( $return < 10 ) {
				$error = "Malformed output data";
			} else {
				$error = "HTTR ERROR $return";
			}
		} else {
			$text = $return;
		}
		
		if($error) return (object)array('error'=>$error);		
		else return (object)array('text'=>$text,'benchmark'=>(int)$benchmark);				
	}
}