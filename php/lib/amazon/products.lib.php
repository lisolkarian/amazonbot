<?php
uselib('amazon::amazon');

class amazonProducts extends amazon {
	var $request,$response;

	public function __construct($uId=0){		
		parent::__construct($uId);
	}		
	
	public function fetchLowestPrice($id,$type='sku'){
		$this->response = false;
		
		if($type == 'sku'){ $res = $this->priceSKU($id);  if($res) $this->response = array('raw'=>$res,'offers'=> $res->GetLowestPricedOffersForSKUResult); }		
		else if($type == 'asin'){ $res = $this->priceASIN($id); if($res) $this->response = array('raw'=>$res,'offers'=> $res->GetLowestPricedOffersForASINResult); }

		return ($this->response)?true:false;
	}
	public function getOffersCount(){
		if(!$this->response) return false;
						
		$count = 0;
		$offers = $this->response['offers']->Summary->NumberOfOffers->OfferCount;		
		if($offers){
			foreach($offers as $o){				
				if($o->condition == 'new'){
					$count = (int)$o->OfferCount;
				}
			}
		}
		
		return $count;
	}
	public function getLowestPrice(){
		if(!$this->response) return false;
				
		$price = 0;
		$offers = $this->response['offers']->Offers->Offer;			
		if($offers){												
			foreach($offers as $o){
				$oprice = (float)$o->ListingPrice->Amount + (float)$o->Shipping->Amount;				
				
				$omine = false;
				if ($o instanceof MarketplaceWebServiceProducts_Model_SKUOfferDetail)$omine = ($o->MyOffer == "true")?true:false;				
		
				#t($omine.': '.$oprice,1);
		
				if($omine){ 
					//if(!$price)$price=$oprice; 
				}
				else{
					if(!$price || $oprice < $price)$price = $oprice;
				}		
			}									
		}		
		return $price;
	}
	public function priceASIN($asin){
		$this->request = new MarketplaceWebServiceProducts_Model_GetLowestPricedOffersForASINRequest();
		$this->request->setSellerId($this->settings["amazon_seller_id"]);
		$this->request->setMarketplaceId($this->settings["amazon_marketplace_id"]);
		$this->request->setMWSAuthToken($this->settings["amazon_mws_auth_token"]);
		$this->request->setASIN($asin);
		$this->request->setItemCondition('New');
	
		$response = $this->productsService->GetLowestPricedOffersForASIN($this->request);
		
		return $response;
	}
	public function priceSKU($sku){		
		$this->request = new MarketplaceWebServiceProducts_Model_GetLowestPricedOffersForSKURequest();
		$this->request->setSellerId($this->settings["amazon_seller_id"]);
		$this->request->setMarketplaceId($this->settings["amazon_marketplace_id"]);
		$this->request->setMWSAuthToken($this->settings["amazon_mws_auth_token"]);
		$this->request->setSellerSKU($sku);
		$this->request->setItemCondition('New');
		
		$response = $this->productsService->GetLowestPricedOffersForSKU($this->request);								
		
		return $response;
	}	
	
	//Publish
	function getFeedResult($fId){ 
        $config = array(
            'ServiceURL' => "https://mws.amazonservices.com",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'MaxErrorRetry' => 3,
        );
		
        $service = new MarketplaceWebService_Client(
                $this->settings["amazon_access_key"], $this->settings["amazon_secret_key"], $config, $this->settings["amazon_application_name"], $this->settings["amazon_application_version"]);
        $fileHandle = @fopen('php://memory', 'rw+');
        $parameters = array(
            'Merchant' => $this->settings["amazon_seller_id"],
            'FeedSubmissionId' => $fId,
            'FeedSubmissionResult' => $fileHandle,
            'MWSAuthToken' => $this->settings["amazon_mws_auth_token"], // Optional
        );
        $request = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);

		try {
    		$response = $service->getFeedSubmissionResult($request);	
			rewind($fileHandle);       
			$responseStr = stream_get_contents($fileHandle);										
			$responseXML = new SimpleXMLElement($responseStr);				
    	} catch (MarketplaceWebService_Exception $ex) {
    		//echo("Caught Exception: " . $ex->getMessage() . "\n");
    		//echo("Response Status Code: " . $ex->getStatusCode() . "\n");
    		//echo("Error Code: " . $ex->getErrorCode() . "\n");
    		//echo("Error Type: " . $ex->getErrorType() . "\n");
    		//echo("Request ID: " . $ex->getRequestId() . "\n");
    		//echo("XML: " . $ex->getXML() . "\n");
    		//echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");    		    		
    		$response = false;
    	}	
		if($response){
			$error = array();		
			foreach($responseXML->Message->ProcessingReport->Result as $e){
				$errors[] = (string)$e->ResultDescription;
			}			
			if(!empty($errors)){ throw new Exception(implode("\n",$errors),404); }
		}
		
        return (int)$responseXML->Message->ProcessingReport->ProcessingSummary->MessagesSuccessful;    
	}
	function setQty($data){	
		$id = $this->updateQty($data);
		return $id;
	}
	function setPrice($data){	
		$id = $this->updateMin($data);
		return $id;
	}
	function overrideShipping($data){
		if(!$data)return false;
    		 
    	if(!is_array($data))$data = array($data);
    		 
    	$xml = array();
    	foreach($data as $i=>$obj){
    		$i++;
			$xml[] = "<Message>
						<MessageID>{$i}</MessageID>
						<OperationType>Update</OperationType>
						<Override>
							<SKU>{$obj['sku']}</SKU>
							<ShippingOverride>
								<ShipOption>Std Cont US Street Addr</ShipOption>
								<Type>Exclusive</Type>
								<ShipAmount currency=\"USD\">0.00</ShipAmount>
							</ShippingOverride>
						</Override>
					</Message>";    		
    	}    	
    	$xml = implode("\n",$xml);	 		     	        	
    		 		
    	$config = array(
    		'ServiceURL' => "https://mws.amazonservices.com",
    		'ProxyHost' => null,
    		'ProxyPort' => -1,
    		'MaxErrorRetry' => 3,
    	);
        $service = new MarketplaceWebService_Client($this->settings["amazon_access_key"], $this->settings["amazon_secret_key"], $config, $this->settings["amazon_application_name"], $this->settings["amazon_application_version"]);
            	
    	$feed = <<<EOD
<?xml version="1.0" encoding="utf-8" ?>
	<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amznenvelope.xsd">
	<Header>
		<DocumentVersion>1.02</DocumentVersion>
		<MerchantIdentifier>{$this->settings["merchant_identifier"]}</MerchantIdentifier>
	</Header>
	<MessageType>Override</MessageType>
	{$xml}
	</AmazonEnvelope>
EOD;

    	$feedHandle = @fopen('php://temp', 'rw+');
    	fwrite($feedHandle, $feed);
    	rewind($feedHandle);
    	$parameters = array(
    			'Merchant' => $this->settings["amazon_seller_id"],
    			'MarketplaceIdList' => array("Id" => $this->settings["amazon_marketplace_id"]),
    			'FeedType' => '_POST_PRODUCT_OVERRIDES_DATA_',
    			'FeedContent' => $feedHandle,
    			'PurgeAndReplace' => false,
    			'ContentMd5' => base64_encode(md5(stream_get_contents($feedHandle), true)),
    			'MWSAuthToken' => $this->settings["amazon_mws_auth_token"], // Optional
    	);
    	rewind($feedHandle);
    	$request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
    	
    	try {
    		$response = $service->submitFeed($request);    		    		
    	} catch (MarketplaceWebService_Exception $ex) {
    		echo("Caught Exception: " . $ex->getMessage() . "\n");
    		echo("Response Status Code: " . $ex->getStatusCode() . "\n");
    		echo("Error Code: " . $ex->getErrorCode() . "\n");
    		echo("Error Type: " . $ex->getErrorType() . "\n");
    		echo("Request ID: " . $ex->getRequestId() . "\n");
    		echo("XML: " . $ex->getXML() . "\n");
    		echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");    		    		
    		$response = false;
    	}
    	 
    	if($response){
    		$submitFeedResult = $response->getSubmitFeedResult();
    		$feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
    		$submission_id = $feedSubmissionInfo->getFeedSubmissionId();
    		sleep(1);
    	}
    	else{    	
    		$submission_id = 0;
    	}
       	  		
    	return $submission_id;		
	}
	function publishProduct($data) {
		if(!$data)return false;
    		 
    	if(!is_array($data))$data = array($data);
    		 
    	$xml = array();
    	foreach($data as $i=>$obj){
    		$i++;
			$xml[] = "<Message>
						<MessageID>{$i}</MessageID>
						<OperationType>Update</OperationType>
						<Product>
							<SKU>{$obj['sku']}</SKU>
							<StandardProductID>
								<Type>ASIN</Type>
								<Value>{$obj['asin']}</Value>
							</StandardProductID>
						</Product>
					</Message>";    		
    	}    	
    	$xml = implode("\n",$xml);	
    		 		
    	$config = array(
    		'ServiceURL' => "https://mws.amazonservices.com",
    		'ProxyHost' => null,
    		'ProxyPort' => -1,
    		'MaxErrorRetry' => 3,
    	);
        $service = new MarketplaceWebService_Client($this->settings["amazon_access_key"], $this->settings["amazon_secret_key"], $config, $this->settings["amazon_application_name"], $this->settings["amazon_application_version"]);
            	
    	$feed = <<<EOD
<?xml version="1.0" ?>
	<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amznenvelope.xsd">
	<Header>
		<DocumentVersion>1.01</DocumentVersion>
		<MerchantIdentifier>{$this->settings["merchant_identifier"]}</MerchantIdentifier>
	</Header>
	<MessageType>Product</MessageType>
	<PurgeAndReplace>false</PurgeAndReplace>
	{$xml}						
</AmazonEnvelope>
EOD;

    	$feedHandle = @fopen('php://temp', 'rw+');
    	fwrite($feedHandle, $feed);
    	rewind($feedHandle);
    	$parameters = array(
    			'Merchant' => $this->settings["amazon_seller_id"],
    			'MarketplaceIdList' => array("Id" => $this->settings["amazon_marketplace_id"]),
    			'FeedType' => '_POST_PRODUCT_DATA_',
    			'FeedContent' => $feedHandle,
    			'PurgeAndReplace' => false,
    			'ContentMd5' => base64_encode(md5(stream_get_contents($feedHandle), true)),
    			'MWSAuthToken' => $this->settings["amazon_mws_auth_token"], // Optional
    	);
    	rewind($feedHandle);
    	$request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
    	
    	try {
    		$response = $service->submitFeed($request);    		    		
    	} catch (MarketplaceWebService_Exception $ex) {
    		echo("Caught Exception: " . $ex->getMessage() . "\n");
    		echo("Response Status Code: " . $ex->getStatusCode() . "\n");
    		echo("Error Code: " . $ex->getErrorCode() . "\n");
    		echo("Error Type: " . $ex->getErrorType() . "\n");
    		echo("Request ID: " . $ex->getRequestId() . "\n");
    		echo("XML: " . $ex->getXML() . "\n");
    		echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");    		    		
    		$response = false;
    	}
    	 
    	if($response){
    		$submitFeedResult = $response->getSubmitFeedResult();
    		$feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
    		$submission_id = $feedSubmissionInfo->getFeedSubmissionId();
    		sleep(1);
    	}
    	else{    	
    		$submission_id = 0;
    	}
       	  		
    	return $submission_id;
    }
	
	
	
	
}