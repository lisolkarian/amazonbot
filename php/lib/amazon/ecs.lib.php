<?php
//include_once "ECS/lib/AmazonECS.class.php";

Class ECS {
	private $settings;
	public function __construct(){		
		$this->settings = (object)array(
			'key' 		=> $GLOBALS['SETTINGS']['amazon_aws_key'],
			'secret' 	=> $GLOBALS['SETTINGS']['amazon_aws_secret'],
			'tag' 		=> $GLOBALS['SETTINGS']['amazon_associate_tag'],
			'endpoint' 	=> "webservices.amazon.com",
			'uri' 		=> "/onca/xml"
		);
	}
	
	public function updateSalesrank($asin){					
		try{
			$p = $this->getProduct($asin,'ASIN');
		} catch (Exception $e) {
			$error = $e->getMessage();
		}
						
		//if($error) err($error);
		//if(!$p) err("Product not found");
		if($error || !$p) return false;
	
		$rank = mysql_real_escape_string($p->rank);
				
		mysql_query("UPDATE products SET salesrank='$rank' WHERE asin='$asin'");
		
		$q = mysql_query("SELECT * FROM products WHERE asin='$asin'");
		while($r = mysql_fetch_assoc($q)){
			$userId = $r['user_id'];
			$pId = $r['id'];
			
			mysql_query("INSERT products_salesrank SET product_id='$pId', user_id='$userId', `value`='$rank'");
		}
		return true;
	}
	
	public function findASIN($cat,$brand,$keyword){
		//Use getProduct instead instead
		$params = array(
			"Service" => "AWSECommerceService",
			"Operation" => "ItemSearch",
			"AWSAccessKeyId" => $this->settings->key,
			"AssociateTag" => $this->settings->tag,
			"SearchIndex" => $cat,
			"Keywords" => $keyword,
			"Brand" => $brand,
			"Sort" => "price",
			"ResponseGroup" => "ItemAttributes,OfferSummary,Offers,OfferFull,Images,Reviews,SalesRank,Images",
			"Condition" => "New"				
		);			
		$res = $this->request($params);			
		if($res->Error) throw new Exception('ASIN Error: '.$res->Error->Message);
						
		$product = new ecsProduct($res);				
		return $product;				
	}
	public function findByTitle($cat,$brand,$title){
		//Use getProduct instead instead
		$params = array(
			"Service" => "AWSECommerceService",
			"Operation" => "ItemSearch",
			"AWSAccessKeyId" => $this->settings->key,
			"AssociateTag" => $this->settings->tag,
			"SearchIndex" => $cat,
			"Title" => $title,
			"Brand" => $brand,
			"Sort" => "price",
			"ResponseGroup" => "ItemAttributes,OfferSummary,Offers,OfferFull,Images,Reviews,SalesRank,Images",
			"Condition" => "New"				
		);			
		$res = $this->request($params);			
		if($res->Error) throw new Exception('ASIN Error: '.$res->Error->Message);
						
		$product = new ecsProduct($res);				
		return $product;				
	}
	public function getProduct($id,$type,$cat=''){
		$params = array(
			"Service" => "AWSECommerceService",
			"Operation" => "ItemLookup",
			"AWSAccessKeyId" => $this->settings->key,
			"AssociateTag" => $this->settings->tag,
			"ItemId" => $id,
			"IdType" => $type,			
			"Condition" => 'New',			
			"ResponseGroup" => "ItemAttributes,OfferSummary,Offers,OfferFull,OfferListings,SalesRank,Images,Reviews",			
		);
						
		if($type=='ASIN'){
			
		}
		else{
			$params["SearchIndex"] = $cat;
		}
		
		$res = $this->request($params);		
		if($res->Error) throw new Exception('AWS Error: '.$res->Error->Message);
		if($res->Items->Request->Errors->Error->Message) throw new Exception('AWS Error: '.$res->Items->Request->Errors->Error->Message);
									
		$product = new ecsProduct($res);				
		return $product;				
	}
	private function prepareRequest($params){
		if (!isset($params["Timestamp"])) {$params["Timestamp"] = gmdate('Y-m-d\TH:i:s\Z');}
		ksort($params);
		
		// Generate the canonical query
		$pairs = array();
		foreach ($params as $key => $value) array_push($pairs, rawurlencode($key)."=".rawurlencode($value));
		$canonical_query_string = join("&", $pairs);
		
		// Generate the string to be signed
		$string_to_sign = "GET\n".$this->settings->endpoint."\n".$this->settings->uri."\n".$canonical_query_string;
		
		// Generate the signature required by the Product Advertising API
		$signature = base64_encode(hash_hmac("sha256", $string_to_sign, $this->settings->secret, true));

		// Generate the signed URL
		$request_url = 'http://'.$this->settings->endpoint.$this->settings->uri.'?'.$canonical_query_string.'&Signature='.rawurlencode($signature);

		return $request_url;
	}
	private function request($params){
		$url = $this->prepareRequest($params);			
		$res = curl_get($url);
		
		try{
			if($res)$res = new SimpleXMLElement($res);
		}catch (Exception $e){}
		
		return $res;
	}	
}

Class ecsProduct{
	private $data;
	public function __construct($obj){
		$this->data = $obj;
	}
	public function __get($k){
		switch($k){
			case 'title':
				return reset($this->data->Items->Item->ItemAttributes->Title);
			case 'rank':
				return (int)$this->data->Items->Item->SalesRank;
			case 'image':
				return reset($this->data->Items->Item->LargeImage->URL);
			case 'price':
				//$p = (int)$this->data->Items->Item->OfferSummary->LowestNewPrice->Amount;
				$p = (int)$this->data->Items->Item->Offers->Offer->OfferListing->Price->Amount;
				return $p/100;
				break;
			case 'offers':
				return (int)$this->data->Items->Item->OfferSummary->TotalNew;
			case 'asin':				
				return (string)$this->data->Items->Item->ASIN;
				break;
			case 'url':				
				return $url = (string)$this->data->Items->Item->DetailPageURL;
				break;
			case 'reviews':
				return reset($this->data->Items->Item->CustomerReviews->IFrameURL);
				break;
			case 'hasReviews':
				return reset($this->data->Items->Item->CustomerReviews->HasReviews);
				break;				
			default:							
				return $this->data->{$k};
				break;
		}
	}
}