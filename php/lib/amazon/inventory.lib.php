<?php
uselib('amazon::amazon');

class amazonInventory extends amazon {
   var $settings;
  
    public function __construct($uId){
		$this->settings = array(
			'type'	=> '_GET_MERCHANT_LISTINGS_DATA_'
		);
		parent::__construct($uId);				
	}
	static public function getInventoryTitles(){
		$titles = array();
		
		$q = mysql_query("SELECT * FROM amazon_inventory WHERE user_id='".Users::getOwnerId()."'");
		while($r = mysql_fetch_assoc($q)){
			$titles[$r['title']] = $r;
		}
		return array_keys($titles);
	}
	public function updateActiveInventory(){
		$r = $this->getPendingRequest();
		
		if($r){
			if($r->report_id){
				if($r->status != 'processed'){
					$this->parseReport($r->report_id);
				}
				else if(strtotime($r->timestamp)<strtotime('-1 day')){
					$this->removeRequest($r->report_id);
					$this->submitRequest();
				}
			}
			else{
				$reportId = $this->checkRequest($r->request_id);				
				if($reportId){
					$this->parseReport($reportId);
				}										
			}
		}
		else{
			$this->submitRequest();
		}		
	}
	private function removeRequest($rId){
		mysql_query("DELETE FROM amazon_inventory_reports WHERE report_id='$rId'");
	}
	private function getPendingRequest(){
		$q = mysql_query("SELECT * FROM amazon_inventory_reports WHERE user_id='{$this->userId}'");
		$r = mysql_fetch_assoc($q);
		
		return (object)$r;		
	}
	private function submitRequest(){
		$rId = $this->requestReport($this->settings['type']);		
		if($rId){
			mysql_query("INSERT INTO amazon_inventory_reports (`user_id`,`request_id`,`status`) VALUES ('{$this->userId}','$rId','pending')");
			return true;
		}
		return false;
	}
	private function parseReport($rId){;
		mysql_query("UPDATE amazon_inventory_reports SET status='processed' WHERE report_id='$rId'");
		
		$report = $this->getReport($rId);
		$csv = preg_split("/\n\r|\r\n|\n|\r/",$report); //str_getcsv($report);		
		
		$items = array();
		if($csv){		
			mysql_query("DELETE FROM amazon_inventory WHERE user_id='{$this->userId}'");
			$cols = str_getcsv(array_shift($csv),"\t");			
			foreach($csv as $row){
				$data = array();
				foreach(preg_split("/\t/",$row) as $i=>$v) $data[$cols[$i]] = $v;							
											
				$raw = base64_encode($row);
				$serialized = base64_encode(serialize($data));
				$title = mysql_real_escape_string($data['item-name']);				
				mysql_query("INSERT INTO amazon_inventory (`user_id`,`title`,`raw`,`serialized`) VALUES ('{$this->userId}','$title','$raw','$serialized')");								
			}
		}		
	}	
	private function checkRequest($rId){
		$response = $this->getReportRequests($rId);		
		
		if ($response->isSetGetReportRequestListResult()) {                     
			$getReportRequestListResult = $response->getGetReportRequestListResult();
                    
			$nextToken = ($getReportRequestListResult->isSetNextToken())?$getReportRequestListResult->getNextToken():false;
			//Use token to navigate pages. No needed at time of implementation.
                    
			$reportRequestInfoList = $getReportRequestListResult->getReportRequestInfoList();
			foreach ($reportRequestInfoList as $reportRequestInfo) {                        
				$status = ($reportRequestInfo->isSetReportProcessingStatus())?$reportRequestInfo->getReportProcessingStatus():false;
				$reportId = ($reportRequestInfo->isSetGeneratedReportId())?$reportRequestInfo->getGeneratedReportId():false;
				if($status == '_DONE_'){
					mysql_query("UPDATE amazon_inventory_reports SET `report_id`='$reportId',`status`='ready' WHERE request_id='$rId'");
					return $reportId;
				}
				else
					return false;
			}
		}
		return false;
	}
}