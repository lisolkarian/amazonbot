<?php

uselib('giftcards::giftcards');

include_once __DIR__ . "/MarketplaceWebServiceOrders/Exception.php";
include_once __DIR__ . "/MarketplaceWebServiceOrders/Client.php";
include_once __DIR__ . "/MarketplaceWebServiceOrders/Model.php";
include_once __DIR__ . "/MarketplaceWebService/Interface.php";
include_once __DIR__ . "/MarketplaceWebService/Exception.php";
include_once __DIR__ . "/MarketplaceWebService/RequestType.php";
include_once __DIR__ . "/MarketplaceWebService/Client.php";
include_once __DIR__ . "/MarketplaceWebService/Model.php";

include_once __DIR__ . "/MarketplaceWebServiceProducts/Exception.php";
include_once __DIR__ . "/MarketplaceWebServiceProducts/Client.php";
include_once __DIR__ . "/MarketplaceWebServiceProducts/Model.php";

$includefiles = scandir(__DIR__ . "/MarketplaceWebServiceOrders/Model/");
foreach ($includefiles as $file) {
    if ($file != "." && $file != "..") {
        include_once __DIR__ . "/MarketplaceWebServiceOrders/Model/$file";
    }
}

$includefiles = scandir(__DIR__ . "/MarketplaceWebService/Model/");
foreach ($includefiles as $file) {
    if ($file != "." && $file != "..") {
        include_once __DIR__ . "/MarketplaceWebService/Model/$file";
    }
}

$includefiles = scandir(__DIR__ . "/MarketplaceWebServiceProducts/Model/");
foreach ($includefiles as $file) {
	if ($file != "." && $file != "..") {
		include_once __DIR__ . "/MarketplaceWebServiceProducts/Model/$file";
	}
}

class amazon {

    private $service;
    private $request;
	public $settings;
	public $userId;
	public $init;
	private static $defaultPortalStatus = 'Unconfirmed';

    function __construct($userId=0) {			
		if(!$userId)$userId=Users::getOwnerId();
		$this->userId = $userId;
			
		$this->settings = Users::getUserSettings($this->userId);		

		$this->init = true;
		try{		
			$this->serviceLogin();
			$this->setupRequest();									
		}catch (Exception $e) { $this->init = false; }
    }

    function serviceLogin() {
        $config = array(
            'ServiceURL' => $this->settings["amazon_service_url"],
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 3,
        );
		$generalConfig = array(
            'ServiceURL' => $this->settings["amazon_general_service_url"],
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'ProxyUsername' => null,
            'ProxyPassword' => null,
            'MaxErrorRetry' => 3,
        );
		$productsConfig = array(
			'ServiceURL' => $this->settings["amazon_products_url"],
			'ProxyHost' => null,
			'ProxyPort' => -1,
			'ProxyUsername' => null,
			'ProxyPassword' => null,
			'MaxErrorRetry' => 3,
		);
		
		$this->productsService = new MarketplaceWebServiceProducts_Client(
				$this->settings["amazon_access_key"], //access key
				$this->settings["amazon_secret_key"], //secret key
				$this->settings["amazon_application_name"], //application name
				$this->settings["amazon_application_version"], //application version
				$productsConfig);
		
        $this->service = new MarketplaceWebServiceOrders_Client(
                $this->settings["amazon_access_key"], //access key
                $this->settings["amazon_secret_key"], //secret key
                $this->settings["amazon_application_name"], //application name
                $this->settings["amazon_application_version"], //application version
                $config);
			
		$this->generalService = new MarketplaceWebService_Client(
                $this->settings["amazon_access_key"], //access key
                $this->settings["amazon_secret_key"], //secret key
				$generalConfig,
                $this->settings["amazon_application_name"], //application name
                $this->settings["amazon_application_version"] //application version
               );	 
		
    }

    function log($order, $message, $type = "INFO") {
        $message = mysql_real_escape_string($message);
        mysql_query("INSERT INTO `amazon_order_logs`(`order`,`type`,`message`) VALUES('$order','$type','$message')");
        return mysql_insert_id();
    }

    function updateStatus($order, $status) {
        $order = mysql_real_escape_string($order);
        $status = mysql_real_escape_string($status);
        $sql = mysql_query("UPDATE `amazon_orders` SET `status`='$status' WHERE `id`='$order'");
    }

    function getLogs($id, $order = "DESC") {
        $sql = mysql_query("SELECT * FROM `amazon_order_logs` WHERE `order`='$id' ORDER BY `id` $order");
        $out = array();
        while ($row = mysql_fetch_assoc($sql)) {
            $out[] = $row;
        }
        return $out;
    }
	
	function markCanceled(){
		sql("UPDATE `amazon_orders` SET order_status='Canceled', status='Mark as Canceled' WHERE id='$_REQUEST[id]'");
	}

    function setupRequest() {
        $this->request = new MarketplaceWebServiceOrders_Model_ListOrdersRequest();
        $this->request->setSellerId($this->settings["amazon_seller_id"]);
        $this->request->setMarketplaceId($this->settings["amazon_marketplace_id"]);
        $this->request->setMWSAuthToken($this->settings["amazon_mws_auth_token"]);
    }
        
    function getResyncQueue(){	
        $sql = mysql_query("SELECT * FROM `amazon_orders` WHERE user_id='".$this->settings['ownerId']."' AND `status`='Unprocessed' AND `order_status`='Pending'");
        $out = array();
        while($row = mysql_fetch_assoc($sql)){
            $out[] = $row;
        }
        return $out;
    }
    
    function amazon_resync($ids){
        if (!empty($ids)) {
            $request = new MarketplaceWebServiceOrders_Model_GetOrderRequest();
            $request->setSellerId($this->settings["amazon_seller_id"]);
            $request->setMWSAuthToken($this->settings["amazon_mws_auth_token"]);
            $request->setAmazonOrderId($ids);
            $response = $this->service->GetOrder($request);
            $xml = $response->toXML();
            $response = simplexml_load_string($xml);
            foreach ($response->GetOrderResult->Orders->Order as $order) {
                $order_id = $order->AmazonOrderId;
                $this->getOrderItems($order_id);
                $update_arr = array(
                    "purchase_date" => date("Y-m-d H:i:s", strtotime($order->PurchaseDate)),
                    "last_update_date" => date("Y-m-d H:i:s", strtotime($order->LastUpdateDate)),
                    "order_status" => $order->OrderStatus,
                    "fulfillment_channel" => $order->FulfillmentChannel,
                    "sales_channel" => $order->SalesChannel,
                    "ship_service_level" => $order->ShipServiceLevel,
                    "name" => $order->ShippingAddress->Name,
                    "address_1" => $order->ShippingAddress->AddressLine1,
                    "address_2" => $order->ShippingAddress->AddressLine2,
                    "city" => $order->ShippingAddress->City,
                    "state" => $order->ShippingAddress->StateOrRegion,
                    "zip" => $order->ShippingAddress->PostalCode,
                    "country_code" => $order->ShippingAddress->CountryCode,
                    "phone" => $order->ShippingAddress->Phone,
                    "currency_code" => $order->OrderTotal->CurrencyCode,
                    "amount" => $order->OrderTotal->Amount,
                    "number_of_items_shipped" => $order->NumberOfItemsShipped,
                    "number_of_items_unshipped" => $order->NumberOfItemsUnshipped,
                    "payment_execution_detail" => $order->PaymentExecutionDetail,
                    "payment_method" => $order->PaymentMethod,
                    "marketplace_id" => $order->MarketplaceId,
                    "buyer_email" => $order->BuyerEmail,
                    "buyer_name" => $order->BuyerName,
                    "shipment_service_level_category" => $order->ShipmentServiceLevelCategory,
                    "shipped_by_amazon" => ($order->ShippedByAmazonTFM == "true") ? 1 : 0,
                    "order_type" => $order->OrderType,
                    "earliest_ship_date" => date("Y-m-d H:i:s", strtotime($order->EarliestShipDate)),
                    "latest_ship_date" => date("Y-m-d H:i:s", strtotime($order->LatestShipDate)),
                    "ealiest_delivery_date" => date("Y-m-d H:i:s", strtotime($order->EarliestDeliveryDate)),
                    "latest_delivery_date" => date("Y-m-d H:i:s", strtotime($order->LatestDeliveryDate)),
                    "is_business_order" => ($order->IsBusinessOrder == "true") ? 1 : 0,
                    "is_prime" => ($order->IsPrime == "true") ? 1 : 0,
                    "is_premium_order" => ($order->IsPremiumOrder == "true") ? 1 : 0,
					"fees"	=> -1*(float)$order->OrderTotal->Amount*($this->settings["amazon_default_fee_percentage"]/100)
                );
                $qString = "";
                foreach($update_arr as $k => $v){
                    $qString .= ($qString == "") ? "" : ",";
                    $v = mysql_real_escape_string($v);
                    $qString .= "`$k`='$v'";
                }
                mysql_query("UPDATE `amazon_orders` SET $qString WHERE `order_id`='$order_id'");
            }
        }
    }

    function updateUnshipped() {
        $sql = mysql_query("SELECT * FROM `amazon_order_checking` WHERE `checked`='0'");
        $ids = array();
        while ($row = mysql_fetch_assoc($sql)) {
            $ids[] = $row["amazon_order_id"];
        }
        if (!empty($ids)) {
            $request = new MarketplaceWebServiceOrders_Model_GetOrderRequest();
            $request->setSellerId($this->settings["amazon_seller_id"]);
            $request->setMWSAuthToken($this->settings["amazon_mws_auth_token"]);
            $request->setAmazonOrderId($ids);
            $response = $this->service->GetOrder($request);
            $xml = $response->toXML();
            $response = simplexml_load_string($xml);
            foreach ($response->GetOrderResult->Orders->Order as $order) {
                $amazon_id = $order->AmazonOrderId;
                $status = $order->OrderStatus;
                mysql_query("UPDATE `amazon_order_checking` SET `checked`='1' WHERE `amazon_order_id`='$amazon_id'");
                mysql_query("UPDATE `amazon_orders` SET `order_status`='$status' WHERE `order_id`='$amazon_id'");
                $sql = mysql_query("SELECT * FROM `amazon_order_checking` WHERE `amazon_order_id`='$amazon_id' LIMIT 1");
                $row = mysql_fetch_assoc($sql);
                $this->log($row["order_id"], "Order update fetched from Amazon.");
            }
        }
    }

    function setCreatedAfter($date) {
        $this->request->setCreatedAfter($date);
    }
	 function setCreatedBefore($date) {
        $this->request->setCreatedBefore($date);
    }

    function getOrders() {
        return $this->service->ListOrders($this->request)->toXML();
    }

    function getOrdersByNextToken($token) {
        $request = new MarketplaceWebServiceOrders_Model_ListOrdersByNextTokenRequest(array(
            "SellerId" => $this->settings["amazon_seller_id"],
            "MWSAuthToken" => $this->settings["amazon_mws_auth_token"],
            "NextToken" => $token
        ));
        return $this->service->listOrdersByNextToken($request)->toXML();
    }

    function processOrders() {
        $response = $this->getOrders();
				
        $page = 1;
        $orders = 0;
        $response = simplexml_load_string($response);
        $rootElement = "ListOrdersResult";
        $run = true;
        //die(var_dump($response->$rootElement->Orders));
        while ($run) {
            foreach ($response->$rootElement->Orders->Order as $order) {
                $orders++;
                $fulfillmentChannel = $order->FulfillmentChannel;
                $insertArr = array(
                    "order_id" => $order->AmazonOrderId,
                    "purchase_date" => date("Y-m-d H:i:s", strtotime($order->PurchaseDate)),
                    "last_update_date" => date("Y-m-d H:i:s", strtotime($order->LastUpdateDate)),
                    "order_status" => $order->OrderStatus,
                    "fulfillment_channel" => $order->FulfillmentChannel,
                    "sales_channel" => $order->SalesChannel,
                    "ship_service_level" => $order->ShipServiceLevel,
                    "name" => $order->ShippingAddress->Name,
                    "address_1" => $order->ShippingAddress->AddressLine1,
                    "address_2" => $order->ShippingAddress->AddressLine2,
                    "city" => $order->ShippingAddress->City,
                    "state" => $order->ShippingAddress->StateOrRegion,
                    "zip" => $order->ShippingAddress->PostalCode,
                    "country_code" => $order->ShippingAddress->CountryCode,
                    "phone" => $order->ShippingAddress->Phone,
                    "currency_code" => $order->OrderTotal->CurrencyCode,
                    "amount" => $order->OrderTotal->Amount,
                    "number_of_items_shipped" => $order->NumberOfItemsShipped,
                    "number_of_items_unshipped" => $order->NumberOfItemsUnshipped,
                    "payment_execution_detail" => $order->PaymentExecutionDetail,
                    "payment_method" => $order->PaymentMethod,
                    "marketplace_id" => $order->MarketplaceId,
                    "buyer_email" => $order->BuyerEmail,
                    "buyer_name" => $order->BuyerName,
                    "shipment_service_level_category" => $order->ShipmentServiceLevelCategory,
                    "shipped_by_amazon" => ($order->ShippedByAmazonTFM == "true") ? 1 : 0,
                    "order_type" => $order->OrderType,
                    "earliest_ship_date" => (strtotime($order->EarliestShipDate) == 0) ?  date("Y-m-d H:i:s", strtotime("+1 day")) : date("Y-m-d H:i:s", strtotime($order->EarliestShipDate)), 
                    "latest_ship_date" => (strtotime($order->LatestShipDate) == 0) ?  date("Y-m-d H:i:s", strtotime("+1 day")) : date("Y-m-d H:i:s", strtotime($order->LatestShipDate)),
                    "ealiest_delivery_date" => (strtotime($order->EarliestDeliveryDate) == 0) ?  date("Y-m-d H:i:s", strtotime("+1 day")) : date("Y-m-d H:i:s", strtotime($order->EarliestDeliveryDate)),
                    "latest_delivery_date" => (strtotime($order->LatestDeliveryDate) == 0) ?  date("Y-m-d H:i:s", strtotime("+1 day")) : date("Y-m-d H:i:s", strtotime($order->LatestDeliveryDate)),
                    "is_business_order" => ($order->IsBusinessOrder == "true") ? 1 : 0,
                    "is_prime" => ($order->IsPrime == "true") ? 1 : 0,
                    "is_premium_order" => ($order->IsPremiumOrder == "true") ? 1 : 0,
					"fees"	=> -1*(float)$order->OrderTotal->Amount*($this->settings["amazon_default_fee_percentage"]/100)
                );				
				$insertArr['user_id'] = $this->userId;
				
                $keys = array();
                foreach ($insertArr as $key => $value) {
                    $keys[] = $key;
                    $insertArr[$key] = mysql_real_escape_string($value);
                }
                
                $q = mysql_query("SELECT * FROM `amazon_orders` WHERE `order_id`='$insertArr[order_id]'");
				$ord = mysql_fetch_assoc($q);
                if ($ord) {
                	if($ord['order_status'] == 'Pending'){
                		$updatesql = array();
                		foreach($insertArr as $k=>$v) $updatesql[] = "`$k`='$v'";
                		//t("UPDATE `amazon_orders` SET ".implode(",",$updatesql)." WHERE id='$ord[id]'");
                		mysql_query("UPDATE `amazon_orders` SET ".implode(",",$updatesql)." WHERE id='$ord[id]'")or die(mysql_error());
                	}
                }
                else{
                	mysql_query("INSERT INTO `amazon_orders`(`" . implode("`,`", $keys) . "`)"
                			. " VALUES('" . implode("','", $insertArr) . "')")or die(mysql_error());
                	$id = mysql_insert_id();
                	$this->log($id, "Order placed on Amazon.");
                	$this->log($id, "Order imported from Amazon.");
                	if(strtolower($fulfillmentChannel) == "afn"){
                		$this->updateStatus($id, "Processed");
                	}else{
                		$this->updateStatus($id, "Unprocessed");
                	}
                }
            }
            if ($response->$rootElement->NextToken) {
                $response = $this->getOrdersByNextToken($response->$rootElement->NextToken);
                $response = simplexml_load_string($response);
            } else {
                $run = false;
            }
            $rootElement = "ListOrdersByNextTokenResult";
            $page++;
        }
        //if($page != 1){
        //    $rootElement = "ListOrdersResult";
        //}
    }
			
	public function requestReport($type,$range=array()){
		$request = new MarketplaceWebService_Model_RequestReportRequest(array(
			"Merchant" => $this->settings["amazon_seller_id"],
            "MWSAuthToken" => $this->settings["amazon_mws_auth_token"],
			'MarketplaceIdList' => array("Id" => $this->settings["amazon_marketplace_id"]),								
			'ReportType' => $type,
			'ReportOptions' => 'ShowSalesChannel=true',			
		));
		if($range[0]){
			$request->setStartDate(date("Y-m-d H:i:s", strtotime($range[0])));
		}
		if($range[1]){
			$request->setEndDate(date("Y-m-d H:i:s", strtotime($range[1])));
		}

        $response = $this->generalService->requestReport($request);		
		
		if ($response->isSetRequestReportResult()) {                     
			$requestReportResult = $response->getRequestReportResult();                   
			if ($requestReportResult->isSetReportRequestInfo()) {                        
				$reportRequestInfo = $requestReportResult->getReportRequestInfo();                          
					if ($reportRequestInfo->isSetReportRequestId()) 
                          return $reportRequestInfo->getReportRequestId();
			}
		}
						  
		return false;
	}

	public function getReportRequests($id='',$type='') {
		$request = new MarketplaceWebService_Model_GetReportRequestListRequest(array(
			"Merchant" => $this->settings["amazon_seller_id"],
            "MWSAuthToken" => $this->settings["amazon_mws_auth_token"]            
		));						
			
		if($type){
			$typeList = new MarketplaceWebService_Model_TypeList();
			$typeList->setType($type);
			$request->setReportTypeList($typeList);			
		}
		if($id){
			$idList = new MarketplaceWebService_Model_IdList();
			$idList->setId($id);
			$request->setReportRequestIdList($idList);			
		}		
        return $this->generalService->getReportRequestList($request);
    }	
	
	function getReports($id='',$type='',$new=false) {
		$request = new MarketplaceWebService_Model_GetReportListRequest(array(
			"Merchant" => $this->settings["amazon_seller_id"],
            "MWSAuthToken" => $this->settings["amazon_mws_auth_token"]            
		));						
			
		if($type){
			$typeList = new MarketplaceWebService_Model_TypeList();
			$typeList->setType($type);
			$request->setReportTypeList($typeList);			
		}
		if($new)$request->setAcknowledged(false);
		if($id){
			$idList = new MarketplaceWebService_Model_IdList();
			$idList->setId($id);
			$request->setReportRequestIdList($idList);			
		}	

        return $this->generalService->getReportList($request);
    }

    function getReportsByNextToken($token) {
		if(!$token)return false;
        $request = new MarketplaceWebService_Model_GetReportRequestListByNextTokenRequest(array(
            "Merchant" => $this->settings["amazon_seller_id"],
            "MWSAuthToken" => $this->settings["amazon_mws_auth_token"],
            "NextToken" => $token,
        ));
        return $this->generalService->getReportListByNextToken($request);
    }
	function getReport($id) {
		if(!$id)return false;
		
		$request = new MarketplaceWebService_Model_GetReportRequest(array(
			'Merchant' => $this->settings["amazon_seller_id"],
			'Report' => @fopen('php://memory', 'rw+'),
			'ReportId' => $id,
			'MWSAuthToken' => $this->settings["amazon_mws_auth_token"]
		));

        $report = $this->generalService->getReport($request);
		return stream_get_contents($request->getReport());
    }
	function acknowledgedReport($id){
		if(!$id)return false;
		
		$request = new MarketplaceWebService_Model_UpdateReportAcknowledgementsRequest(array(
			'Merchant' => $this->settings["amazon_seller_id"],
			'ReportIdList' => array ('Id' => array ($id)),
			'Acknowledged' => true,
			'MWSAuthToken' => $this->settings["amazon_mws_auth_token"]
		));

        $this->generalService->updateReportAcknowledgements($request);		
	}
	function processSettlementReports($type='',$new=false) {
        $response = $this->getReports('',$type,$new);			
		do{
			if(!$response || !$response->isSetGetReportListResult())return;
			
			$reports = $response->getGetReportListResult();			
			foreach ($reports->getReportInfoList() as $report) {
				$reportId = $report->getReportId();		
				$report = $this->getReport($reportId);				
				if(!$report) continue;				
				$data = simplexml_load_string($report);	
				if(!$data->Message->SettlementReport->Order)continue;
				
				//Fees Update
				foreach ($data->Message->SettlementReport->Order as $order) {
					$id = $order->AmazonOrderID;
					$fees = 0;
					
					t('Fee Update: '.$id,1);

					if(!$order->Fulfillment->Item)continue;
					foreach($order->Fulfillment->Item as $item){
						if($item->ItemFees){
							foreach($item->ItemFees as $feeItem){
								foreach($feeItem->Fee as $fee){
									$fees += (float)$fee->Amount;
								}
							}
						}
					}					
					mysql_query("UPDATE amazon_orders SET fees='$fees', `fees_estimated` = 0 WHERE order_id='$id'");
					$this->log($id, "Order settlement data fetched from Amazon.");					
				}
				
				//Refund Update
				foreach ($data->Message->SettlementReport->Refund as $refund) {
					$id = $refund->AmazonOrderID;
					$amount = 0;
					
					t('Fee Update: '.$id,1);
							
					if(!$refund->Fulfillment->AdjustedItem)continue;
					foreach($refund->Fulfillment->AdjustedItem as $item){
						if($item->ItemPriceAdjustments){
							foreach($item->ItemPriceAdjustments as $priceItem){
								foreach($priceItem->Component as $price){
									$amount += (float)$price->Amount;
								}
							}
							foreach($item->ItemFeeAdjustments as $feeItem){
								foreach($feeItem->Fee as $fee){
									$amount += (float)$fee->Amount;
								}
							}
						}
					}
					mysql_query("UPDATE amazon_orders SET refund_amount='$amount' WHERE order_id='$id'");
					$this->log($id, "Order refund data fetched from Amazon.");										
				}
				
				$this->acknowledgedReport($reportId);
			}
			$token = false;
			if($reports->isSetNextToken())$token = $reports->getNextToken();
			$response = $this->getReportsByNextToken($token);
		}while($response && $token);	       
    }
	
    function listOrdersFromDB($id=0,$owner=false,$filter=array()) {
		if(!$filter)$filter=$_REQUEST;
        $sqlOpt = "1=1";
		
		if($id){
			$sqlOpt .= " AND o.`id`=$id";		
		}
		else{		
			if($owner) $sqlOpt .= " AND o.`sales_channel` in ('".implode("','",$GLOBALS['orders']['ownerChannel'])."')";
			else $sqlOpt .= " AND o.`sales_channel` not in ('".implode("','",$GLOBALS['orders']['ownerChannel'])."')";
			
			if ((int) $filter["order"] == 1) {
				$sqlOpt .= " AND o.`remote_order_id`!=''";
			} else if ((int) $filter["order"] == 2) {
				$sqlOpt .= " AND (o.`remote_order_id`='' OR o.`remote_order_id` IS NULL)";
			}
			if ((int) $filter["tracking"] == 1) {
				$sqlOpt .= " AND o.`tracking_number`!=''";
			} else if ((int) $filter["tracking"] == 2) {
				$sqlOpt .= " AND (o.`tracking_number`='' OR o.`tracking_number` IS NULL)";
			}
			if($filter["status"]){
				$sqlOpt .= " AND o.`order_status` IN('".implode("','", $filter["status"])."')";				
			}
			if($filter["refund"]){
				$sqlOpt .= " AND o.`refund_status` IN('".implode("','", $filter["refund"])."')";
			}
			if($filter["timeback"])
				$sqlOpt .= " AND o.`purchase_date` BETWEEN NOW() - INTERVAL {$filter["timeback"]} DAY AND NOW()";
		}	
		
		$sqlOpt .= " AND o.`user_id`='".Users::getOwnerId()."'";
			
        //t("SELECT o.* FROM `amazon_orders` AS o WHERE $sqlOpt GROUP BY o.id ORDER BY o.`id` DESC");
        $sql = mysql_query("SELECT o.* FROM `amazon_orders` AS o WHERE $sqlOpt GROUP BY o.id ORDER BY o.`id` DESC");
							
        $out = array();
        while ($row = mysql_fetch_assoc($sql)) {
			//if($row['order_status'] == 'Unshipped' && $row['tracking_number'] && in_array('Unshipped',$filter['status']))continue;
			list($row['resolved'],$row['unresolved']) = $this->getOrderIssueCount($row['id']);						
			
			$match = false;
			if(!$filter['issues'])$match=true;
			if(in_array('noissues',$filter['issues']) && !$row['resolved'] && !$row['unresolved'])$match=true;
			if(in_array('resolved',$filter['issues']) && $row['resolved'])$match=true;
			if(in_array('unresolved',$filter['issues']) && $row['unresolved'])$match=true;
			
			if($match)
				$out[] = $row;
        }
        return $out;
    }
	function isOwnerOrder($id){
		if(!$id)return true;
		list($channel) = mysql_fetch_array(mysql_query("SELECT sales_channel FROM amazon_orders WHERE id='$id' OR order_id='$id'"));
		return in_array($channel,$GLOBALS['orders']['ownerChannel']);
	}
	function createOwnerOrder(){
		$channel = reset($GLOBALS['orders']['ownerChannel']);
		mysql_query("INSERT INTO amazon_orders (`status`,`remote_order_id`,`sales_channel`,`purchase_date`,`user_id`) VALUES ('Pending','New Order','$channel',NOW(),'".$this->settings['ownerId']."')");
		$error = mysql_error();		
		if(!$error){			
			$oId = mysql_insert_id();
			$iId = $this->createOwnerItem($oId,'No Title',1);
			$order = mysql_fetch_assoc(mysql_query("SELECT * FROM amazon_orders WHERE id='$oId'"));			
		}
		else{
			$order = false;
		}
		return $order;
	}
	function createOwnerItem($oId,$title,$qty){			
		if(!$oId || !$title || !$qty) err("Unable to add order item!");
				
		mysql_query("INSERT INTO amazon_order_items (order_id,Title,QuantityOrdered) VALUES ('$oId','$title','$qty')");
		$error = mysql_error();
		$id = mysql_insert_id();
		if($error){ return false; }
		else{ return $id;	}
	}
	function updateOrderRefundStatus($id,$status){
		sql("UPDATE amazon_orders SET refund_status='$status' WHERE id='$id'");
	}
	function getOrderIssueCount($oId){
		$resolved = $unresolved = 0;
		$q = mysql_query("SELECT resolved, count(id) FROM `amazon_orders_issues` WHERE order_id=$oId GROUP BY resolved");
		while(list($res,$num) = mysql_fetch_array($q)){
			if($res)$resolved=$num;
			else $unresolved=$num;
		}	
		return array($resolved,$unresolved);
	}
	function getOrderIssues($oId){
		$issues = array();
		$q = mysql_query("SELECT * FROM amazon_orders_issues WHERE order_id='$oId' ORDER BY id DESC");		
		while($r = mysql_fetch_assoc($q)){			
			$r['timestamp'] = tzdate('m/d/y h:i:s A',$r['timestamp']);			
			$issues[] = $r;
		}
		return $issues;
	}
	function getOrderLatestIssue($oId){
		return reset($this->getOrderIssues($oId));
	}
	function updateOrderIssue($id,$orderId,$issue=false,$resolved=false,$resolution=false){
		$updatesql = array();
		if($resolution!==false)$updatesql[] = "resolution='$resolution'";
		if($resolved!==false)$updatesql[] = "resolved='$resolved'";
		if($issue)$updatesql[] = "issue='$issue'";
		
		//t("UPDATE amazon_orders_issues SET ".implode(",",$updatesql)." WHERE id='$id'");
		
		sql("UPDATE amazon_orders_issues SET ".implode(",",$updatesql)." WHERE id='$id'",array('orderId'=>$orderId));
	}
	function createOrderIssue($oId,$issue){
		sql("INSERT INTO amazon_orders_issues (order_id,issue) VALUES ('$oId','$issue')");
	}
	function deleteOrderIssue($id){
		sql("DELETE FROM amazon_orders_issues WHERE id='$id'");
	}
    function getLastOrder() {		
        $sql = mysql_query("SELECT * FROM `amazon_orders` WHERE user_id='".$this->settings['ownerId']."' AND `sales_channel` not in ('".implode("','",$GLOBALS['orders']['ownerChannel'])."') ORDER BY `id` DESC LIMIT 1")or die(mysql_error());
        return mysql_fetch_assoc($sql);
    }	
    function getOrder($id) {
        $sql = mysql_query("SELECT * FROM `amazon_orders` WHERE `id`='$id'");
		$order = mysql_fetch_assoc($sql);
		$order['portal'] = $this->getPortalInfo($order['id']);
		$order['credits'] = $this->getOrderCredits($order['id']);
		$order['costs'] = $this->getOrderCosts($order['id']);
		$order = $this->fixFees($order);
				
		$order['inbound_summary'] = $order['outbound_summary'] = array();
		$order['inbound'] = $order['outbound'] = 0;
						
		if($order){
			$order['inbound'] += $order['amount'];
			$order['inbound_summary']['charge'] = $order['amount']; 
			
			$order['outbound'] += $order['fees'];
			$order['outbound'] += -1*$order['portal']['portal_price'];
			$order['outbound'] += -1*$order['portal']['portal_tax'];
			$order['outbound'] += -1*$order['portal']['portal_shipping'];
			
			$order['outbound_summary']['fees'] = $order['fees']; 
			$order['outbound_summary']['purchase'] = -1 * ($order['portal']['portal_price']+$order['portal']['portal_tax']+$order['portal']['portal_shipping']);
			$order['outbound_summary']['other'] = 0;
			
			if($order['costs']){
				foreach($order['costs'] as $c){
					$order['outbound'] +=-1* (((float)$c->amount+(float)$c->tax+(float)$c->shipping));
					$order['outbound_summary']['other'] += (((float)$c->amount+(float)$c->tax+(float)$c->shipping));
				}
			}			
			if($order['credits']){
				foreach($order['credits'] as $credit){
					$order['inbound'] += $credit->amount;
					$order['inbound_summary']['other'] += $credit->amount;
				}
			}											
			if($order['portal'] && $order['portal']['portal_price'] && $order['portal']['portal_cashback']){ 
				$order['inbound'] += (float)$order['portal']['portal_cashback'];
				$order['inbound_summary']['cashback'] = (float)$order['portal']['portal_cashback'];
			}
			if($order['portal'] && $order['portal']['portal_price'] && $order['portal']['portal_points']){
				$order['inbound'] += (float)$order['portal']['portal_points']/100;
				$order['inbound_summary']['cashback'] = (float)$order['portal']['portal_points']/100;
			}
			
			$order['gains'] = $order['inbound'] + $order['outbound'];
			
			//if($order['refund_amount'])$order['amount_net'] += (float)$order['refund_amount'];
		}
		
		/*
		if($order){		
			if($order['amount'] && $order['fees']){ 
				$order['amount_gross'] = (float)$order['amount']+(float)$order['fees'];				
				$order['amount_gross_estimated'] = (float)$order['amount']+( -1*(float)$order['amount']*($this->settings["amazon_default_fee_percentage"]/100) );
			}	
			
			if($order['portal']){										
				$order['total_cost']+=(float)$order['portal']['portal_price']+(float)$order['portal']['portal_tax']+(float)$order['portal']['portal_shipping'];
			}
			if($order['costs']){
				foreach($order['costs'] as $c){ 					
					$order['total_cost'] +=((float)$c->amount+(float)$c->tax+(float)$c->shipping);
				}
			}
				
			
			if($order['portal'] && $order['portal']['portal_price'] && $order['amount_gross'] && !$order['fees_estimated']){										
				$order['amount_net'] = (float)$order['amount_gross']-((float)$order['portal']['portal_price']+(float)$order['portal']['portal_tax']+(float)$order['portal']['portal_shipping']);															
				if($order['costs'])foreach($order['costs'] as $c)$order['amount_net'] -= ((float)$c->amount+(float)$c->tax+(float)$c->shipping);				
				
				if($order['refund_amount'])
					$order['amount_net'] += (float)$order['refund_amount'];					
			}
			if($order['portal'] && $order['portal']['portal_price'] && $order['amount_gross_estimated']){
				$order['amount_net_estimated'] = (float)$order['amount_gross_estimated']-((float)$order['portal']['portal_price']+(float)$order['portal']['portal_tax']+(float)$order['portal']['portal_shipping']);
				if($order['costs'])foreach($order['costs'] as $c)$order['amount_net_estimated'] -= ((float)$c->amount+(float)$c->tax+(float)$c->shipping);				
				
				if($order['refund_amount'])
					$order['amount_net_estimated'] = (float)$order['amount_gross_estimated']-((float)$order['portal']['portal_price']+(float)$order['portal']['portal_tax']+(float)$order['portal']['portal_shipping']);
			}
			
			if($order['portal'] && $order['portal']['portal_price'] && $order['portal']['portal_cashback'])	$order['cashback_earned'] = (float)$order['portal']['portal_price']*((float)$order['portal']['portal_cashback']/100);
			if($order['portal'] && $order['portal']['portal_price'] && $order['portal']['portal_points'])	$order['points_earned'] = (float)$order['portal']['portal_price']*(float)$order['portal']['portal_points'];
			
			
			if($order['portal']['type'] == 'cashback' && $order["rewards_amount"]){				
				if($order['refund_amount']){
					//$order['amount_net'] += (float)$order['refund_amount'];			
				}
				else{
					$order['amount_net'] += (float)$order['refund_amount'];	
					$order['amount_net_estimated'] += (float)$order['cashback_earned'];	
				}
			}					
											
			if($order['credits']){
				foreach($order['credits'] as $credit){
					if(isset($order['amount_net']))$order['amount_net'] += $credit->amount;
					if(isset($order['amount_net_estimated']))$order['amount_net_estimated'] += $credit->amount;
				}
			}
		}	
		*/		
		
		$order['amount_gross_rounded'] = ($order['amount_gross'])?number_format(round($order['amount_gross']),0):'N/A';
		$order['amount_net_rounded'] = ($order['amount_net'])?number_format(round($order['amount_net']),0):'N/A';
		$order['amount_net_estimated_rounded'] = ($order['amount_net_estimated'])?number_format(round($order['amount_net_estimated']),0):'N/A';
		$order['cashback_earned_rounded'] = ($order['cashback_earned'])?number_format(round($order['cashback_earned']),0):'N/A';
		$order['points_earned_rounded'] = ($order['points_earned'])?number_format(round($order['points_earned']),0):'N/A';		
		
		$order['amount_gross'] = ($order['amount_gross'])?number_format($order['amount_gross'],2):'N/A';
		$order['amount_net'] = ($order['amount_net'])?number_format($order['amount_net'],2):'N/A';
		$order['amount_net_estimated'] = ($order['amount_net_estimated'])?number_format($order['amount_net_estimated'],2):'N/A';
		$order['cashback_earned'] = ($order['cashback_earned'])?number_format($order['cashback_earned'],2):'N/A';
		$order['points_earned'] = ($order['points_earned'])?number_format($order['points_earned'],2):'N/A';
		
		$gc = new Giftcards();
		$order['giftcards'] = $gc->getOrderCards($order['id']);						
		
		$issues = array();
		list($issues['resolved'],$issues['unresolved']) = $this->getOrderIssueCount($order['id']);	
		$order['issuesCount'] = $issues['resolved']+$issues['unresolved'];
		

        return $order;
    }
	static public function getCreditStores(){
		$stores = array();
		
		$q = mysql_query("SELECT DISTINCT store FROM amazon_orders_credits ORDER BY store ASC");
		while(list($s) = mysql_fetch_array($q))$stores[] = $s;
		
		return $stores;
	}
	public function getOrderCredits($id){
		$credits = array();
		
		$q = mysql_query("SELECT * FROM amazon_orders_credits WHERE order_id = '$id' ORDER BY timestamp DESC");
		while($r = mysql_fetch_assoc($q)){		
			$r['timestamp'] = tzdate('m/d/y h:i:s A', $r['timestamp']);
			$r['amount_formatted'] = number_format($r['amount'],2);
			$credits[] = (object)$r;
		}
		return $credits;
	}
	function removeOrderCredit($id){
		sql("DELETE FROM amazon_orders_credits WHERE id='$id'");
	}
	function addOrderCredit($oId,$amount,$store,$notes){
		sql("INSERT INTO amazon_orders_credits (`order_id`,`amount`,`notes`,`store`) VALUES ('$oId','$amount','$notes','$store')");		
	}
	function fixFees($order){
		if($order && $order['fees_estimated'] && $order['amount'] && (!$order['fees'] || !$order['fees']==-0)){
			$order['fees'] = -1*(float)$order['amount']*($this->settings["amazon_default_fee_percentage"]/100);
			mysql_query("UPDATE amazon_orders SET fees = '$order[fees]' WHERE id='$order[id]'");
		}
		return $order;
	}
	public function updateOrderReward($oId,$amount){		
		list($old_amount) = mysql_fetch_array(mysql_query("SELECT rewards_amount FROM amazon_orders WHERE remote_order_id='$oId'"));		
		if($amount != $old_amount){			
			mysql_query("UPDATE amazon_orders SET rewards_amount='$amount' WHERE remote_order_id='$oId'");
			mysql_query("UPDATE amazon_orders_portals SET status='".self::getPortalConfirmedStatus()."' WHERE order_id='$oId'");
		}
	}
	
    function updateRemoteOrder($id, $order_id) {
        $order_id = str_replace(" ", "", $order_id);
        $order_id = mysql_real_escape_string($order_id);
        mysql_query("UPDATE `amazon_orders` SET `remote_order_id`='$order_id' WHERE `id`='$id'");
    }
	function updateLocalOrder($orderId,$data){				
		$portalId = (int)$data['portal_id'];
		$storeId = (int) $data['store_id'];
		$portalPrice= (float)$data['portal_price'];
		$portalTax= (float)$data['portal_tax'];
		$portalShipping= (float)$data['portal_shipping'];
		$portalPoints= (float)$data['portal_points'];
		$portalCashback= (float)$data['portal_cashback'];
		$portalStatus= $data['portal_status'];
		$portalTimestamp= $data['portal_timestamp'];
		$confirmationTimestamp= $data['portal_confirmation_timestamp'];
											
		//$amountGross = (float)$data['amount_gross'];
		//$amountNet = (float)$data['amount_net'];
		//mysql_query("UPDATE amazon_orders SET amount_gross='$amountGross' WHERE id='$orderId'");
		mysql_query("UPDATE amazon_orders SET last_update_date=NOW() WHERE id='$orderId'");
		
		$confirmed = self::isPortalConfirmed($portalStatus);
		list($id) = mysql_fetch_array(mysql_query("SELECT id FROM amazon_orders_portals WHERE order_id='$orderId'"));
		if($id){
			$updateSql = array();
			$updateSql[] = "portal_id='$portalId'";
			$updateSql[] = "store_id='$storeId'";
			$updateSql[] = "status='$portalStatus'";
			$updateSql[] = "portal_price='$portalPrice'";			
			$updateSql[] = "portal_tax='$portalTax'";			
			$updateSql[] = "portal_shipping='$portalShipping'";			
			$updateSql[] = "portal_points='$portalPoints'";			
			$updateSql[] = "portal_cashback='$portalCashback'";			
			if($portalTimestamp)$updateSql[] = "timestamp='".dbtimestamp($portalTimestamp)."'";
			if($confirmationTimestamp)$updateSql[] = "confirmation_timestamp='".dbtimestamp($confirmationTimestamp)."'";
			else if($confirmed)$updateSql[] = "confirmation_timestamp='".dbtimestamp('+1 second')."'";
									
			mysql_query("UPDATE amazon_orders_portals SET ".implode(", ",$updateSql)." WHERE id='$id'");
		}
		else{
			$portalCashback = $portalPoints;
			mysql_query("INSERT INTO amazon_orders_portals (portal_id,store_id,order_id,status,confirmation_timestamp,portal_price,portal_tax,portal_shipping,portal_points,portal_cashback) VALUES ('$portalId','$storeId','$orderId','$portalStatus','".dbtimestamp('+1 second')."','$portalPrice','$portalTax','$portalShipping','$portalPoints','$portalCashback')");
		}
	}	
	function getPortalInfo($id){	
		$q = mysql_query("SELECT op.*, p.title AS portal_title, p.url AS portal_url, s.title AS store_title, s.url AS site_url, p.type, p.email FROM amazon_orders_portals AS op
							LEFT JOIN portals AS p ON p.id=op.portal_id
							LEFT JOIN stores AS s ON s.id=op.store_id
							WHERE op.order_id='$id'");
		$data = mysql_fetch_assoc($q);
		if($data){
			$data['confirmed'] = self::isPortalConfirmed($data['status']);
			if($data['confirmed'])$data['confirmation_period'] = xTimeAgo($data['timestamp'],$data['confirmation_timestamp'],'');
			$data['timestamp'] = tzdate('m/d/Y',$data['timestamp']);
			$data['confirmation_timestamp'] = tzdate('m/d/Y',$data['confirmation_timestamp']);						
		}
		return $data;
	}
	static function isPortalConfirmed($status){
		if(strpos($status,self::getPortalConfirmedStatus()) === false)return false;
		else return true;
	}
	static function getPortalConfirmedStatus(){
		return 'Confirmed';
	}
	public function getOrdersByPortalStatus($id,$status=''){
		$orders = array();
		if(!$status)return $orders;
		
		$wheresql = array();
		$wheresql[] = "p.status='$status'";
		if($status == self::getDefaultPortalStatus())$wheresql[0] = "(".$wheresql[0]." OR p.status='')";
		$wheresql[] = "p.portal_id='$id'";
		$q = mysql_query("SELECT o.* FROM amazon_orders_portals AS p
							JOIN amazon_orders AS o ON p.order_id=o.id							
							WHERE ".implode(" AND ",$wheresql)."
							ORDER BY p.timestamp DESC");

		while($r = mysql_fetch_assoc($q)){
			$r['portal'] = $this->getPortalInfo($r['id']);
			$orders[] = $r;
		}
		return $orders;
	}
	static function getDefaultPortalStatus(){
		return self::$defaultPortalStatus;
	}	

    function getUnsubmittedFulfilments($fulfilmentType) {	
        $sql = mysql_query("SELECT * FROM `amazon_submissions` WHERE (`amazon_id`='' OR amazon_id is NULL) AND `type`='$fulfilmentType'");
        $out = array();
        while ($row = mysql_fetch_assoc($sql)) {
            $out[] = $row;
        }
        return $out;
    }		    
            
    function updateQty($data) {
    	if(!$data)return false;
    		 
    	if(!is_array($data))$data = array($data);
    		 
    	$xml = array();
    	foreach($data as $i=>$obj){
    		$i++;
    		$xml[] = "<Message>
    					<MessageID>{$i}</MessageID>
    					<OperationType>Update</OperationType>
    					<Inventory>
							<SKU>{$obj['sku']}</SKU>
							<Quantity>{$obj['qty']}</Quantity>
    					</Inventory>
    				  </Message>";    	  
    	}    	
    	$xml = implode("\n",$xml);
					
    	$config = array(
    		'ServiceURL' => "https://mws.amazonservices.com",
    		'ProxyHost' => null,
    		'ProxyPort' => -1,
    		'MaxErrorRetry' => 3,
    	);
        $service = new MarketplaceWebService_Client($this->settings["amazon_access_key"], $this->settings["amazon_secret_key"], $config, $this->settings["amazon_application_name"], $this->settings["amazon_application_version"]);
            	
    	$feed = <<<EOD
<?xml version="1.0" encoding="utf-8" ?>
	<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amzn-envelope.xsd">
	<Header>
		<DocumentVersion>1.01</DocumentVersion>
		<MerchantIdentifier>{$this->settings["merchant_identifier"]}</MerchantIdentifier>
	</Header>
	<MessageType>Inventory</MessageType>	
	{$xml}			
</AmazonEnvelope>
EOD;
    	    
    	//t($feed);
   
    	$feedHandle = @fopen('php://temp', 'rw+');
    	fwrite($feedHandle, $feed);
    	rewind($feedHandle);
    	$parameters = array(
    			'Merchant' => $this->settings["amazon_seller_id"],
    			'MarketplaceIdList' => array("Id" => $this->settings["amazon_marketplace_id"]),
    			'FeedType' => '_POST_INVENTORY_AVAILABILITY_DATA_',
    			'FeedContent' => $feedHandle,
    			'PurgeAndReplace' => false,
    			'ContentMd5' => base64_encode(md5(stream_get_contents($feedHandle), true)),
    			'MWSAuthToken' => $this->settings["amazon_mws_auth_token"], // Optional
    	);
    	rewind($feedHandle);
    	$request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
    	
    	try {
    		$response = $service->submitFeed($request);    		    		
    	} catch (MarketplaceWebService_Exception $ex) {
    		echo("Caught Exception: " . $ex->getMessage() . "\n");
    		echo("Response Status Code: " . $ex->getStatusCode() . "\n");
    		echo("Error Code: " . $ex->getErrorCode() . "\n");
    		echo("Error Type: " . $ex->getErrorType() . "\n");
    		echo("Request ID: " . $ex->getRequestId() . "\n");
    		echo("XML: " . $ex->getXML() . "\n");
    		echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
    		
    		t("Updating Qty",1);
    		t($response);
    		 
    		$response = false;
    	}
    	 
    	if($response){
    		$submitFeedResult = $response->getSubmitFeedResult();
    		$feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
    		$submission_id = $feedSubmissionInfo->getFeedSubmissionId();
    		sleep(1);
    	}
    	else{    	
    		$submission_id = 0;
    	}
       	    	    
    	return $submission_id;
    }
    function updateMin($data) {
    	if(!$data)return false;
    	
    	if(!is_array($data))$data = array($data);
    	
    	$xml = array();
    	foreach($data as $i=>$obj){
    		$i++;    		
    		$xml[] = "<Message>
						<MessageID>{$i}</MessageID>
						<Price>
    						<SKU>{$obj['sku']}</SKU>";
    		
    		if(!$obj['keep_current_price'])$xml[] = "		<StandardPrice currency=\"USD\">{$obj['pricing']['max']}</StandardPrice>";
    		
			$xml[] = "		<MinimumSellerAllowedPrice currency=\"USD\">{$obj['pricing']['min_real']}</MinimumSellerAllowedPrice>
    						<MaximumSellerAllowedPrice currency=\"USD\">{$obj['pricing']['max']}</MaximumSellerAllowedPrice>
    					</Price>
    				</Message>";
    	}
    	$xml = implode("\n",$xml);
    	
    	
    	$config = array(
    		'ServiceURL' => "https://mws.amazonservices.com",
    		'ProxyHost' => null,
    		'ProxyPort' => -1,
    		'MaxErrorRetry' => 3,
    	);
    	$service = new MarketplaceWebService_Client($this->settings["amazon_access_key"], $this->settings["amazon_secret_key"], $config, $this->settings["amazon_application_name"], $this->settings["amazon_application_version"]);
         
    	$feed = <<<EOD
<?xml version="1.0" encoding="utf-8" ?>
<AmazonEnvelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="amznenvelope.xsd">
	<Header>
		<DocumentVersion>1.01</DocumentVersion>
		<MerchantIdentifier>{$this->settings["merchant_identifier"]}</MerchantIdentifier>
	</Header>
	<MessageType>Price</MessageType>
	{$xml}	
</AmazonEnvelope>
EOD;
    
    	//t($feed);
    	
    	$feedHandle = @fopen('php://temp', 'rw+');
    	fwrite($feedHandle, $feed);
    	rewind($feedHandle);
    	$parameters = array(
    			'Merchant' => $this->settings["amazon_seller_id"],
    			'MarketplaceIdList' => array("Id" => $this->settings["amazon_marketplace_id"]),
    			'FeedType' => '_POST_PRODUCT_PRICING_DATA_',
    			'FeedContent' => $feedHandle,
    			'PurgeAndReplace' => false,
    			'ContentMd5' => base64_encode(md5(stream_get_contents($feedHandle), true)),
    			'MWSAuthToken' => $this->settings["amazon_mws_auth_token"], // Optional
    	);
    	rewind($feedHandle);
    	$request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
    	
    	try {
    		$response = $service->submitFeed($request);    		
    	} catch (MarketplaceWebService_Exception $ex) {
    		echo("Caught Exception: " . $ex->getMessage() . "\n");
    		echo("Response Status Code: " . $ex->getStatusCode() . "\n");
    		echo("Error Code: " . $ex->getErrorCode() . "\n");
    		echo("Error Type: " . $ex->getErrorType() . "\n");
    		echo("Request ID: " . $ex->getRequestId() . "\n");
    		echo("XML: " . $ex->getXML() . "\n");
    		echo("ResponseHeaderMetadata: " . $ex->getResponseHeaderMetadata() . "\n");
    		
    		t("Updating Pricing",1);    		    		
			t($response);
    			
    		$response = false;    		
    	}
    	
    	if($response){
    		$submitFeedResult = $response->getSubmitFeedResult();
    		$feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
    		$submission_id = $feedSubmissionInfo->getFeedSubmissionId();
    		sleep(1);
    	}
    	else{    		
    		$submission_id = 0;    		
    	}    
    	
    	return $submission_id;
    }
    
    
    
    function submitOrderFulfilment($table_id, $fulfilmentType, $order) {
		$internal_id = $order["id"];
		$remote_id = $order["remote_order_id"];
		$amazon_order_id = $order["order_id"];
		$tracking_number = $order["tracking_number"];
		
        $config = array(
            'ServiceURL' => "https://mws.amazonservices.com",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'MaxErrorRetry' => 3,
        );
        $service = new MarketplaceWebService_Client(
                $this->settings["amazon_access_key"], $this->settings["amazon_secret_key"], $config, $this->settings["amazon_application_name"], $this->settings["amazon_application_version"]);

        //die(date("c"));
		
		$date =  date("c",strtotime($order['purchase_date']." +1 day"));
		if($date > date('c'))$date = date('c');
        
        $feed = <<<EOD
<?xml version="1.0" encoding="utf-8"?>
<AmazonEnvelope xsi:noNamespaceSchemaLocation="amzn-envelope.xsd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <Header>
        <DocumentVersion>1.01</DocumentVersion>
        <MerchantIdentifier>{$this->settings["merchant_identifier"]}</MerchantIdentifier>
    </Header>
    <MessageType>OrderFulfillment</MessageType>
    <Message>
        <MessageID>{$internal_id}</MessageID>
        <OperationType>Update</OperationType>
        <OrderFulfillment>
            <AmazonOrderID>{$amazon_order_id}</AmazonOrderID>
            <FulfillmentDate>{$date}</FulfillmentDate>
            <FulfillmentData>
                <CarrierCode>UPS</CarrierCode>
                <ShippingMethod>UPS Ground</ShippingMethod>
                <ShipperTrackingNumber>{$tracking_number}</ShipperTrackingNumber>
            </FulfillmentData>
        </OrderFulfillment>
    </Message>
</AmazonEnvelope>
EOD;
        $feedHandle = @fopen('php://temp', 'rw+');
        fwrite($feedHandle, $feed);
        rewind($feedHandle);
        $parameters = array(
            'Merchant' => $this->settings["amazon_seller_id"],
            'MarketplaceIdList' => array("Id" => $this->settings["amazon_marketplace_id"]),
            'FeedType' => '_POST_ORDER_FULFILLMENT_DATA_',
            'FeedContent' => $feedHandle,
            'PurgeAndReplace' => false,
            'ContentMd5' => base64_encode(md5(stream_get_contents($feedHandle), true)),
            'MWSAuthToken' => $this->settings["amazon_mws_auth_token"], // Optional
        );
        rewind($feedHandle);
        $request = new MarketplaceWebService_Model_SubmitFeedRequest($parameters);
        $response = $service->submitFeed($request);
        $submitFeedResult = $response->getSubmitFeedResult();
        $feedSubmissionInfo = $submitFeedResult->getFeedSubmissionInfo();
        $submission_id = $feedSubmissionInfo->getFeedSubmissionId();
        mysql_query("UPDATE `amazon_submissions` SET `amazon_id`='$submission_id' WHERE `id`='$table_id'");
        if ($fulfilmentType == "Fulfilment") {
            $this->log($internal_id, "Fulfilment feed sent to Amazon.");
            $this->updateStatus($internal_id, "Order number submitted");
        } else {
            $this->log($internal_id, "Shipping feed sent to Amazon.");
            $this->updateStatus($internal_id, "Tracking number submitted");
        }
    }

    function getFeedSubmissions() {
        $sql = mysql_query("SELECT * FROM `amazon_submissions` WHERE `status`='Processing' AND `amazon_id`!='' "
                . "OR `status`='SUBMITTED' AND `amazon_id`!='' "
                . "OR `status`='IN PROGRESS' AND `amazon_id`!='' "
                . "OR `status`='AWAITING ASYNCHRONOUS REPLY' AND `amazon_id`!='' "
                . "OR `status`='IN SAFETY NET' AND `amazon_id`!='' "
                . "OR `status`='UNCONFIRMED' AND `amazon_id`!='' LIMIT 50");
        $idList = array();
        while ($row = mysql_fetch_assoc($sql)) {
            $idList[] = $row["amazon_id"];
        }
        $config = array(
            'ServiceURL' => "https://mws.amazonservices.com",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'MaxErrorRetry' => 3,
        );
		
        $service = new MarketplaceWebService_Client(
                $this->settings["amazon_access_key"], $this->settings["amazon_secret_key"], $config, $this->settings["amazon_application_name"], $this->settings["amazon_application_version"]);
        $parameters = array(
            'Merchant' => $this->settings["amazon_seller_id"],
            'FeedSubmissionIdList' => array("Id" => $idList),
            'MWSAuthToken' => $this->settings["amazon_mws_auth_token"], // Optional
            "MaxCount" => 100,
        );
        if (!empty($idList)) {
            $request = new MarketplaceWebService_Model_GetFeedSubmissionListRequest($parameters);
            $response = $service->getFeedSubmissionList($request);

            if ($response->isSetGetFeedSubmissionListResult()) {
                $getFeedSubmissionListResult = $response->getGetFeedSubmissionListResult();
                $feedSubmissionInfoList = $getFeedSubmissionListResult->getFeedSubmissionInfoList();
                foreach ($feedSubmissionInfoList as $feedSubmissionInfo) {
                    $amazon_id = $feedSubmissionInfo->getFeedSubmissionId();
                    $status = trim(str_replace("_", " ", $feedSubmissionInfo->getFeedProcessingStatus()));
					
					t($status,1);

                    if ($status == "DONE") {
                        $message = mysql_real_escape_string($this->getFeedMessage($amazon_id));
                        $field = ",`message`='$message'";
                        $sql = mysql_query("SELECT * FROM `amazon_submissions` WHERE `amazon_id`='$amazon_id'");
                        $orderDetails = mysql_fetch_assoc($sql);
                        if ($message) {
                            if ($orderDetails["type"] == "Fulfilment") {
                                $this->log($orderDetails["order_id"], "Fulfilment Feed Error: " . $message, "ERROR");
                                $this->updateStatus($orderDetails["order_id"], "ERROR");
                            } else {
                                $this->log($orderDetails["order_id"], "Shipping Feed Error: " . $message, "ERROR");
                                $this->updateStatus($orderDetails["order_id"], "ERROR");
                            }
                        } else {
                            if ($orderDetails["type"] == "Fulfilment") {
                                $this->log($orderDetails["order_id"], "Fulfilment feed processed by Amazon.");
                                $sql = mysql_query("SELECT `order_id` FROM `amazon_orders` WHERE `id`='$orderDetails[order_id]'");
                                $amazon_order = mysql_fetch_assoc($sql);
                                $sql = mysql_query("INSERT INTO `amazon_order_checking`(`order_id`,`amazon_order_id`) VALUES('$orderDetails[order_id]','$amazon_order[order_id]')");
                                $this->log($orderDetails["order_id"], "Order queued to be refetched from Amazon.");
                            } else {
                                $this->log($orderDetails["order_id"], "Shipping feed processed by Amazon.");
                                $this->updateStatus($orderDetails["order_id"], "Processed");
                            }
                        }
                    } else {
                        $field = "";
                    }
					t("UPDATE `amazon_submissions` SET `status`='$status' $field WHERE `amazon_id`='$amazon_id'",1);
                    $sql = mysql_query("UPDATE `amazon_submissions` SET `status`='$status' $field WHERE `amazon_id`='$amazon_id'");
					
					sleep(5);
                }
            }
        }
    }

    function getFeedMessage($feed_id) {
        $config = array(
            'ServiceURL' => "https://mws.amazonservices.com",
            'ProxyHost' => null,
            'ProxyPort' => -1,
            'MaxErrorRetry' => 3,
        );
        $service = new MarketplaceWebService_Client(
                $this->settings["amazon_access_key"], $this->settings["amazon_secret_key"], $config, $this->settings["amazon_application_name"], $this->settings["amazon_application_version"]);
        $fileHandle = @fopen('php://memory', 'rw+');
        $parameters = array(
            'Merchant' => $this->settings["amazon_seller_id"],
            'FeedSubmissionId' => $feed_id,
            'FeedSubmissionResult' => $fileHandle,
            'MWSAuthToken' => $this->settings["amazon_mws_auth_token"], // Optional
        );
        $request = new MarketplaceWebService_Model_GetFeedSubmissionResultRequest($parameters);
        $service->getFeedSubmissionResult($request);
        rewind($fileHandle);
        $responseStr = stream_get_contents($fileHandle);
        $responseXML = new SimpleXMLElement($responseStr);		
        return $responseXML->Message->ProcessingReport->Result->ResultDescription;
    }

    function getLastFeedItem($order_id) {
        $sql = mysql_query("SELECT * FROM `amazon_submissions` WHERE `order_id`='$order_id' ORDER BY `id` DESC LIMIT 1");
        return mysql_fetch_assoc($sql);
    }
	function getOrdersByItem($id,$owner){
		$wheresql = array();
		$wheresql[] = "o.user_id='".Users::getOwnerId()."'";
		if($owner)
			$wheresql[] = "o.`sales_channel` in ('".implode("','",$GLOBALS['orders']['ownerChannel'])."')";
		else
			$wheresql[] = "o.`sales_channel` not in ('".implode("','",$GLOBALS['orders']['ownerChannel'])."')";
			
		if($id)$wheresql[] = "o.id='$id'";
		
		
		if(isset($_REQUEST["order"])){
			if ((int) $_REQUEST["order"] == 1) $wheresql[] = "o.`remote_order_id`!=''";
			else if ((int) $_REQUEST["order"] == 2) $wheresql[] = "(o.`remote_order_id`='' OR o.`remote_order_id` IS NULL)";
		}		
		if($_REQUEST["status"]){
			$wheresql[] = "i.`item_status` IN('".implode("','", $_REQUEST["status"])."')";
		}
		if($_REQUEST["location"]){
			$wheresql[] = "i.`item_location` IN('".implode("','", $_REQUEST["location"])."')";
		}
		if($_REQUEST["available"]){
			$wheresql[] = "i.`available_count` > 0";
		}
			
		$q = mysql_query("SELECT i.*, o.purchase_date, o.remote_order_id FROM amazon_order_items AS i
							LEFT JOIN amazon_orders AS o ON o.id=i.order_id
							WHERE ".implode(" AND ",$wheresql)."
							");
		$orders = array();
		while($r = mysql_fetch_assoc($q)){
			$r['purchase_date'] = tzdate('m/d/Y h:i:s A',$r['purchase_date']);
			$r['item_amount_formatted'] = number_format((float)$r['item_amount'],2);
			list($r['resolved'],$r['unresolved']) = $this->getOrderIssueCount($r['order_id']);	

			if($r['available_count'] == 0)$r['class'] = 'darkgray-bg';
			
			$orders[] = $r;
		}			
		return $orders;
	}
    function getOrderItems($amazon_id,$id=0) {
		$wheresql = array();
		
		if($amazon_id) $wheresql[] = "`amazon_order_id`='$amazon_id'";
		else $wheresql[] = "`order_id`='$id'";
	
        $sql = mysql_query("SELECT * FROM `amazon_order_items` WHERE ".implode(" AND ",$wheresql));
        if (mysql_num_rows($sql) == 0 && !$this->isOwnerOrder($amazon_id)) {
            $this->request = new MarketplaceWebServiceOrders_Model_ListOrderItemsRequest(array(
                "SellerId" => $this->settings["amazon_seller_id"],
                "MWSAuthToken" => $this->settings["amazon_mws_auth_token"],
                "AmazonOrderId" => $amazon_id
            ));
            $response = $this->service->ListOrderItems($this->request)->toXML();
            $response = simplexml_load_string($response);
            $rootElement = "ListOrderItemsResult";
            foreach ($response->$rootElement->OrderItems->OrderItem as $item) {
                $insertArr = array(
                    "amazon_order_id" => $amazon_id,
                    "ASIN" => $item->ASIN,
                    "SellerSKU" => $item->SellerSKU,
                    "OrderItemId" => $item->OrderItemId,
                    "Title" => $item->Title,
                    "QuantityOrdered" => $item->QuantityOrdered,
                    "ConditionId" => $item->ConditionId,
                    "ConditionSubtypeId" => $item->ConditionSubtypeId,
                );
                $keys = array();
                foreach ($insertArr as $key => $value) {
                    $keys[] = $key;
                    $insertArr[$key] = mysql_real_escape_string($value);
                }
                mysql_query("INSERT INTO `amazon_order_items`(`" . implode("`,`", $keys) . "`)"
                        . " VALUES('" . implode("','", $insertArr) . "')");
            }
            $sql = mysql_query("SELECT * FROM `amazon_order_items` WHERE `amazon_order_id`='$amazon_id'");
        }
        $out = array();
        while ($row = mysql_fetch_assoc($sql)) {
			$row['upc'] = $this->getUPC($row['SellerSKU']);			
			$row['inventory'] = $this->getAssignedInventory($row['id']);
            $out[] = $row;
        }
        return $out;
    }
	function getUPC($sku){
		uselib('spotter');
		
		$sp = new Spotter();
		$upc = $sp->getUPC($sku);
		return $upc;
	}	
	function getUniqueOwnerItems(){
		$titles = array();
		$wheresql = array();
		
		$wheresql[] = "o.`sales_channel` in ('".implode("','",$GLOBALS['orders']['ownerChannel'])."')";
		
		$q = mysql_query("SELECT DISTINCT i.Title FROM amazon_order_items AS i 
							LEFT JOIN amazon_orders AS o ON o.id=i.order_id 
							WHERE ".implode(" AND ",$wheresql)." ORDER BY i.Title ASC");
		while(list($title) = mysql_fetch_array($q))
			$titles[] = $title;
			
		return $titles;
	}
    
    function getAmazonId($id){
        $sql = mysql_query("SELECT `order_id` FROM `amazon_orders` WHERE `id`='$id'");
        $row = mysql_fetch_assoc($sql);
        return $row["order_id"];
    }
	
	function getInventory($availableOnly=false){
		$items = array();
		
		$wheresql = array();
		$wheresql[] = "o.user_id='".Users::getOwnerId()."'";
		$wheresql[] = "o.`sales_channel` in ('".implode("','",$GLOBALS['orders']['ownerChannel'])."')";
		if($availableOnly)$wheresql[] = "i.available_count>0";
		$q = mysql_query("SELECT i.* FROM amazon_order_items AS i 
							LEFT JOIN amazon_orders AS o ON o.id=i.order_id 
							WHERE ".implode(" AND ",$wheresql)."
							ORDER BY `Title` asc,i.`available_count` asc");
							
		while($r = mysql_fetch_assoc($q)){
			$k = $r['Title'];
			if(!$items[$k]){ $items[$k] = $r; $items[$k]['orders']=array($r['id']);}
			else{				
				$items[$k]['QuantityOrdered'] += $r['QuantityOrdered'];
				$items[$k]['received_count'] += $r['received_count'];
				$items[$k]['available_count'] += $r['available_count'];
				$items[$k]['used_count'] += $r['used_count'];
				$items[$k]['orders'][] = $r['id'];
			}
		}		
		return $items;
	}
	function getAvailableInventory(){
		$items = array();
		
		$q = mysql_query("SELECT i.* FROM amazon_order_items AS i 
							LEFT JOIN amazon_orders AS o ON o.id=i.order_id 
							WHERE o.`sales_channel` in ('".implode("','",$GLOBALS['orders']['ownerChannel'])."') AND i.available_count>0 
							ORDER BY `Title` asc");
							
		while($r = mysql_fetch_assoc($q))
			$items[] = $r;
		return $items;
	}
	function getInventorysAvailableCount($id){
		list($count) = mysql_fetch_array(mysql_query("SELECT available_count FROM amazon_order_items WHERE id='$id'"));
		return $count;
	}
	function getInventoryDetails($id){
		$data = mysql_fetch_assoc(mysql_query("SELECT * FROM amazon_order_items AS i 
												LEFT JOIN amazon_orders_portals AS p ON p.order_id=i.order_id
												WHERE i.id='$id'"));
		//t($data);
		return $data;
	}
	function updateInventoryAvailableCount($id){	
		list($received,$used) = mysql_fetch_array(mysql_query("SELECT received_count,used_count FROM amazon_order_items  WHERE id='$id'"));
		$avl = $received-$used;
		
		$updatesql = array();
		$updatesql[] = "available_count='$avl'";
		if($avl<=0 && $received>0)$updatesql[] = "item_status='Used'";
		mysql_query("UPDATE amazon_order_items SET available_count=received_count-used_count WHERE id='$id'");
	}
	function getItemQty($id){
		list($count) = mysql_fetch_array(mysql_query("SELECT QuantityOrdered FROM amazon_order_items WHERE id='$id'"));
		return $count;
	}
	function assignInventory($id,$itemId,$qty){		
		mysql_query("UPDATE amazon_order_items SET used_count=used_count+$qty WHERE id='$id'");		
		$this->updateInventoryAvailableCount($id);
		sql("INSERT INTO amazon_orders_inventory (inventory_id,item_id,qty) VALUES ('$id','$itemId','$qty')");
	}
	function unassignInventory($id,$itemId){
		$q = mysql_query("SELECT * FROM amazon_orders_inventory WHERE inventory_id='$id' AND item_id='$itemId'");
		while($r = mysql_fetch_assoc($q)){
			mysql_query("UPDATE amazon_order_items SET used_count=used_count-$r[qty] WHERE id='$r[inventory_id]'");	
		}
		$this->updateInventoryAvailableCount($id);					
		mysql_query("DELETE FROM amazon_orders_inventory WHERE inventory_id='$id' AND item_id='$itemId'");
	}
	
	
	function getAssignedInventory($id){
		$items = array();
		
		$q = mysql_query("SELECT i.* FROM amazon_orders_inventory AS oi
							LEFT JOIN amazon_order_items AS i ON i.id=oi.inventory_id
							WHERE oi.item_id='$id'");
		while($r = mysql_fetch_assoc($q))
			$items[] = $r;

		return $items;
	}
				
	//Costs
	public function getOrderCost($id){
		return reset($this->getOrderCosts(0,$id));
	}
	public function getOrderCosts($oId=0,$id=0){
		$costs = array();
		if(!$oId && !$id)return $costs;
		
		$wheresql = array();
		if($oId)$wheresql[] = "order_id='$oId'";
		if($id)$wheresql[] = "id='$id'";
		
		$q = mysql_query("SELECT * FROM amazon_orders_costs WHERE ".implode(" AND ",$wheresql));
		while($r = mysql_fetch_assoc($q)){
			$costs[] = (object)$r;
		}
		return $costs;
	}
	public function saveOrderCost($id=0,$data){
		$id = (int)$_REQUEST['id'];			
			
		$columns = array();
		$values = array();
		foreach($data as $k=>$v){
			$columns[] = "`".$k."`";
			$values[] = "'".$v."'";
		}
							
		if(!$id){
			sql("INSERT INTO amazon_orders_costs (".implode(",",$columns).") VALUES (".implode(",",$values).")");			
		}
		else{
			$update = array();
			foreach($columns as $i=>$c) $update[] = "$c = $values[$i]";
			sql("UPDATE amazon_orders_costs SET ".implode(",",$update)." WHERE id='$id'");
		}
	}
	public function removeOrderCost($id){
		sql("DELETE FROM amazon_orders_costs WHERE id='$id'");
	}
}

?>