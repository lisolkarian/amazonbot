<?php
uselib('amazon::amazon');
uselib('giftcards::giftcards');

class amazonReports extends amazon {
   var $settings;
  
    public function __construct($uId){
		$this->settings = array();
		parent::__construct($uId);				
	}
	
	public function getSummary($range){		
		if($range){$range = split(" - ",$range);}
				
		$data = array(
			'profit'		=> 0,
			'costs'			=> 0,
			'cashback'		=> 0,
			'points'		=> 0,
			'discount'		=> 0,
			'expenses'		=> 0,
			'grand_total'	=> 0,
			'orders'		=> 0,
		);
				
		$wheresql = array();
		if($range)$wheresql[] = "purchase_date BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' and '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";				
		$wheresql[] = "sales_channel != 'owner'";				
		$wheresql[] = "user_id='".Users::getOwnerId()."'";
		$wheresql[] = "order_status<>'Canceled'";
		
		$sql = "SELECT * FROM amazon_orders 					
					WHERE ".implode(" AND ",$wheresql)." 
					ORDER BY purchase_date ASC";
		$q = mysql_query($sql);	
		while($r = mysql_fetch_assoc($q)){												
			$order = $this->getOrder($r['id']);
			if(!$order['portal'])continue;
			
			$data['profit'] += $order['gains'];		
																					
			if($order['portal']){
				$data['costs'] += $order['portal']['portal_price']+$order['portal']['portal_tax']+$order['portal']['portal_shipping'];
				if($order['portal']['type'] == 'cashback') $data['estimated_cashback'] += (float)$order['cashback_earned'];
				else $data['estimated_points'] += (float)$order['points_earned'];
			}			
		}	
		
		
		$wheresql = array();
		$wheresql[] = "user_id='".Users::getOwnerId()."'";
		if($range)$wheresql[] = "purchased BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' and '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";						

		$sql = "SELECT * FROM giftcards 					
					WHERE ".implode(" AND ",$wheresql)." 
					ORDER BY purchased ASC";
		$q = mysql_query($sql);		
		while($r = mysql_fetch_assoc($q)){		
			$data['discount'] += ($r['amount']-$r['price']);			
		}	
		
		$exp = new Expenses();		
		$expenses = $exp->search(array('filter_range'=>$range));									
		foreach($expenses as $r){			
			$data['expenses'] += $r->amount; 			
		}		
		
		$data['grand_total'] += $data['profit'] + $data['discount'] + $data['cashback'] - $data['expenses']; 			
		$data['cost_ratio'] = ($data['costs'])?(100*$data['profit'])/$data['costs']:0;
		
		foreach($data as $k=>$v){
			$data[$k] = number_format($v,2);
		}		
		return $data;
	}
	/*
	public function getPendingAmazonPayments(){
		$total = 0;
		$az = new Amazon();
		$q = mysql_query("SELECT * FROM amazon_orders WHERE (tracking_number='' OR tracking_number IS NULL) AND fulfillment_channel='MFN' ORDER BY id DESC");
		while($r = mysql_fetch_assoc($q)){
			$order = $az->getOrder($r['id']);
		
			$payable = ($order['amount']+$order['fees']);
			$total+=$payable;
		
		
			t($order['order_id'].': '.$payable,1);
		}
		t("Total: ".$total);
		exit;
		
	}
	*/	
	public function getOrdersGraphData($range,$owner=false){								
		if($range){$range = split(" - ",$range);}
				
		$data = (object)array(
			'labels'	=> array(),
			'data'		=> array(),
			'series'	=> array(),
			'totals'	=> array(
				'orders' => (object)array('title'=>'Total Orders','value'=>0, 'prefix'=>''),
				'amount' => (object)array('title'=>'Total Amount','value'=>0, 'prefix'=>'$'),
			),
		);
				
		$wheresql = array();
		if($range)$wheresql[] = "purchase_date BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' and '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";		
		//if($days)$wheresql[] = "purchase_date BETWEEN '".date('Y-m-d',strtotime("-{$days} days"))." 00:00:00' and '".date('Y-m-d h:i:s',strtotime('now'))."'";
		if($owner)$wheresql[] = "sales_channel = 'owner'";
		else $wheresql[] = "sales_channel != 'owner'";
		
		$wheresql[] = "user_id='".Users::getOwnerId()."'";

		$sql = "SELECT * FROM amazon_orders 					
					WHERE ".implode(" AND ",$wheresql)." 
					ORDER BY purchase_date ASC";
		$q = mysql_query($sql);
		//t($sql);
		while($r = mysql_fetch_assoc($q)){
			$date = date('m/d/y',strtotime($r['purchase_date']));
			$data->chartData['labels'][] = $date;			
			if(!$data->chartData['data'][$date])$data->chartData['data'][$date]=0;
			$data->chartData['data'][$date]++;	
			
			$data->totals['orders']->value++;
			$data->totals['amount']->value += (float)$r['amount']+(float)$r['fees'];
		}		
		$data->chartData['labels'] = array_unique($data->chartData['labels']);		
		
		$series = array();
		foreach($data->chartData['labels'] as $i=>$l){			
			$hits = ($data->chartData['data'][$l])?$data->chartData['data'][$l]:0;
			$series[] = array($l,$hits);			
		}						
		$data->chartData['series'] = $series;
		
		foreach($data->totals as $k=>$v){
			$data->totals[$k]->value = number_format($data->totals[$k]->value,0);
		}
		
		return $data;
	}
	public function getProfitGraphData($range,$owner=false){								
		if($range){$range = split(" - ",$range);}
				
		$data = (object)array(
			'labels'	=> array(),
			'data'		=> array(),
			'series'	=> array(),
			'totals'	=> array(
				'profit' => (object)array('title'=>'Profit','value'=>0, 'prefix'=>'$'),
				'estimated_profit' => (object)array('title'=>'Estimated Profit','value'=>0, 'prefix'=>'$'),
			),
		);
				
		$wheresql = array();
		if($range)$wheresql[] = "purchase_date BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' and '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";		
		//if($days)$wheresql[] = "purchase_date BETWEEN '".date('Y-m-d',strtotime("-{$days} days"))." 00:00:00' and '".date('Y-m-d h:i:s',strtotime('now'))."'";
		if($owner)$wheresql[] = "sales_channel = 'owner'";
		else $wheresql[] = "sales_channel != 'owner'";
		$wheresql[] = "order_status<>'Canceled'";
		
		$wheresql[] = "user_id='".Users::getOwnerId()."'";

		$sql = "SELECT * FROM amazon_orders 					
					WHERE ".implode(" AND ",$wheresql)." 
					ORDER BY purchase_date ASC";
		$q = mysql_query($sql);
		//t($sql);
		while($r = mysql_fetch_assoc($q)){
			$order = $this->getOrder($r['id']);
			if(!$order['portal'])continue;
				
			
			$date = date('m/d/y',strtotime($r['purchase_date']));
			$data->chartData['labels'][] = $date;			
			if(!isset($data->chartData['data'][$date]))$data->chartData['data'][$date]=array();
			
			
			$profit = (float)$order['gains'];										
			
			//$data->chartData['data'][$date]['net'] += $order['amount_net'];
			$data->chartData['data'][$date]['net'] += $profit;			
			$data->chartData['data'][$date]['estimated_net'] += $order['amount_net_estimated'];	

			$data->totals['profit']->value += $profit;
			$data->totals['estimated_profit']->value += $order['amount_net_estimated'];
		}				
		$data->chartData['labels'] = array_unique($data->chartData['labels']);		
		
		$series = array();
		foreach($data->chartData['labels'] as $i=>$l){	
			$net = ($data->chartData['data'][$l]['net'])?$data->chartData['data'][$l]['net']:0;
			$estimated = ($data->chartData['data'][$l]['estimated_net'])?$data->chartData['data'][$l]['estimated_net']:0;
			$series[] = array($l,$net,$estimated);			
		}		
		$data->chartData['series'] = $series;
		
		foreach($data->totals as $k=>$v){
			$data->totals[$k]->value = number_format($data->totals[$k]->value,0);
		}
		
		return $data;
	}
		
	public function getGiftcardsGraphData($range){								
		if($range){$range = split(" - ",$range);}
		$data = (object)array(
			'labels'	=> array(),
			'data'		=> array(),
			'series'	=> array(),
			'totals'	=> array(
				'amount' => (object)array('title'=>'Total Balance','value'=>0, 'prefix'=>'$'),				
			),			
		);
				
		$wheresql = array();		
		$wheresql[] = "user_id='".Users::getOwnerId()."'";
		
		if($range)$wheresql[] = "purchased BETWEEN '".date('Y-m-d',strtotime($range[0]))."' and '".date('Y-m-d',strtotime($range[1]))."'";		

		$sql = "SELECT * FROM giftcards
					WHERE ".implode(" AND ",$wheresql)." 
					ORDER BY store ASC";
		$q = mysql_query($sql);
		//t($sql);
		while($r = mysql_fetch_assoc($q)){
			$label = $r['store'];
			$data->chartData['labels'][] = $label;			
			if(!isset($data->chartData['data'][$label]))$data->chartData['data'][$label]=array();
						
			$data->chartData['data'][$label]['balance'] += $r['balance'];			
			//$data->chartData['data'][$label]['amount'] += $r['amount'];	

			$data->totals['amount']->value += $r['balance'];
			
		}				
		$data->chartData['labels'] = array_unique($data->chartData['labels']);		
		
		$series = array();
		foreach($data->chartData['labels'] as $i=>$l){	
			$balance = ($data->chartData['data'][$l]['balance'])?$data->chartData['data'][$l]['balance']:0;			
			//$amount = ($data->chartData['data'][$l]['amount'])?$data->chartData['data'][$l]['amount']:0;			
			//$series[] = array($l,$balance,$amount);			
			$series[] = array($l,$balance);			
		}		
		$data->chartData['series'] = $series;
		
		foreach($data->totals as $k=>$v){
			$data->totals[$k]->value = number_format($data->totals[$k]->value,0);
		}
		
		return $data;
	}
	public function getGiftcardsPurchasedGraphData($range,$store=false){
		if($range){$range = split(" - ",$range);}
		
		$data = (object)array(
			'labels'	=> array(),
			'data'		=> array(),
			'series'	=> array(),
			'totals'	=> array(
				'amount' => (object)array('title'=>'Total Value Added','value'=>0, 'prefix'=>'$'),				
				'cost' => (object)array('title'=>'Total Cost','value'=>0, 'prefix'=>'$'),				
				'discount' => (object)array('title'=>'Total Discount','value'=>0, 'prefix'=>'$'),				
				'avg_discount' => (object)array('title'=>'Average Discount','value'=>0, 'postfix'=>'%'),				
			),			
		);
				
		$wheresql = array();
		$wheresql[] = "user_id='".Users::getOwnerId()."'";
		if($range)$wheresql[] = "purchased BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' and '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";				
		if($store)$wheresql[] = "store = '$store'";		
		

		$sql = "SELECT * FROM giftcards 					
					WHERE ".implode(" AND ",$wheresql)." 
					ORDER BY purchased ASC";
		$q = mysql_query($sql);
		//t($sql);
		while($r = mysql_fetch_assoc($q)){
			$date = date('m/d/y',strtotime($r['purchased']));
			$data->chartData['labels'][] = $date;			
			if(!isset($data->chartData['data'][$date]))$data->chartData['data'][$date]=array();
			
			//$order = $this->getOrder($r['id']);			
			$data->chartData['data'][$date]['amount'] += $r['amount'];
			$data->chartData['data'][$date]['cost'] += $r['price'];
			
			$data->totals['amount']->value += $r['amount'];
			$data->totals['cost']->value += $r['price'];
			$data->totals['discount']->value += ($r['amount']-$r['price']);			
		}				
		$data->chartData['labels'] = array_unique($data->chartData['labels']);		
				
		$series = array();
		$diff_p_total = 0;
		foreach($data->chartData['labels'] as $i=>$l){	
			$amount = ($data->chartData['data'][$l]['amount'])?$data->chartData['data'][$l]['amount']:0;			
			$cost = ($data->chartData['data'][$l]['cost'])?$data->chartData['data'][$l]['cost']:0;			
			$diff = $amount-$cost;					
			$series[] = array($l,$amount,$cost,$diff);
			
			//Average discount calc
			$diff_p_total += ($amount)?(100*($amount-$cost))/$amount:0;			
		}								
		$data->chartData['series'] = $series;
		
		foreach($data->totals as $k=>$v){
			$data->totals[$k]->value = number_format($data->totals[$k]->value,0);
		}					
		$data->totals['avg_discount']->value = (count($data->chartData['labels']))?number_format($diff_p_total/count($data->chartData['labels']),2):'N/A';
		
		
		return $data;
	}
	public function getPortalGraphData($range,$portal=false){								
		if($range){$range = split(" - ",$range);}
		$data = (object)array(
			'labels'	=> array(),
			'data'		=> array(),
			'series'	=> array(),
			'totals'	=> array(
				'cashback' => (object)array('title'=>'Total Cashback','value'=>0, 'prefix'=>'$'),
				'points' => (object)array('title'=>'Total Points','value'=>0),
			),			
		);
				
		$wheresql = array();		
		$wheresql[] = "p.user_id='".Users::getOwnerId()."'";
		if($portal)$wheresql[] = "ap.portal_id = '$portal'";				
		if($range)$wheresql[] = "ap.timestamp BETWEEN '".date('Y-m-d',strtotime($range[0]))."' and '".date('Y-m-d',strtotime($range[1]))."'";		

		$sql = "SELECT p.*,ap.* FROM amazon_orders_portals AS ap
					LEFT JOIN portals AS p ON ap.portal_id=p.id						
					WHERE ".implode(" AND ",$wheresql)." 
					ORDER BY ap.timestamp ASC";
		$q = mysql_query($sql);
		//t($sql);
		while($r = mysql_fetch_assoc($q)){			
			$date = date('m/d/y',strtotime($r['timestamp']));
			$data->chartData['labels'][] = $date;			
			if(!isset($data->chartData['data'][$date]))$data->chartData['data'][$date]=array();
			
			if($r['type'] == 'cashback'){										
				$data->chartData['data'][$date]['cashback'] += $r['portal_cashback'];
				$data->totals['cashback']->value += $r['portal_cashback'];
			}
			else{
				$data->chartData['data'][$date]['points'] += $r['portal_points'];
				$data->totals['points']->value += $r['portal_points'];
			}						
		}				
		$data->chartData['labels'] = array_unique($data->chartData['labels']);		
		
		$series = array();
		foreach($data->chartData['labels'] as $i=>$l){	
			$cashback = ($data->chartData['data'][$l]['cashback'])?$data->chartData['data'][$l]['cashback']:0;						
			$points = ($data->chartData['data'][$l]['points'])?$data->chartData['data'][$l]['points']:0;						
			$series[] = array($l,$cashback,$points);			
		}		
		$data->chartData['series'] = $series;
		
		foreach($data->totals as $k=>$v){
			$data->totals[$k]->value = number_format($data->totals[$k]->value,0);
		}
		
		return $data;
	}
	
	public function getExpensesGraphData($range,$tags=false){								
		//if($range){$range = split(" - ",$range);}
		$data = (object)array(
			'labels'	=> array(),
			'data'		=> array(),
			'series'	=> array(),
			'totals'	=> array(
				'amount' => (object)array('title'=>'Total Amount','value'=>0, 'prefix'=>'$'),				
			),			
		);
			
		$exp = new Expenses();
		$filter = array();
		$filter['filter_range'] = $range;
		if($tags) $filter['filter_tags'] = $tags;
		$expenses = $exp->search($filter);
					
		foreach($expenses as $r){
			$date = date('m/d/y',strtotime($r->date));
			$data->chartData['labels'][] = $date;			
			if(!isset($data->chartData['data'][$date]))$data->chartData['data'][$date]=array();
						
			$data->chartData['data'][$date]['amount'] += $r->amount;
			$data->totals['amount']->value += $r->amount;			
		}				
		$data->chartData['labels'] = array_unique($data->chartData['labels']);		
		
		$series = array();
		foreach($data->chartData['labels'] as $i=>$l){	
			$amount = ($data->chartData['data'][$l]['amount'])?$data->chartData['data'][$l]['amount']:0;									
			$series[] = array($l,$amount);			
		}		
		$data->chartData['series'] = $series;
		
		foreach($data->totals as $k=>$v){
			$data->totals[$k]->value = number_format($data->totals[$k]->value,0);
		}
		
		return $data;
	}
	
	
	
}
