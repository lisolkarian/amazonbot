<?php
uselib('amazon::amazon');

class amazonFeedback extends amazon {
   var $settings;
  
    public function __construct($uId){
		$this->settings = array(
			'type'	=> '_GET_SELLER_FEEDBACK_DATA_'
		);
		parent::__construct($uId);				
	}

	public function getFeedback($id){
		$res = $this->search(array('id'=>$id));
		return ($res)?reset($res):false;
	}
	public function search($filter=array()){
		$array = array();
		
		$wheresql = array();
		$wheresql[] = "user_id='{$this->userId}'";
		foreach($filter as $k=>$v){ 
			switch($k){
				case 'range':					
					$wheresql[] = "`date` BETWEEN '".date('Y-m-d',strtotime($filter['range'][0]))."' AND '".date('Y-m-d',strtotime($filter['range'][1]))."'";
				case 'filter_timeback':
					if($filter["filter_timeback"]>0)$wheresql[] = "`date` BETWEEN DATE(NOW() - INTERVAL {$filter["filter_timeback"]} DAY) AND DATE(NOW())";
					break;
				default:
					$wheresql[] = "`$k`='$v'";
					break;
			}							
		}
		
		$q = mysql_query("SELECT * FROM amazon_feedback WHERE ".implode(" AND ",$wheresql)." ORDER BY date DESC");
		while($r = mysql_fetch_assoc($q)){
			$r['comments_preview'] = strshorten($r['comments'],100);
			$r['date'] = date('m/d/Y',strtotime($r['date']));
			$array[] = (object)$r;
		}
		
		return $array;
	}
	public function resetFeedback(){
		mysql_query("DELETE FROM amazon_feedback WHERE user_id='{$this->userId}'");
		mysql_query("DELETE FROM amazon_feedback_reports WHERE user_id='{$this->userId}'");
		
		foreach(range(date('Y',strtotime("-6 years")),date('Y')) as $y){
			$range = array("{$y}-01-01 00:00:00","{$y}-12-31 23:59:59");
			$this->submitRequest($range);
		}
	}
	public function updateFeedback(){
		$this->updatePendingReports();
		$pending = $this->getPendingRequests();
		if(empty($pending))$this->submitRequest(array(date('Y-m-d H:i:s',strtotime('-365 days')),date('Y-m-d H:i:s',strtotime('now'))));
		$reports = $this->getReadyReports();
		foreach($reports as $r){	
			$this->markRequestProcesses($r->request_id);			
			if($r->report_id){
				$this->parseReport($r);
			}
			if(count($reports)>1){ sleep(30); }
		}	
	}
	private function getReadyReports(){
		$reports = array();
		$q = mysql_query("SELECT * FROM amazon_feedback_reports WHERE status='ready' AND user_id='{$this->userId}'");
		while($r = mysql_fetch_assoc($q)){
			$reports[] = (object)$r;
		}
		return $reports;
	}
	private function updatePendingReports(){
		$pending = $this->getPendingRequests();
		foreach($pending as $r){
			$this->checkRequest($r->request_id);
		}
	}
	private function parseReport($r){	
		$report = $this->getReport($r->report_id);
		$csv = preg_split("/\n\r|\r\n|\n|\r/",$report); //str_getcsv($report);		
		
		if($r->start_date && $r->end_date){
			$cur = $this->search(array('range'=>array($r->start_date,$r->end_date)));
		}		
		
		$new = array();
		if($csv){					
			$cols = str_getcsv(array_shift($csv),"\t");			
			foreach($csv as $row){
				if(!trim($row))continue;
				$data = array();
				foreach(preg_split("/\t/",$row) as $i=>$v) $data[$cols[$i]] = $v;
				
				$fields = (object)array(
					'user_id' => $this->userId,
					'date' => dbDate($data["Date"]),
					'rating' => $data["Rating"],
					'comments' => $data["Comments"],
					'response' => $data["Your Response"],
					'arrived_on_time' => $data["Arrived on Time"],
					'item_as_described' => $data["Item as Described"], 
					'customer_service' => $data["Customer Service"],
					'order_id' => $data["Order ID"],
					'email' => $data["Rater Email"],
					'role' => $data["Rater Role"]
				);	
				$new[] = $fields;
			}
		}
		
		
		//Delete removed feedback
		foreach($cur as $f1){
			$flag = 0;
			foreach($new as $f2){
				if($this->compareFeedbacks($f1,$f2)){ $flag=1; }
			}

			if(!$flag){
				t("Removing: ".$f1->id,1);
				$this->removeFeedback($f1->id);
			}
		}				

		//Add missing feedback
		foreach($new as $f){
			$updateSql = array();
			foreach($f as $k=>$v){
				$updateSql[] = "`$k`='".mysql_real_escape_string($v)."'";
			}

			$found = $this->findFeedback($f);			
			if($found)$sql = "UPDATE amazon_feedback SET ".implode(",",$updateSql)." WHERE id='$found'";				
			else $sql = "INSERT INTO amazon_feedback SET ".implode(",",$updateSql);												
			
			t($sql,1);
			mysql_query($sql);
		}		
		
	}
	private function removeFeedback($id){
		mysql_query("DELETE FROM amazon_feedback WHERE id='$id'");
	}
	private function findFeedback($data){
		#t("SELECT id FROM amazon_feedback WHERE date='".dbDate($data->date)."' AND order_id='{$data->order_id}' AND email='{$data->email}'",1);
		list($id) = mysql_fetch_array(mysql_query("SELECT id FROM amazon_feedback WHERE date='".dbDate($data->date)."' AND order_id='{$data->order_id}' AND email='{$data->email}'"));
		return $id;
	}
	private function compareFeedbacks($f1,$f2){
		#t($f1->comments."<br>".$f1->order_id."<br>".$f1->email."<br>".$f1->date,1);
		#t($f2->comments."<br>".$f2->order_id."<br>".$f2->email."<br>".$f2->date,1);
		return ($f1->comments==$f2->comments && $f1->order_id==$f2->order_id && $f1->email==$f2->email && dbDate($f1->date)==dbDate($f2->date))?true:false;
	}
	private function markRequestProcesses($rId){
		mysql_query("UPDATE amazon_feedback_reports SET status='processed' WHERE request_id='$rId'");
	}
	private function removeRequest($id){
		mysql_query("DELETE FROM amazon_feedback_reports WHERE id='$id'");
	}
	private function getPendingRequests(){
		$requests = array();
		$q = mysql_query("SELECT * FROM amazon_feedback_reports WHERE user_id='{$this->userId}' AND status='pending'");
		while($r = mysql_fetch_assoc($q)){
			$requests[] = (object)$r;
		}
		return $requests;		
	}
	public function submitRequest($range=array()){
		$rId = $this->requestReport($this->settings['type'],$range);				
		if($rId){
			mysql_query("INSERT INTO amazon_feedback_reports (`user_id`,`request_id`,`status`,`start_date`,`end_date`) VALUES ('{$this->userId}','$rId','pending','$range[0]','$range[1]')");
			return true;
		}
		return false;
	}	
	private function checkRequest($rId){
		$response = $this->getReportRequests($rId);		
		
		if ($response->isSetGetReportRequestListResult()) {                     
			$getReportRequestListResult = $response->getGetReportRequestListResult();
                    
			$nextToken = ($getReportRequestListResult->isSetNextToken())?$getReportRequestListResult->getNextToken():false;
			//Use token to navigate pages. No needed at time of implementation.
                    
			$reportRequestInfoList = $getReportRequestListResult->getReportRequestInfoList();
			foreach ($reportRequestInfoList as $reportRequestInfo) {                        
				$status = ($reportRequestInfo->isSetReportProcessingStatus())?$reportRequestInfo->getReportProcessingStatus():false;
				$reportId = ($reportRequestInfo->isSetGeneratedReportId())?$reportRequestInfo->getGeneratedReportId():false;
				if($status != '_SUBMITTED_' && $status !='_IN_PROGRESS_'){
					mysql_query("UPDATE amazon_feedback_reports SET `report_id`='$reportId',`status`='ready' WHERE request_id='$rId'");
					return $reportId;
				}
				else
					return false;
			}
		}
		return false;
	}
}