<?php 
uselib('process');

Class Fivek{
	function __construct(){
	}
	
	public function create($asin,$keywords){
		if(!is_array($keywords))return false;
		if(!$asin)return false;
		
		mysql_query("INSERT INTO fivek_searches SET asin='$asin', user_id='{$_SESSION['user']->id}'");
		$error = mysql_error();
		if($error)return false;
		$id = mysql_insert_id();
		
		foreach($keywords as $k){
			$k = trim($k);
			if(!$k)continue;
			mysql_query("INSERT INTO fivek_searches_sessions SET search_id='$id', keyword='$k'");
		}

		return true;
	}
	
	public function reset($id){
		sql("UPDATE fivek_searches_sessions SET pending=1, error='', result=0 WHERE search_id='$id'");
	}
	public function getSearch($id){		
		$q = mysql_query("SELECT * FROM fivek_searches WHERE id='$id'");
		$item = mysql_fetch_assoc($q);
		
		if($item)$item = $this->format($item);
		
		return $item;
	} 	
	public function getKeywords($sId){
		$items = array();
		
		$q = mysql_query("SELECT * FROM fivek_searches_sessions WHERE search_id='$sId'");
		while($r = mysql_fetch_assoc($q)){
			$items[] = $this->formatKeyword($r);
		}

		return $items;
	}
	private function calculateStats($keywords){
		$stats = (object)array(
			'pending'	=> 0,
			'errors'	=> 0,
			'done'		=> 0,
			'progress'	=> 0,
			'total'		=> 0,
			'found'		=> 0,
			'notfound'	=> 0,
		);
		if(!$keywords)return $stats;
		
		foreach($keywords as $k){
			$stats->total++;
			
			if($k->pending) $stats->pending++;									
			else{
				$stats->done++;
				if($k->error){
					$stats->errors++;
				}
				else{
					if($k->result) $stats->found++;
					else $stats->notfound++;
				}
			}									
		}
		
		
		$stats->progress = ($stats->total>0)?100*($stats->error+$stats->done)/$stats->total:0;
		
		return $stats;
	}
	private function getProduct($asin){
		$product = mysql_fetch_assoc(mysql_query("SELECT * FROM products WHERE asin='$asin'"));
		
		$product['title'] = strshorten($product['title'],50);
		
		return ($product)?(object)$product:false;
	}
	
	public function format($r){
		$r['product'] = $this->getProduct($r['asin']);
		$r['keywords'] = $this->getKeywords($r['id']);
		$r['stats'] = $this->calculateStats($r['keywords']);
		
		$r['timestamp'] = date('m/d/Y h:iA',strtotime($r['timestamp']));
		
		return (object)$r;
	}
	public function formatKeyword($r){		
		return (object)$r;
	}
	
	static public function sessionError($sId,$error){
		$error = mysql_real_escape_string($error);
		
		mysql_query("UPDATE fivek_searches_sessions SET error='$error', pending=0, result=0 WHERE id='$sId'");		
	}
	static public function sessionOk($sId,$res){
		$res = mysql_real_escape_string($res);
	
		mysql_query("UPDATE fivek_searches_sessions SET error='', pending=0, result='$res' WHERE id='$sId'");
	}
	static public function sessionRun($sId){
		$sql = "SELECT ss.*, s.asin FROM fivek_searches_sessions AS ss
				LEFT JOIN fivek_searches AS s ON s.id=ss.search_id 
				WHERE ss.id='$sId'";
		//t($sql);	
		$q = mysql_query($sql);
		$r = mysql_fetch_assoc($q);
						
		mysql_query("UPDATE products_sessions SET pending=0 WHERE id='$sId'");		
		if(!$r['asin'] || !$r['keyword'])self::sessionError($r['id'],'Missing Info');
		
		$res = self::fetchPage($r['asin'],$r['keyword']);
		
		preg_match("/{$r['asin']}/",$res,$matches);
		if(!$matches){ self::sessionError($r['id'],"Unexpected Error"); }
		else{				
			preg_match("/(\d) result for/",$res,$matches);
			if($matches && $matches[1]){ $found = true; }
			else $found = false;
		
			if($found) self::sessionOk($r['id'],1);
			else self::sessionOk($r['id'],0);
		}									
	}
	static public function keywordRun($asin,$keyword){
		$res = self::fetchPage($asin,$keyword);
		
		preg_match("/{$r['asin']}/",$res,$matches);
		if(!$matches){ return 'error'; }
		else{
			preg_match("/(\d) result for/",$res,$matches);
			if($matches && $matches[1]){ $found = true; }
			else $found = false;
		
			if($found) return 1;
			else return 0;
		}
	}
	private function fetchPage($asin,$keyword){
		$proxyProvider = '';					//Blank for default.
		switch($proxyProvider){
			case 'RP':
				$proxy = getProxyRotating();
				break;
			case '"Microleaves"':
				$proxy = getProxyMicroleaves();
				break;
			default:
				$proxy = getProxy();
				break;
		}
		$useragent = getUseragent();
		
		$url = "https://www.amazon.com/s?field-keywords=".urlencode($keyword)."+".$asin;
		$res = curl_get($url,array(),$proxy->proxy,$useragent);
		
		return $res;
	} 
	
	
	//Launcher
	function start($id,$debug=false){
		$vars = array(
			'id'		=> $id			
		);		
		
		//$cmd = "perl '{$GLOBALS['system']['path']}/perl/fivek.pl' '".json_encode($vars)."'";		
		$cmd = "php '{$GLOBALS['system']['path']}/{$GLOBALS['system']['util_path']}/fivekSession.util.php' '".json_encode($vars)."'";
		if($debug)t($cmd);

		$process = new Process($cmd);
		$process->start();		
		$pid = $process->getPid();
		
		mysql_query("UPDATE fivek_searches_sessions SET pid='$pid' WHERE id='$id'");
		
		return $pid;
	}
    function isActive($pid){
	    return self::checkPid($pid);
	}
	static public function checkPid($pid){
		$process = new Process();
		$process->setPid($pid);
		$status = $process->status();
		$status = ($status==true)?1:0;
		return $status;
	}	 
}

?> 