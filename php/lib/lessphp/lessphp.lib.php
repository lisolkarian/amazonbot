<?php
include_once 'lessc.inc.php';

$cache_dir   = $GLOBALS['system']['cache_path'];
$less_file   = $GLOBALS['system']['path'] ."{$_GET['f']}";
$css_file    =  $cache_dir . '/' . preg_replace('/\.less/', '.css', $_GET['f']);
$cache_file  =  $cache_dir . '/' . preg_replace('/\.less/', '.cache', $_GET['f']);
$enable_gzip = !empty($_SERVER['HTTP_ACCEPT_ENCODING']) && in_array('gzip', explode(',', $_SERVER['HTTP_ACCEPT_ENCODING']));
if (!is_file($less_file)) {
	header('HTTP/1.0 404 Not Found');
	die();
}
if (!is_dir(dirname($css_file))) {
	mkdir(dirname($css_file), 0755, true);
}
$less = new lessc;
$less->setFormatter("compressed");
try {

  if (file_exists($cache_file))$cache = unserialize(file_get_contents($cache_file));
  else $cache = $less_file;    
  $newCache = $less->cachedCompile($cache);
  if (!is_array($cache) || $newCache["updated"] > $cache["updated"]) {
    file_put_contents($cache_file, serialize($newCache));
    file_put_contents($css_file, $newCache['compiled']);
  }
  
} catch (Exception $e) {
	//header('HTTP/1.0 500 Internal Server Error');
	echo $e->getMessage();
	die();
}

$fp = fopen($css_file, 'r');
$stat = fstat($fp);
if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) && strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) >= $stat['mtime']) {
	header('HTTP/1.0 304 Not Modified');
} else {
	header('Cache-Control: must-revalidate');
	header('Content-Type: text/css; charset=utf-8');
	header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $stat['mtime']) . ' GMT');
	if ($enable_gzip) {
		header('Content-Encoding: gzip');
		ob_start("ob_gzhandler");
	}		
	fpassthru($fp);
}
fclose($fp);