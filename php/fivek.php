<?php
session_write_close();

uselib('fivek');
$fivek = new Fivek();
	
usehelper("ajax::dispatch");

function updateSearches(){
	global $fivek;
	
	$items = array();
	$ids = $_REQUEST['ids'];
	
	if($ids){
		foreach($ids as $id){
			$items[] = $fivek->getSearch($id);
		}
	}		
	json(array('items'=>$items));
}
function quickCheck(){
	global $fivek;
	
	$asin = $_REQUEST['asin'];
	$keyword = $_REQUEST['keyword'];	
	
	$res = Fivek::keywordRun($asin,$keyword);
	
	json(array('result'=>$res));
}
function redoSearch(){
	global $fivek;
	
	$id = $_REQUEST['id'];
	
	$fivek->reset($id);	
}
function createSearch(){
	global $fivek;
	
	$asin = $_REQUEST['asin'];
	$keywords = explode("\\r\\n",$_REQUEST['keywords']);
	
	$res = $fivek->create($asin,$keywords);
	if(!$res)err("Search not created!");
	
	json();	
}
function loadSearch(){
	global $fivek;
	
	$id = (int)$_REQUEST['id'];
	
	$item = $fivek->getSearch($id);			
	if(!$item)err("Item not found!");
	
	json(array('item'=>$item));
}
function loadSearches(){
	global $fivek;
	
	$sortColumns = array('timestamp','asin','','','','');
	$array = array();
		
	$offset = (int)$_REQUEST['start'];
	$length = (int)$_REQUEST['length'];

	if($_REQUEST['order'])$orderby = array('col'=>$sortColumns[$_REQUEST['order'][0]['column']],'dir'=>$_REQUEST['order'][0]['dir']);

	$offset = $offset;
	$length = (int) $length;
	if ($length)
		$limit = "LIMIT $offset,$length";
	else
		$limit = "";

	if (!$orderby)
		$orderby = array('timestamp DESC');
	else
		$orderby = array($orderby['col'] . " " . $orderby['dir']);


	$wheresql = array();
	$wheresql[] = "user_id='{$_SESSION['user']->id}'";
	$filter = $_REQUEST['filter'];
	if($filter){
		foreach($filter as $k=>$v){
			if(is_array($v) || trim($v)){
				switch($k){	
					case 'asin':
						$wheresql[] = "asin LIKE '%$v%'";
						break;				
					default:
						break;
				}
			}
		}
	}


	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM fivek_searches WHERE ".implode(" AND ",$wheresql)." ORDER BY  " . implode(' ', $orderby) . " $limit";
	//t($sql);
	$q = mysql_query($sql);
	list($total) = mysql_fetch_array(mysql_query("SELECT FOUND_ROWS();"));
	while($r = mysql_fetch_assoc($q)){
		$r = $fivek->format($r);		
		$array[] = $r;
	}
	json(array(
	'data'=> $array,
	'total' => $total,
	'page' => $offset,
	'sort'	=> ($sortby)?$sortby['col']:$_REQUEST['order'][0]['column'],
	'sortDir' => ($sortby)?$sortby['dir']:$_REQUEST['order'][0]['dir'],
	'length' => $length,
	));
}
function removeSearch(){
	$id = (int)$_REQUEST['id'];

	sql("DELETE FROM fivek_searches WHERE id='$id'");
}
function formatSearch($r){	
	return (object)$r;
}
function importKeywords(){	
	$csv_mimetypes = array(
			'text/csv',
			'text/plain',
			'application/csv',
			'text/comma-separated-values',
			'application/excel',
			'application/vnd.ms-excel',
			'application/vnd.msexcel',
			'text/anytext',
			'application/octet-stream',
			'application/txt',
	);

	$error = '';
	if ($_FILES['file']['error'] !== UPLOAD_ERR_OK) $error = "Upload failed with error code " . $_FILES['file']['error'];
	else if ($info === FALSE) $error = "Unable to determine image type of uploaded file";
	else if (!in_array($_FILES['file']['type'], $csv_mimetypes)) $error = "Invalid file uploaded";
	else if ($_FILES['file']['error'] != 0) $error = "Unkown Error";

	if($error){
		if(file_exists($_FILES["file"]["tmp_name"]))unlink($_FILES["file"]["tmp_name"]);
		err($error);
	}

	$items = array();
	if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
		while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
			$items[] = $row[0];
		}
	}
	fclose($handle);
	unlink($_FILES["file"]["tmp_name"]);

	json(array('items'=>$items));
}