<?php
session_write_close();
	
usehelper("ajax::dispatch");

function getBadgesHistory(){
	$id = $_REQUEST['id'];
	
	$range = $_REQUEST['range'];
	if(!$range) $range = date('m/d/y',strtotime("-30 days")).' - '.date('m/d/y',strtotime("now"));
	$range = split(" - ",$range);
			
	$data = (object)array('items'=>array(),'chart'=>array());
	$data->items = getKeywordBadgeHistory($id,$range);	
	
	if($data->items){
		$chart = (object)array(
				'labels'	=> array(),
				'series'	=> array()
		);
		foreach($data->items as $r){
			$date = strtotime(date('y-m-d',strtotime($r->timestamp)));
			$badge=($r->badge)?1:0;
	
			$chart->labels[] = $date;
			$chart->series[] = array(date('m/d/y',$date),$badge);
				
		}
		$data->chart = $chart;
	}
	
	/*

	
	$chart = (object)array(
		'series'	=> array()
	);
	
	if($items){
		$found = 0;
		$total = 0;
		$chart->series[] = array("Badge","Count");
		foreach($items as $r){
			$total = $r->total;
			$chart->series[] = array($r->title,$r->count);
			$found+=$r->count;
		}
		$chart->series[] = array("No Badge",$total-$found);
	}
	*/
	
	json($data);
	return;
}
function getHistory(){
	$id = $_REQUEST['id'];
	
	$range = $_REQUEST['range'];
	if(!$range) $range = date('m/d/y',strtotime("-30 days")).' - '.date('m/d/y',strtotime("now"));
	$range = split(" - ",$range);
	
	$items = getRanks($id,$range);
	$lineItems = getRankHistory($id,$range);
	
	$chart = (object)array(
		'labels'	=> array(),
		'series'	=> array()
	);
	
	if($items){		
		foreach($items as $r){
			$date = strtotime(date('y-m-d H:i:s',strtotime($r->timestamp)));
			$rank=$r->rank;
			
			 
			foreach($lineItems as $li){
				if($li->date == $r->date)$lineRank = $li->rank;
			}
	
			$chart->labels[] = $date;
			$chart->series[] = array(date('m/d/y H:i:s',$date),(int)$rank, (int)$lineRank);
				
		}
		$chart = $chart;
	}
	json($chart);
	return;
	
	
	/*
	$data = (object)array('items'=>array(),'chart'=>array());
	$data->items = getRankHistory($id,30);
	
	if($data->items){
		$chart = (object)array(
				'labels'	=> array(),				
				'series'	=> array()
		);			
		foreach($data->items as $r){
			$date = strtotime(date('y-m-d',strtotime($r->timestamp)));							
			$rank=$r->rank;

			$chart->labels[] = $date;
			$chart->series[] = array(date('m/d/y',$date),(int)$rank);
			
		}
		$data->chart = $chart;
	}
	*/
	
	json($data);
}
function rankKeyword(){	
	$id = (int)$_REQUEST['id'];
	if(!$id)err("Keyword not found");
	
	$keyword = mysql_fetch_assoc(mysql_query("SELECT * FROM products_keywords WHERE id='$id'"));
	if(!$keyword)err("Keyword not found!");
	
	$sId = createSession(array(
		'provider' 	=> '',
		'pId' 		=> $keyword['product_id'],
		'kId' 		=> $keyword['id'],
		'cId' 		=> 0,
		'period' 	=> 0,
	));
	if(!$sId)err("Unknown error");

	uselib('launcher');
	$l = new Launcher($sId);
	$l->start();
	$l->wait();	
	
	json();
}
function getSummary(){	
	json(array('stats'=>array()));
	exit;
	
	$stats = (object)array(
		'total'		=> (object)array('label'=>'Total','value'=>0),
		'profit'	=> (object)array('label'=>'+ Margin','value'=>0),
		'loss'		=> (object)array('label'=>'- Margin','value'=>0),
		'errors'	=> (object)array('label'=>'Errors','value'=>0),
		'match'		=> (object)array('label'=>'POI','value'=>0),
	);
		
	$q = mysql_query("SELECT * FROM spotter_products");
	while($r = mysql_fetch_assoc($q)){
		$stats->total->value++;
		if($r['margin'] > 0)$stats->profit->value++;
		if($r['margin'] < 0)$stats->loss->value++;
		if($r['error'])$stats->error->value++;
		
		if($r['margin'] > 2 && $r['reviews'] > 2 && (int)$r['available'])$stats->match->value++;
	}	
	
	$total = $stats->total->value;
	foreach($stats as $k=>$v){
		$stats->{$k}->p = (100*$v->value)/$total;
		$stats->{$k}->value = number_format($v->value,0);
		$stats->{$k}->p = number_format($v->p,2);
	}	
	json(array('stats'=>$stats));
}
function addKeywords(){
	$pId = $_REQUEST['pId'];
	$keywords = array_unique(array_filter(explode("\\r\\n",$_REQUEST['keywords'])));
	
	if(!$keywords)err('No valid keywords found!');
	if(!$pId)err("Product not found!");
	
	foreach($keywords as $k){
		list($found) = mysql_fetch_array(mysql_query("SELECT id FROM products_keywords WHERE product_id='$pId' AND term='$k'"));
		if(!$found)mysql_query("INSERT INTO products_keywords (`product_id`,`term`,`user_id`) VALUES ('$pId','$k','{$_SESSION['user']->id}')");
	}
	json();
}
function loadKeyword(){
	$id = (int)$_REQUEST['id'];
	
	$sql = "SELECT k.*,p.title AS product_title, r.rank AS rank, r.timestamp AS rank_timestamp, ro.rank as latest_pos, ro.timestamp AS latest_pos_timestamp FROM products_keywords AS k
				JOIN products AS p ON p.id=k.product_id
				LEFT JOIN products_keywords_ranks AS r ON r.id=(SELECT id FROM products_keywords_ranks WHERE keyword_id=k.id ORDER BY id DESC LIMIT 1)
				LEFT JOIN products_keywords_ranks AS ro ON ro.id=(SELECT id FROM products_keywords_ranks WHERE keyword_id=k.id AND rank!=r.rank ORDER BY id DESC LIMIT 1)								
				WHERE k.id='$id'";
	$q = mysql_query($sql);	
	$item = mysql_fetch_assoc($q);	
	if(!$item)err("Item not found!");
	
	$item = formatKeyword($item);	
	json(array('item'=>$item));
}
function loadKeywords(){
	$sortColumns = array('','k.timestamp','r.timestamp','k.term','p.title','r.rank','','k.badges_found','');
	
	$array = array();
	
	$offset = (int)$_REQUEST['start'];
	$length = (int)$_REQUEST['length'];
	
	if($_REQUEST['order'] && $sortColumns[$_REQUEST['order'][0]['column']])$orderby = array('col'=>$sortColumns[$_REQUEST['order'][0]['column']],'dir'=>$_REQUEST['order'][0]['dir']);
	
	$offset = $offset;
	$length = (int) $length;
	if ($length)
		$limit = "LIMIT $offset,$length";
	else
		$limit = "";
	
	if (!$orderby)
		$orderby = array('k.timestamp DESC');
	else
		$orderby = array($orderby['col'] . " " . $orderby['dir']);
					
	$wheresql = array();
	$wheresql[] = "k.user_id='{$_SESSION['user']->id}'";
	$filter = $_REQUEST['filter'];
	if($filter){
		foreach($filter as $k=>$v){
			if(trim($v)){
				switch($k){
					case 'q':
						$wheresql[] = "k.term like '%$v%'";
						break;
					default:
						$wheresql[] = "k.$k = '$v'";
						break;
				}
			}
		}
	}
   	//$wheresql[] = "r.id = (SELECT MAX(id) FROM products_keywords_ranks WHERE keyword_id=k.id)";
   	
/*	
	$sql = "SELECT SQL_CALC_FOUND_ROWS k.*,p.title AS product_title, r.rank AS rank, r.timestamp AS rank_timestamp, ro.rank as latest_pos, ro.timestamp AS latest_pos_timestamp FROM products_keywords AS k
				JOIN products AS p ON p.id=k.product_id		
				LEFT JOIN products_keywords_ranks AS r ON r.id=(SELECT id FROM products_keywords_ranks WHERE keyword_id=k.id ORDER BY id DESC LIMIT 1)								
				LEFT JOIN products_keywords_ranks AS ro ON ro.id=(SELECT id FROM products_keywords_ranks WHERE keyword_id=k.id AND rank!=r.rank ORDER BY id DESC LIMIT 1)
				WHERE ".implode(" AND ",$wheresql)." ORDER BY  " . implode(' ', $orderby) . " $limit";
*/
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS p.title AS product_title, r.rank AS rank, r.timestamp AS rank_timestamp, k.* FROM products_keywords AS k
				JOIN products AS p ON p.id=k.product_id
				LEFT JOIN products_keywords_ranks AS r ON r.keyword_id=k.id	AND r.id = (SELECT MAX(id) FROM products_keywords_ranks WHERE keyword_id=k.id)			
				WHERE ".implode(" AND ",$wheresql)." ORDER BY  " . implode(' ', $orderby) . " $limit";
	
	//t($sql);
	$q = mysql_query($sql);
	list($total) = mysql_fetch_array(mysql_query("SELECT FOUND_ROWS();"));
	while($r = mysql_fetch_assoc($q)){
	
		//if($r['id'] == 3030)t($r);
	
		$array[] = formatKeyword($r);
	}
	json(array(		
		'data'=> $array,
		'total' => $total,
		'page' => $offset,
		'sort'	=> ($sortby)?$sortby['col']:$_REQUEST['order'][0]['column'],
		'sortDir' => ($sortby)?$sortby['dir']:$_REQUEST['order'][0]['dir'],
		'length' => $length,
	));
}

function saveKeyword(){
	$id = (int)$_REQUEST['id'];
	$term = trim($_REQUEST['term']);
	
	if(!$term)err("Keyword cannot be empty!");
	sql("UPDATE products_keywords SET term='$term' WHERE id='$id'");
}
function removeKeyword(){
	$id = (int)$_REQUEST['id'];			
	sql("DELETE FROM products_keywords WHERE id='$id'");
}
function importTerms(){
	global $policies;
	$csv_mimetypes = array(
			'text/csv',
			'text/plain',
			'application/csv',
			'text/comma-separated-values',
			'application/excel',
			'application/vnd.ms-excel',
			'application/vnd.msexcel',
			'text/anytext',
			'application/octet-stream',
			'application/txt',
	);

	$error = '';
	if ($_FILES['file']['error'] !== UPLOAD_ERR_OK) $error = "Upload failed with error code " . $_FILES['file']['error'];
	else if ($info === FALSE) $error = "Unable to determine image type of uploaded file";
	else if (!in_array($_FILES['file']['type'], $csv_mimetypes)) $error = "Invalid file uploaded";
	else if ($_FILES['file']['error'] != 0) $error = "Unkown Error";

	if($error){
		if(file_exists($_FILES["file"]["tmp_name"]))unlink($_FILES["file"]["tmp_name"]);
		err($error);
	}

	$items = array();	
	if (($handle = fopen($_FILES["file"]["tmp_name"], "r")) !== FALSE) {
		while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {
			$items[] = $row[0];
		}
	}
	fclose($handle);
	unlink($_FILES["file"]["tmp_name"]);
	
	json(array('items'=>$items));
}