<?php
//Keywords
function getKeywordsList($filter=array()){
	$items = array();
	
	$wheresql = array();
	$wheresql[] = "k.user_id='{$_SESSION['user']->id}'";
	foreach($filter as $k=>$v)$wheresql[] = "k.{$k}='$v'";
	
	$sql = "SELECT p.title AS product_title,k.* FROM products_keywords AS k
						JOIN products AS p ON p.id=k.product_id 
						WHERE ".implode(" AND ",$wheresql)." ORDER BY k.term";
	//t($sql);
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
		$r['product_title'] = strshorten($r['product_title'],50);
		$items[] = (object)$r;
	}
	return $items;
}
function getUserKeywords(){
	$array = array();
	
	$wheresql = array();
	$wheresql[] = "k.user_id='{$_SESSION['user']->id}'";
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS k.*,p.title AS product_title, r.rank AS rank, r.timestamp AS rank_timestamp, ro.rank as latest_pos, ro.timestamp AS latest_pos_timestamp FROM products_keywords AS k
				JOIN products AS p ON p.id=k.product_id
				LEFT JOIN products_keywords_ranks AS r ON r.id=(SELECT id FROM products_keywords_ranks WHERE keyword_id=k.id ORDER BY id DESC LIMIT 1)
				LEFT JOIN products_keywords_ranks AS ro ON ro.id=(SELECT id FROM products_keywords_ranks WHERE keyword_id=k.id AND rank!=r.rank ORDER BY id DESC LIMIT 1)
				WHERE ".implode(" AND ",$wheresql)." 
				ORDER BY k.term";
	t($sql);	
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q))
		$array[] = formatKeyword($r);
	
	return $array;
}
function getProductKeywords($pId){
	$array = array();
	
	$wheresql = array();
	$wheresql[] = "k.user_id='{$_SESSION['user']->id}'";
	$wheresql[] = "k.product_id='$pId'";

	$sql = "SELECT SQL_CALC_FOUND_ROWS k.*,p.title AS product_title, r.rank AS rank, r.timestamp AS rank_timestamp, ro.rank as latest_pos, ro.timestamp AS latest_pos_timestamp FROM products_keywords AS k
				JOIN products AS p ON p.id=k.product_id
				LEFT JOIN products_keywords_ranks AS r ON r.id=(SELECT id FROM products_keywords_ranks WHERE keyword_id=k.id ORDER BY id DESC LIMIT 1)
				LEFT JOIN products_keywords_ranks AS ro ON ro.id=(SELECT id FROM products_keywords_ranks WHERE keyword_id=k.id AND rank!=r.rank ORDER BY id DESC LIMIT 1)
				WHERE ".implode(" AND ",$wheresql)."
				ORDER BY k.term";
	//t($sql);
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q))
		$array[] = formatKeyword($r);

	return $array;
}
function formatKeyword($r){	
	//list($r['rank'],$r['timestamp']) = mysql_fetch_array(mysql_query("SELECT rank,timestamp FROM products_keywords_ranks WHERE keyword_id='$r[id]' ORDER BY id DESC LIMIT 1"));
	list($r['rank']) = mysql_fetch_array(mysql_query("SELECT rank,timestamp FROM products_keywords_ranks WHERE keyword_id='$r[id]' ORDER BY id DESC LIMIT 1"));
	list($r['latest_pos'],$r['latest_pos_timestamp']) = mysql_fetch_array(mysql_query("SELECT rank,timestamp FROM products_keywords_ranks WHERE keyword_id='$r[id]' AND rank!='$r[rank]' ORDER BY id DESC LIMIT 1"));
	
	$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
	$r['latest_pos_timestamp'] = date('m/d/Y h:i:s a',strtotime($r['latest_pos_timestamp']));	
			
	$r['history'] = getRankHistory($r['id'],array(),7);
	$r['line'] = array();
	foreach($r['history'] as $h)$r['line'][] = $h->rank;
	$r['line'] = implode(",",$r['line']);
	
	if($r['latest_pos'])$r['rank_delta']=$r['latest_pos']-$r['rank'];
	else $r['rank_delta']='-'; 
	
	$r['badges'] = getKeywordBadgeLatest($r['id']);
	
	$r['badges_found'] = explode(',',str_replace('?',"'",$r['badges_found']));
	
	if($r['history']) $r['latest_rank_timestamp'] = date('m/d/Y h:i:s a',strtotime(end($r['history'])->timestamp));
	else  $r['latest_rank_timestamp'] = "-";
	
	$r['sessions'] = getKeywordSessions($r['id']);
	
	/*
	if(count($r['history'])>2){
		$rank = end($r['history']);
		$rank_old = end(array_slice($r['history'],count($r['history'])-1,1));
		
		t($rank->rank,1);
		t($rank_old->rank);
						
		if($rank_old)$r['rank_delta'] = $rank_old->rank-$rank->rank;
		else $r['rank_delta']='-';
	}
	else $r['rank_delta']='-';
	*/
	
	$r['product_title'] = strshorten($r['product_title'],50);
	return (object)$r;
}
/*
function getKeywordBadgeHistory($kId,$range=false){
	$badges = array();
	$total = 0;
	
	$wheresql = array();
	$wheresql[] = "keyword_id='$kId'";
	if($range)$wheresql[] = "timestamp BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' AND '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";
	else $wheresql[] = "timestamp BETWEEN '".date('Y-m-d',strtotime("-{$days} days"))." 00:00:00' AND '".date('Y-m-d',strtotime('now'))." 23:59:59'";
	
	$q = mysql_query("SELECT * FROM products_keywords_ranks WHERE ".implode(" AND ",$wheresql));
	while($r = mysql_fetch_assoc($q)){
		$total++;
		$badge = trim($r['badge']);
		if(!$badge)continue;
		
		if(!$badges[$badge])$badges[$badge] = (object)array('title'=>$badge,'count'=>0,'p'=>0,'total'=>0);
		$badges[$badge]->count++;
	}
	
	foreach($badges as $k=>$v){
		$badges[$k]->p = ($total)?(100*$badges[$k]->count)/$total:0;
		$badges[$k]->total = $total;
	}
	return $badges;
}*/
function getKeywordBadgeHistory($kId,$range=false){
	$items = array();
	$total = 0;

	$wheresql = array();
	$wheresql[] = "keyword_id='$kId'";
	if($range)$wheresql[] = "timestamp BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' AND '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";
	else $wheresql[] = "timestamp BETWEEN '".date('Y-m-d',strtotime("-{$days} days"))." 00:00:00' AND '".date('Y-m-d',strtotime('now'))." 23:59:59'";

	$sql = "SELECT * FROM products_keywords_ranks WHERE ".implode(" AND ",$wheresql);	
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
		$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
		$r['date'] = date('m/d/Y',strtotime($r['timestamp']));
		$items[] = (object)$r;				
	}		

	return $items;
}

function getKeywordBadgeLatest($kId,$range=false){
	$badges = array();	

	$wheresql = array();
	$wheresql[] = "keyword_id='$kId'";	
	$wheresql[] = "badge <> ''";
	
	$q = mysql_query("SELECT * FROM products_keywords_ranks WHERE ".implode(" AND ",$wheresql)." GROUP BY badge ORDER BY timestamp DESC");
	while($r = mysql_fetch_assoc($q)){		
		$badge = trim($r['badge']);
		if(!$badge)continue;

		$r['current']=0;
		if(date('Y-m-d',strtotime($r['timestamp'])) == date('Y-m-d',strtotime($r['now']))) $r['current']=1;
		$badges[] = (object)array('title'=>$badge,'timestamp'=>date('m/d/Y',strtotime($r['timestamp'])));		
	}		

	return $badges;
}
function getRankHistory($kId,$range=array(),$limit=false){
	$items = array();
	$limit = ($limit)?"LIMIT $limit":"";
	
	$wheresql = array();
	$wheresql[] = "r.keyword_id='$kId'";
	if($range)$wheresql[] = "r.timestamp BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' AND '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";
	
	$sql = "SELECT * FROM products_keywords_ranks AS r
						INNER JOIN (SELECT MAX(timestamp) latest FROM products_keywords_ranks WHERE keyword_id='$kId' GROUP BY date(`timestamp`)) AS t ON r.timestamp = t.latest
						WHERE ".implode(" AND ",$wheresql)."
						ORDER BY r.id ASC						
						$limit";
	//t($sql);
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
		$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
		$r['date'] = date('m/d/Y',strtotime($r['timestamp']));
		$items[] = (object)$r;
	}
	return $items;
}
function getRanks($kId,$range=array(),$limit=false){
	$items = array();
	$limit = ($limit)?"LIMIT $limit":"";
	
	$wheresql = array();
	$wheresql[] = "r.keyword_id='$kId'";
	if($range)$wheresql[] = "r.timestamp BETWEEN '".date('Y-m-d',strtotime($range[0]))." 00:00:00' AND '".date('Y-m-d',strtotime($range[1]))." 23:59:59'";
	
	$sql = "SELECT * FROM products_keywords_ranks AS r				
				WHERE ".implode(" AND ",$wheresql)."
				ORDER BY r.id ASC
				$limit";
	//t($sql);
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
		$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp']));
		$r['date'] = date('m/d/Y',strtotime($r['timestamp']));
		$items[] = (object)$r;
	}
	return $items;	
}

//Sessions
function getProductSessions($id){
	$data = (object)array(
			'pending'	=> 0,
			'working'	=> 0,
			'done'		=> 0,
			'total'		=> 0
	);
	
	$q = mysql_query("SELECT count(id),status FROM products_sessions WHERE product_id='$id' GROUP BY status");
	while(list($count,$status) = mysql_fetch_array($q)){
		$data->total += $count;
		switch($status){
			case 'Done':
				$data->done+=$count;
				break;
			case 'Working':
				$data->working+=$count;
				break;
			case 'Pending':
			case 'Queued':
				$data->pending+=$count;
				break;
		}
	}	
	
	return $data;
}
function getKeywordSessions($id){
	$data = (object)array(
			'pending'	=> 0,
			'working'	=> 0,
			'done'		=> 0,
			'total'		=> 0,
			'next'		=> '',
			'final'		=> '',
	);
	$q = mysql_query("SELECT count(id),status FROM products_sessions WHERE term_id='$id' GROUP BY status");
	while(list($count,$status) = mysql_fetch_array($q)){
		$data->total += $count;
		switch($status){
			case 'Done':
				$data->done+=$count;
				break;
			case 'Working':
				$data->working+=$count;
				break;
			case 'Pending':
			case 'Queued':
				$data->pending+=$count;
				break;
		}
	}
	
	list($next) = mysql_fetch_array(mysql_query("SELECT schedule FROM products_sessions WHERE term_id='$id' AND status='Pending' ORDER BY schedule ASC LIMIT 1"));
	list($final) = mysql_fetch_array(mysql_query("SELECT schedule FROM products_sessions WHERE term_id='$id' AND status='Pending' ORDER BY schedule DESC LIMIT 1"));
	
	$data->next = ($next)?date('m/d/Y h:i:s a',strtotime($next)):'-';
	$data->final = ($final)?date('m/d/Y h:i:s a',strtotime($final)):'-';
	
	return $data;
}
function getKeywordCampaigns($id){
	$items = array();
	
	$q = mysql_query("SELECT c.*, count(s.id) AS sessions_count, 							
							(SELECT schedule FROM products_sessions WHERE term_id='$id' ORDER BY schedule DESC LIMIT 1) AS ts_end
						 FROM products_sessions AS s
						LEFT JOIN products_campaigns AS c ON c.id=s.campaign_id						
						WHERE s.term_id='$id' AND s.status='Pending' 
						GROUP BY campaign_id");
	while($r = mysql_fetch_assoc($q)){
		$r['timestamp']	= date('m/d/Y h:i:sa',strtotime($r['timestamp']));		
		if($r['ts_end'])$r['ts_end']	= date('m/d/Y h:i:sa',strtotime($r['ts_end']));
		
		$items[] = (object)$r;
	}		
	return $items;	
}
//Products
function getUserProducts(){
	$array = array();
	
	$wheresql = array();
	$wheresql[] = "user_id='{$_SESSION['user']->id}'";
	$q = mysql_query("SELECT * FROM products WHERE ".implode(" AND ",$wheresql)." ORDER BY timestamp DESC");
	while($r = mysql_fetch_assoc($q)){
		$r = formatProduct($r);
		$array[] = $r;
	}	
	return $array;
}
function formatProduct($r){			
	$r['title'] = preg_replace( '/[^[:print:]]/', '',$r['title']);		
	$r['title_short'] = strshorten($r['title'],50);
	return $r;
	
	
	$r['keywords'] = getProductKeywords($r['id']);
	
	$r['top10'] = 0;
	foreach($r['keywords'] as $k)if($k->rank>0 && $k->rank<=10)$r['top10']++;
	
	$r['sessions'] = getProductSessions($r['id']);
	$r['badge'] = getProductBadge($r['id']);
	
	$r['progress'] = ($r['sessions']->pending && $r['sessions']->total)?((100*$r['sessions']->pending)/$r['sessions']->total):0;
	$r['percentage'] = 100-$r['progress'];		
		
	return $r;
}
function getProductBadge($pId){	
	list($badge) = mysql_fetch_array(mysql_query("SELECT badge FROM products_sessions WHERE product_id='$pId' AND status='Done' ORDER BY id DESC LIMIT 1")); 
	return $badge;
}
//Sessions
function createSession($data){								//This should replace createCampaign. createCampaign should use this function.
	$provider = $data['provider'];
	$pId = $data['pId'];
	$kId = $data['kId'];
	$cId = $data['cId'];
	$period = $data['period'];

	$product = mysql_fetch_assoc(mysql_query("SELECT * FROM products WHERE id='$pId'"));
	$keyword = mysql_fetch_assoc(mysql_query("SELECT * FROM products_keywords WHERE id='$kId'"));

	if(!$product || !$keyword ) err("Missing search keyword or product!");	
	if(!$product['asin'] || !$product['title'] ) err("Missing product title or ASIN!");
	
	$updateSql = array();

	switch($provider){
		case 'RP':
			$proxy = getProxyRotating();
			break;
		case '"Microleaves"':
			$proxy = getProxyMicroleaves();
			break;
		default:
			$proxy = getProxy();
			break;
	}
	$useragent = getUseragent();

	if($period){
		$seconds = mt_rand(10,$period*60*60);
		$schedule = date('Y-m-d H:i:s',strtotime("+{$seconds} seconds"));
	}
	else{
		$schedule = '';
	}

	$updateSql[] = "`proxy_provider`='".$provider."'";
	$updateSql[] = "`proxy`='".$proxy->proxy."'";
	$updateSql[] = "`useragent`='$useragent'";
	$updateSql[] = "`search_term`='$keyword[term]'";
	$updateSql[] = "`product`='$product[title]'";
	$updateSql[] = "`asin`='$product[asin]'";
	//$updateSql[] = "`visits`='".$_REQUEST['visits']."'";
	$updateSql[] = "`schedule`='".$schedule."'";

	$updateSql[] = "`user_id`='".$_SESSION['user']->id."'";
	$updateSql[] = "`product_id`='".$product['id']."'";
	$updateSql[] = "`campaign_id`='".$cId."'";
	$updateSql[] = "`term_id`='".$keyword['id']."'";

	mysql_query("INSERT INTO products_sessions SET ".implode(",",$updateSql));
	$error = mysql_error();
	
	return($error)?false:mysql_insert_id();		
}

function getSessionSettings($provider=''){
	switch($provider){
		case 'RP':
			$proxy = getProxyRotating();
			break;
		case '"Microleaves"':
			$proxy = getProxyMicroleaves();
			break;
		default:
			$proxy = getProxy();
			break;
	}
	$useragent = getUseragent();
	
	return array($proxy,$useragent);
}

function getProxyRotating(){
	$q = mysql_query("SELECT * FROM proxies_rotating WHERE status=1 ORDER BY timestamp ASC LIMIT 1");
	$proxy = (object)mysql_fetch_assoc($q);

	mysql_query("UPDATE proxies_rotating SET timestamp=NOW() WHERE id='{$proxy->id}'");
	return $proxy;
}
function getProxyMicroleaves(){
	$q = mysql_query("SELECT * FROM proxies_microleaves WHERE status=1 ORDER BY timestamp ASC LIMIT 1");
	$proxy = (object)mysql_fetch_assoc($q);

	mysql_query("UPDATE proxies_microleaves SET timestamp=NOW() WHERE id='{$proxy->id}'");
	return $proxy;
}
function getProxy(){
	$q = mysql_query("SELECT * FROM proxies WHERE status=1 ORDER BY timestamp ASC");
	#$q = mysql_query("SELECT * FROM proxies_mesh WHERE status=1 ORDER BY timestamp ASC");
	$proxy = (object)mysql_fetch_assoc($q);

	mysql_query("UPDATE proxies SET timestamp=NOW() WHERE id='{$proxy->id}'");
	return $proxy;
}
function getUseragent(){
	list($ua) = mysql_fetch_array(mysql_query("SELECT agent FROM useragents WHERE status=1 ORDER BY rand()"));
	return $ua;
}

//Ranker
function createRankerSession($data){
	$provider = $data['provider'];
	$kId = $data['kId'];
	$auto = $data['auto'];
	$userId = ($data['userId'])?$data['userId']:$_SESSION['user']->id;
	$maxPages = 10;

	$keyword = mysql_fetch_assoc(mysql_query("SELECT * FROM ranker_keywords WHERE id='$kId'"));
	if(!$keyword ) err("Missing search keyword or product!");
	if(!$keyword['asin'] || !$keyword['keyword'] ) err("Missing ASIN or keyword!");

	$updateSql = array();

	list($proxy,$useragent) = getSessionSettings($provider);

	$updateSql[] = "`proxy_provider`='".$provider."'";
	$updateSql[] = "`proxy`='".$proxy->proxy."'";
	$updateSql[] = "`useragent`='$useragent'";
	$updateSql[] = "`search_term`='$keyword[keyword]'";
	$updateSql[] = "`product`='$keyword[title]'";
	$updateSql[] = "`asin`='$keyword[asin]'";
	$updateSql[] = "`page_start`=0";
	$updateSql[] = "`page_end`=$maxPages";
	$updateSql[] = "`auto`='$auto'";

	$updateSql[] = "`user_id`='$userId'";
	$updateSql[] = "`keyword_id`='".$keyword['id']."'";

	mysql_query("INSERT INTO ranker_sessions SET ".implode(",",$updateSql));
	$error = mysql_error();

	return($error)?false:mysql_insert_id();
}



//General

function errorlog($type,$source,$msg){
	mysql_query("INSERT INTO log_errors (`user_id`,`type`,`source`,`msg`) VALUES ('{$_SESSION['user']->id}','$type','".mysql_real_escape_string($source)."','".mysql_real_escape_string($msg)."')");	
}
/*
function includeWidget($widget){
	$pagedata = array();
	
	$pagedata['js'] = array();	
	
	$template = $GLOBALS['widgets']['template_path'] . "/$widget/index.phtml";	
	if(!file_exists($template)){
		$template = $GLOBALS['widgets']['template_path'] . "/$widget.phtml";
	}	
	$pagedata['content'] = $template;
						
	$js = $GLOBALS['widgets']['js_path'] . "/$widget.js";		
	if(file_exists($js)){ $pagedata['js'][] = $GLOBALS['widgets']['js_href'] . "/$widget.js?".filemtime($js); }			
	foreach($pagedata['js'] as $js){
		print '<script src="'.$js.'"></script>';				
	}
	
	include $pagedata['content'];
}
*/
function includeWidget($widget){
	global $pagedata;
	$widgetdata = array();

	$widgetdata['js'] = array();

	$template = $GLOBALS['widgets']['template_path'] . "/$widget/index.phtml";
	if(!file_exists($template)){
		$template = $GLOBALS['widgets']['template_path'] . "/$widget.phtml";
	}
	$widgetdata['content'] = $template;
	
	$js = $GLOBALS['widgets']['js_path'] . "/$widget.js";	
	if(file_exists($js)){ $widgetdata['js'][] = $GLOBALS['widgets']['js_href'] . "/$widget.js".'?'.filemtime($js); }
	foreach($widgetdata['js'] as $js){		
		foreach($widgetdata['js'] as $k=>$file){ $widgetdata['js'][$k] = $file; }
		
		//print '<script>$(window).load(function(){ $.getScript("'.$js.'"); })</script>';
		//print '<script src="'.$js.'"></script>';
	}
	$script = $GLOBALS['widgets']['script_path'] . "/$widget.php";
	
	if(file_exists($script)){ $widgetdata['script'] = $script; }
	
	if( $widgetdata['script'])
		include_once $widgetdata['script'];
	include_once $widgetdata['content'];

	$pagedata['js_default'] = array_merge($widgetdata['js'],$pagedata['js_default']);
}
function mapTemplate($template){
	$template = explode("::",$template);
	$file = array_pop($template).'.phtml';
	return $GLOBALS['system']['template_path'].'/'.implode("/",$template).'/'.$file;
}
function includeTemplate($template){
	include mapTemplate($template);
}
function uselib($lib){
	$lib = explode("::",$lib);
	$file = array_pop($lib).'.lib.php';	
	include_once($GLOBALS['system']['lib_path'].'/'.implode("/",$lib).'/'.$file);
}
function usehelper($helper){
	$helper = explode("::",$helper);
	$file = array_pop($helper).'.php';	
	include_once($GLOBALS['system']['helper_path'].'/'.implode("/",$helper).'/'.$file);
}
function t($var,$live=false){
	print '<pre>';
	var_dump($var);
	print '</pre>'."\n";
	if(!$live)
		exit;
}
function sql($sql,$msg='ok',$silent=false){
	if(gettype($msg) == 'boolean'){ $silent = $msg; $msg='ok'; }
	
	mysql_query($sql);
	$error = mysql_error();
	if($error){
		err($error);
	}
	else{	
		if(!$silent)	
			print json_encode(array('success'=>$msg));
	}
}
function json($data=''){
	if(empty($data)){
		$data = array('success'=>'ok');
	}
	$error = mysql_error();
	if(empty($error)){ print json_encode($data); }
	else{ err($error); }	
}
function err($msg,$ajax=1){	
	if($ajax){
		print json_encode(array('error'=>$msg));
	}
	else{
		displayError($msg);
	}
	exit;
}
function errs($errors,$ajax=1){	
	if(is_array($errors))$errors = array_filter($errors);	
	if(!$errors)return;
	
	if($ajax){				
		print json_encode(array('errors'=>$errors));
	}
	else{
		if(is_array($errors)){
			displayError(implode('<br>',$errors));
		}
		else{
			err($errors,0);
		}
	}
	exit;
}
function displayError($msg){
	print '<div class="alert alert-danger">'.$msg.'</div>';
}
function r($id,$msg,$error=false){
	if($error){ $type = 'error'; }
	else{ $type = 'message'; }		
}
function objectToArray($d){
	if (is_object($d)){ $d = get_object_vars($d); } 
	if (is_array($d)){ return array_map(__FUNCTION__, $d); }
	else { return $d; }
}
function arrayToObject($d){
	if (is_array($d)){ return (object) array_map(__FUNCTION__, $d); }
	else{ return $d; }
}
function email($emails,$subject,$message,$attachments=array(),$from='',$from_email=''){
	if($emails && !is_array($emails)) $emails = array($emails);
	if($attachments && !is_array($attachments)) $attachments = array($attachments);
	if(!$emails) return false;
	if(!$from){ $from = $GLOBALS['site']['title']; }
	if(!$from_email){ $from_email = $GLOBALS['emails']['from_email']; }
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
	$headers .= 'From: '.$from.' <'.$from_email.'>' . "\r\n";	
	
	$message = str_replace("\\r","",$message);
	$message = str_replace("\\n","",$message);
	$message = stripslashes($message);	
		
	//return mail($emails,$subject,$message, $headers);	
	//return mail('gontham@hotmail.com',$subject,$message, $headers);
	
	return sendgrid(array(
		'emails'=>$emails,
		'subject'=>$subject,
		'body'=>$message,
		'attachments'=>$attachments,
		'from'=>$from_email,
		'replyTo'=>$from_email,
		'fromName'=>$from 
	));		
	/*
	if($GLOBALS['emails']['smtp_host'])
		return smtp($emails,$subject,$message,$from,$from_email);		
	else
		return mail(implode(",",$emails),$subject,$message, $headers);	
	*/
}
function sendgrid($data){
	require_once("php/lib/sendgrid-php/sendgrid-php.php");
	
	$sendgrid = new SendGrid($GLOBALS['SETTINGS']['sendgrid_key']);
	$email    = new SendGrid\Email();

	if(!$from_name)$from_name=$from;
	
	$emails = $data['emails'];
	if(!$emails)return false;
	if(!is_array($emails))$emails = array($emails);
	
	$email->setTos($emails);
	//$email->setTos(array('gontham@hotmail.com'));	
	if($data['from'])$email->setFrom($data['from']);
	if($data['fromName'])$email->setFromName($data['fromName']);
	if($data['replyTo'])$email->setReplyTo($data['replyTo']);
	if($data['subject'])$email->setSubject($data['subject']);	
	foreach($data['attachments'] as $a)$email->addAttachment($a);
		
	if($data['body'])$email->setHtml($data['body']);
	
	$sendgrid->send($email);
	return true;
}
function smtp($emails=array(),$subject='',$message='',$from='',$from_email=''){	
	require 'lib/PHPMailer/PHPMailerAutoload.php';			
	$mail = new PHPMailer;
	
	if(!is_array($emails))$emails=array($emails);
		
	$mail->isSMTP();                                     		 // Set mailer to use SMTP
	$mail->Host = $GLOBALS['emails']['smtp_host'];			 // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                              		 // Enable SMTP authentication
	$mail->Username = $GLOBALS['emails']['smtp_username'];      // SMTP username
	$mail->Password = $GLOBALS['emails']['smtp_password'];      // SMTP password
	$mail->SMTPSecure = 'tls';                            		 // Enable encryption, 'ssl' also accepted	
	//$mail->SMTPDebug  = 2;
	
	$mail->From = ($from_email)?$from_email:$GLOBALS['emails']['from_email'];
	$mail->FromName = ($from)?$from:$GLOBALS['emails']['from'];
	foreach($emails as $to)
		$mail->addAddress($to);
		
	$mail->isHTML(true);                                  // Set email format to HTML
	
	$mail->Subject = $subject;
	$mail->Body    = $message;
	$res = $mail->send();			
	if(!$res) {		
		return false;
	} else {
		return true;
	}
}
function loadTemplateFile($file=''){
	$file = $GLOBALS['system']['template_path'].$file;	
	if(file_exists($file)){
		ob_start();
		include $file;
		$output = ob_get_clean();
	}
	return $output;
}
function strshorten($str,$len,$suffix='...'){	
	if(strlen($str)>$len){
		return substr($str,0,$len).$suffix;
	}
	else{
		return $str;
	}
}
function formatDate($timestamp,$format=''){	
	if(!$timestamp || strpos("0000-00-00",$timestamp)!==false)return '';
	if(!$format)$format = $GLOBALS['SETTINGS']['date_format_compact'];	
	return date($format,strtotime($timestamp));
}
function dbtimestamp($timestamp){
	if(!$timestamp || strpos("0000-00-00",$timestamp)!==false) return false;
	return date('Y-m-d h:m:i',strtotime($timestamp));
}
function dbdate($timestamp){
	if(!$timestamp || strpos("0000-00-00",$timestamp)!==false) return false;
	return date('Y-m-d',strtotime($timestamp));
}
function getUser($userID){
	$query = mysql_query("SELECT * FROM `users` WHERE `id` ='".$userID."'");
	return mysql_fetch_assoc($query);
}
function xTimeAgo ($oldTime, $newTime, $suffix='ago') {
	$timeCalc = strtotime($newTime) - strtotime($oldTime);
	if ($timeCalc > (60*60*24)) {$timeCalc = round($timeCalc/60/60/24) . " days $suffix"; }
	else if ($timeCalc > (60*60)) {$timeCalc = round($timeCalc/60/60) . " hours $suffix"; }
	else if ($timeCalc > 60) {$timeCalc = round($timeCalc/60) . " minutes $suffix"; }
	else if ($timeCalc > 0) {$timeCalc .= " seconds $suffix"; }
	return $timeCalc;
}
function xHoursAgo ($oldTime, $newTime, $suffix='ago') {
	$timeCalc = strtotime($newTime) - strtotime($oldTime);	
	$timeCalc = number_format($timeCalc/60/60,1) . " $suffix";	
	return $timeCalc;
}
function pathUrl($path){	
	return str_replace($GLOBALS['system']['upload_path'],$GLOBALS['system']['upload_href'],$path);
}
function curl_post($url,$data,$headers=array()){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	#curl_setopt($curl, CURLOPT_POST, count($data));
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

	$response = curl_exec($curl);

	#$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
	#$header = substr($response, 0, $header_size);
	#$body = substr($response, $header_size);


	curl_close($curl);
	return $response;
}
function curl_get($url,$headers=array(),$proxy='',$useragent=''){
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_URL, $url);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
	if($headers)curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
	if($proxy)curl_setopt($curl, CURLOPT_PROXY, $proxy);	
	if($useragent)curl_setopt($curl, CURLOPT_USERAGENT,$useragent);

	$response = curl_exec($curl);

	//$header_size = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
	//$header = substr($response, 0, $header_size);
	//$body = substr($response, $header_size);

	curl_close($curl);
	return $response;
}