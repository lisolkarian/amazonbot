<?php	
usehelper("ajax::dispatch");

//$sessions = loadSessions();

function clearData(){
	sql("TRUNCATE TABLE `sessions`");
}
function getSummary(){
	$stats = array();
	
	$total = $progress = $error = 0;
	$q = mysql_query("SELECT count(id) AS num, status FROM sessions GROUP BY status");
	while($r = mysql_fetch_assoc($q)){
		$stats[] = array('status'=>$r['status'],'num'=>$r['num']);
		$total += $r['num'];
		if($r['status'] == 'Done' || $r['status'] == 'Error') $progress += $r['num'];
		if($r['status'] == 'Error') $error = $r['num'];		
	}
	
	$ep = ($total)?number_format(($error*100)/$total,2):0;
	$p = ($total)?number_format(($progress*100)/$total,2):0;
	
	foreach($stats as $k=>$v)$stats[$k]['num'] = number_format($v['num'],0);
	
	$stats[] = array('status'=>'Progress', 'num'=> $p.'%');
	$stats[] = array('status'=>'Failure Rate', 'num'=> $ep.'%');
	
	json(array('stats'=>$stats));
}
function loadSessions(){
	$sortColumns = array('timestamp','schedule','search_term','product','','status','');
	$array = array();
	
	$offset = (int)$_REQUEST['start'];
	$length = (int)$_REQUEST['length'];
	
	if($_REQUEST['order'])$orderby = array('col'=>$sortColumns[$_REQUEST['order'][0]['column']],'dir'=>$_REQUEST['order'][0]['dir']);
	
	$offset = $offset;
	$length = (int) $length;
	if ($length)
		$limit = "LIMIT $offset,$length";
	else
		$limit = "";
	
	if (!$orderby)
		$orderby = array('timestamp DESC');
	else
		$orderby = array($orderby['col'] . " " . $orderby['dir']);
	
	
	$wheresql = array("1=1");
	$filter = $_REQUEST['filter'];
	if($filter){
		foreach($filter as $k=>$v){
			if(trim($v)){
				switch($k){
					case 'product':
						$wheresql[] = "product like '%$v%'";
						break;
					case 'search_term':
						$wheresql[] = "search_term like '%$v%'";
						break;
					case 'status':
						$wheresql[] = "status = '$v'";
						break;
					default:
						break;
				}
			}
		}
	}
	
	
	$sql = "SELECT SQL_CALC_FOUND_ROWS * FROM sessions WHERE ".implode(" AND ",$wheresql)." ORDER BY  " . implode(' ', $orderby) . " $limit";
	//t($sql);
	$q = mysql_query($sql);
	list($total) = mysql_fetch_array(mysql_query("SELECT FOUND_ROWS();"));
	while($r = mysql_fetch_assoc($q)){
		$r['timestamp'] = date('m/d/Y h:i:s a',strtotime($r['timestamp'])); 
		$r['schedule'] = date('m/d/Y h:i:s a',strtotime($r['schedule']));
		
		if($r['status'] == 'Pending'){
			$r['duration'] = '-';
		}
		else{
			$end = ($r['end'])?$r['end']:'now';
			$r['duration'] = xTimeAgo($r['start'],$end,'');
		}
		$array[] = (object)$r;
	}
	json(array(
		'data'=> $array,
		'total' => $total,
		'page' => $offset,
		'sort'	=> ($sortby)?$sortby['col']:$_REQUEST['order'][0]['column'],
		'sortDir' => ($sortby)?$sortby['dir']:$_REQUEST['order'][0]['dir'],
		'length' => $length,
	));
}
function createSessions(){
	$num = (int)$_REQUEST['sessions'];
	$product = $_REQUEST['product'];
	$q = $_REQUEST['q'];
	
	if(!$product || !$q) err("Missing search term or product title!");
	
	foreach(range(0,$num) as $i){
		$updateSql = array();
		
		$proxy = getProxy();
		$useragent = getUseragent();
		
		$seconds = mt_rand(10,172800);
		$schedule = date('Y-m-d H:i:s',strtotime("+{$seconds} seconds"));
		
		$updateSql[] = "`proxy`='".$proxy->proxy."'";
		$updateSql[] = "`useragent`='$useragent'";
		$updateSql[] = "`search_term`='$q'";
		$updateSql[] = "`product`='$product'";
		$updateSql[] = "`visits`='".$_REQUEST['visits']."'";
		$updateSql[] = "`schedule`='".$schedule."'";		
		
		mysql_query("INSERT INTO sessions SET ".implode(",",$updateSql));
	}
	json();
}
function getProxy(){
	$q = mysql_query("SELECT * FROM proxies WHERE status=1 ORDER BY timestamp ASC");
	$proxy = (object)mysql_fetch_assoc($q);

	mysql_query("UPDATE proxies SET timestamp=NOW() WHERE id='{$proxy->id}'");
	return $proxy;
}
function getUseragent(){
	list($ua) = mysql_fetch_array(mysql_query("SELECT agent FROM useragents WHERE status=1 ORDER BY rand()"));
	return $ua;	
}
function removeSession(){
	$id = (int)$_REQUEST['id'];

	sql("DELETE FROM sessions WHERE id='$id'");
}