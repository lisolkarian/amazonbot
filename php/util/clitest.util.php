<?php
include_once('/var/www/html/php/helpers/cli.php');
	
session_write_close();
uselib('launcher');

$debug = true;
$threads = 5;
$timeout = 60*60*1;
$max_retries = 3;
$launchers = array();

retry();
do{	
	$found = false;
	#$sql = "SELECT * FROM products_sessions WHERE status='Pending' AND schedule<=NOW() ORDER BY schedule ASC LIMIT 1";
	$sql = "SELECT * FROM products_sessions WHERE status='Pending' ORDER BY schedule ASC LIMIT 1";
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){	
		$found = true;				
		mysql_query("UPDATE products_sessions SET `status`='Queued' WHERE id='$r[id]'");
			
		if($debug)print "Launching ".$r['id']."\n";

		$launcher = new Launcher($r['id']);
		$launcher->start();		
		$launchers[] = (object)array(
			'id'		=> $r['id'],
			'launcher'	=> $launcher,
			'start'		=> microtime()
		);
	}
	if($debug)print "Waiting...";
	
	do{
		$count = 0;
		foreach($launchers as $i=>$l){
			$launcher = $l->launcher;
			if($launcher->isActive()){
				$time = microtime() - $l->start;
				if($time>$timeout)$launcher->stop();						
			}
			
			if($launcher->isActive())$count++;
			else{								
				fixStuckStatus($l->id);
				unset($launchers[$i]);
			}
		}
		sleep(1);
		if($debug)t("Active: ".$count,1);
	}while($count>=$threads);
	
	if(!$found){
		if($debug)t("Final wait",1);
		foreach($launchers as $l)$l->launcher->wait();						
		foreach($launchers as $l)fixStuckStatus($l->id);
	}
					
	if($debug)print "Resuming...";
}while($found);		
t('done');

function fixStuckStatus($id){
	list($status) = mysql_fetch_array(mysql_query("SELECT status FROM products_sessions WHERE id='$id'"));
	t($id." not active ($status)",1);
	if($status == 'Working')mysql_query("UPDATE products_sessions SET `status`='Error', `error`='Unexpected error' WHERE id='$id'");
}
function retry(){
	global $max_retries;
	//$q = mysql_query("SELECT id FROM products_sessions WHERE `status`='error' AND retries < '$max_retries'");	
	mysql_query("UPDATE products_sessions SET retries=retries+1, `status`='Pending' WHERE `status`='error' AND retries < '$max_retries'");
}