<?php 
include_once('/var/www/html/php/helpers/cli.php');

uselib('ranker');

$q = mysql_query("SELECT id,user_id FROM ranker_keywords WHERE schedule_timestamp < NOW() - INTERVAL schedule DAY");
while(list($id,$uId) = mysql_fetch_array($q)){
	$lastAutoSession = mysql_fetch_assoc(mysql_query("SELECT * FROM ranker_sessions WHERE keyword_id='$id' ORDER BY id DESC LIMIT 1"));
	if(Ranker::checkPid($lastAutoSession['pid'])){ t("$id: Already running...",1); continue; }
	
	if($lastAutoSession['status'] == 'Pending')$sId = $lastAutoSession['id']; 
	else{
		$sId = createRankerSession(array(
			'provider' 	=> '',		
			'auto'		=> 1,
			'kId' 		=> $id,
			'userId'	=> $uId		
		));
	}
	if(!$sId)continue;
	
	$l = new Ranker($sId);
	$l->start();	
}

