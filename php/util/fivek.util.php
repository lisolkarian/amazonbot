<?php
include_once('/var/www/html/php/helpers/cli.php');
session_write_close();
uselib('fivek');

$debug = true;
$threads = 200;
$limit = 100;
$sessions = array();

$fivek = new Fivek();

do{
	$found = false;
	$sql = "SELECT id FROM fivek_searches_sessions WHERE pending='1' LIMIT $limit";	
	$q = mysql_query($sql);
	while(list($sId) = mysql_fetch_array($q)){
		$found = true;
		mysql_query("UPDATE fivek_searches_sessions SET pending=0 WHERE id='$sId'");
			
		if($debug)print "Launching $sId\n";
		
		$pid = Fivek::start($sId);
		if($pid)continue;

		$sessions[] = (object)array(
				'id'		=> $sId,
				'pid'		=> $pid				
		);
	}
	if($debug)print "Waiting...";
	
	do{
		$count = 0;
		foreach($sessions as $i=>$s){			
			if(Fivek::isActive($s->pid))$count++;
			else{				
				unset($sessions[$i]);
			}
		}
		//sleep(1);
		if($debug) print "Active: ".$count."\n";
	}while($count>=$threads);
		
	if($debug)print "Resuming...";
}while($found);

t('done');