<?php
include_once('/var/www/html/php/helpers/cli.php');
session_write_close();
uselib('launcher');

$debug = true;
$threads = 200;
$timeout = 60*20*1;
$max_retries = 1;
$launchers = array();

retry();
do{
	$found = false;
	$sql = "SELECT * FROM products_sessions WHERE status='Pending' AND schedule<=NOW() ORDER BY schedule ASC LIMIT 100";
	#$sql = "SELECT * FROM products_sessions WHERE status='Pending' ORDER BY schedule ASC LIMIT 1";
	$q = mysql_query($sql);
	while($r = mysql_fetch_assoc($q)){
		$found = true;
		mysql_query("UPDATE products_sessions SET `status`='Queued' WHERE id='$r[id]'");
			
		if($debug)print "Launching ".$r['id']."\n";

		$launcher = new Launcher($r['id']);
		$launcher->start();
		$launchers[] = (object)array(
				'id'		=> $r['id'],
				'launcher'	=> $launcher,
				'start'		=> microtime(true)
		);
	}
	if($debug)print "Waiting...";

	do{
		$count = 0;
		foreach($launchers as $i=>$l){
			$launcher = $l->launcher;
			if($launcher->isActive()){
				$time = abs(microtime(true) - $l->start);
				if($time>$timeout)$launcher->stop();
			}
				
			if($launcher->isActive())$count++;
			else{
				fixStuckStatus($l->id);
				unset($launchers[$i]);
			}
		}
		//sleep(1);
		if($debug) print "Active: ".$count."\n";
	}while($count>=$threads);

	if(!$found){
		if($debug)print "Final wait\n";
		foreach($launchers as $l)$l->launcher->wait();
		foreach($launchers as $l)fixStuckStatus($l->id);
	}
		
	if($debug)print "Resuming...";
}while($found);
t('done');

function fixStuckStatus($id){
	list($status) = mysql_fetch_array(mysql_query("SELECT status FROM products_sessions WHERE id='$id'"));
	if($debug)print $id." not active ($status)\n";
	if($status == 'Working')mysql_query("UPDATE products_sessions SET `status`='Error', `error`='Unexpected error' WHERE id='$id'");
}
function retry(){
	global $max_retries;
	//$q = mysql_query("SELECT id FROM products_sessions WHERE `status`='error' AND retries < '$max_retries'");
	mysql_query("UPDATE products_sessions SET retries=retries+1, `status`='Pending' WHERE `status`='Error' AND retries < '$max_retries'");
}