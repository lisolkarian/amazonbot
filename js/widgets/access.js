$(document).ready(function() {	
	var script = 'ajax/php/widgets/access.php';
		
	var selectors = {				
		container			: '.widget-access',
				
		select				: '.product-select select',
		summary				: '.access-summary-container',
				
		templates: {			
			summary			: '#template-access-summary',
		}
	}		
	var container = $(selectors.container);
	var modal;
	var refreshRate = 5*1000;
	var pId = 0;
			
	if(window.accessWidget)return;
	window.accessWidget = true;
				
	bind();
	init();
		

	function init(){
    	var hash = parseInt(window.location.hash.replace('#',''));
    	if(!isNaN(hash) && hash>0)
    		setTimeout(function(){ selectProduct(hash); },300);
    	else{
    		selectProduct();
    	}
    	    	  
		updateSummary(false);
	}	
	function bind(){			
		container.on('change',selectors.select,function(e){ selectProduct($(this)); });
	}
	
	function selectProduct(id){
		if(!$.defined(id))id = 0;		
		var obj = container.find(selectors.select);
		
		if(id > 0){
			obj.val(id);
		}
		
		pId = obj.val();
		$(window).trigger("productSelected",pId);
		window.location.hash = pId;		
		updateSummary();
		$('[data-filter-product-id]').attr('data-filter-product-id',pId);
	    //window.location = container.find(selectors.product).find('option:selected').attr('data-href');	    
	}
	function updateSummary(single){
		if(!$.defined(single))single=true;
						
    	var c = container.find(selectors.summary);

    	$.post(script,{action: 'getAccessSummary', pId: pId},function(json){
    		if(json.error){ c.html("Error: "+json.error); }
    		else{    			
    			var template = Handlebars.compile(container.find(selectors.templates.summary).html())
    			c.html(template(json));
    		}
    		if(!single)setTimeout(function(){ updateSummary() },refreshRate);
    	},"json")
    }
});