$(document).ready(function() {	
	var script = 'ajax/php/widgets/sessions.php';
		
	var selectors = {				
		container			: '.widget-sessions',
		create				: '.sessions-create-a',
		
		campaignCreate		: '.campaign-create-a',
		campaignPending		: '.sessions-keyword-campaigns-a',
		campaignCancel		: '.session-campaign-cancel-a',
		
		product				: '.sessions-product-selector',
		
		upload				: '.fileupload',
		uploading			: '.uploading',
		keywordsSelect		: '.create-sessions-keywords-select',
				
		templates: {			
			create			: 'ajax/template/widgets/sessions/_create.phtml',
			pending			: 'ajax/template/widgets/sessions/_pending.phtml',
			createResult	: '#template-create-result',
		}
	}		
	var maincontainer = $(selectors.container);
	var modal;
	
	if(window.sessionsWidget)return;
	window.sessionsWidget = true;
	
	bind();
	init();	

	function init(){		
	}	
	function bind(){	
		$(window).on('keywordsCreateSessions',function(e, data){ keywordsCreateSessions(data); });
		
		$('body').on('click',selectors.create,function(e){e.preventDefault(); createModal($(this));});
		$('body').on('click',selectors.campaignPending,function(e){e.preventDefault(); keywordPendingCampaigns($(this));});
		$('body').on('click',selectors.campaignCancel,function(e){e.preventDefault(); keywordCancelPendingCampaigns($(this));});		
		
		maincontainer.on('click',selectors.campaignCreate,function(e){e.preventDefault(); createCampaign($(this));});		
		maincontainer.on('change',selectors.product,function(e){e.preventDefault(); filterKeywords();});		
		
		maincontainer.on('change.bs.fileinput',selectors.upload, function(){ processKeywordsList(); });						
	}	
	function keywordsCreateSessions(data){
		var ids = data.ids;
		var btn = maincontainer.find(selectors.create);
		createModal(btn,ids);
	}
	function filterKeywords(){		
		var pId = modal.modal.find(selectors.product).val();
		var el = modal.modal.find(selectors.keywordsSelect);
		var options = el.find('option');
		var filtered =  options.filter(function(){ return $(this).attr('data-pId')==pId || pId==0; });
		
		options.hide();
		filtered.show();
		el.bootstrapDualListbox('refresh');
	}
	function processKeywordsList(){		
		var form = modal.modal.find(selectors.upload);
		var data = new FormData();
		data.append('file', form.find('input[type="file"]')[0].files[0]);
		data.append('action', 'selectKeywordsFromUpload');
		form.find('.btn').hide();
		form.find(selectors.uploading).show();
		
		$.ajax({
			url: script,
			data: data,
			processData: false,
			contentType: false,
			dataType: "json",
			type: 'POST',
			success: function(json) {
				form.find('.btn').show();
				form.find(selectors.uploading).hide();
				
				modal.modal.find(selectors.keywordsSelect).val(json.items);
				modal.modal.find(selectors.keywordsSelect).trigger("chosen:updated");

			}
		});
	}
	function keywordPendingCampaigns(btn){
		var kId = btn.attr('data-kId');
		
		modal = new Modal({
			parent: maincontainer,
			static: true,
			size: '60%',
			title: 'Pending Campaigns',
			template: selectors.templates.pending,	
			templateData: {kId: kId},
			buttons: new Array(
				$('<a>').addClass('btn btn-default dialog-close').text('Done')
			)
		});			
	}
	function keywordCancelPendingCampaigns(btn){
		if(!confirm("Are you sure you want to cancel this campaign?")) return;
		var cId = btn.attr('data-cId');
		
		btn.button('loading');				
		modal.block();
		$.post(script,{action:'cancelCampaign', cId: cId},function(json){
			btn.button('reset');
			modal.release();
			if(json.error){ $.error(json.error); }
			else{
				btn.closest('tr').fadeOut();
				$(window).trigger('sessionsUpdated');				
			}
		},"json");
	}
	function createModal(btn,ids){
		if(!$.defined(ids))ids = new Array();
		var pId = btn.attr('data-filter-product-id');
		
		modal = new Modal({
			parent: maincontainer,
			static: true,
			size: '70%',
			title: 'Create Sessions',
			template: selectors.templates.create,	
			templateData: {pId: pId, ids: ids},
			callback: function(){ filterKeywords(); },
			buttons: new Array()
		});	
	}
	function createCampaign(btn){		
		var form = btn.closest('form');			
		btn.button('loading');
		
		var data = form.serializeObject();		
		
		modal.block();
		$.post(script,data,function(json){			
			btn.button('reset');
			modal.release();
			if(json.error){ $.error(json.error); }
			else{ 
				//modal.close();
				
				var html = Handlebars.compile(maincontainer.find(selectors.templates.createResult).html());
				modal.update(html(json));
				$(window).trigger('sessionsUpdated');
				//$.notify("Sessions created");
			}
		},"json");
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	function saveTemplate(btn){
		var form = modal.modal.find('form'); 				
		var id = parseInt(btn.attr('data-id'));
		if(isNaN(id) || id<=0){ $.error("Unable to find email!"); return; }
		
		form.loading();
		btn.button('loading');
		modal.block();
		$.post(script,{action:'publish',id:id},function(json){
			btn.button('reset');
			form.loading(false);
			modal.release();
			
			if(json.error){								
				error(json.error);
			}
			else{
				$(window).trigger('updateUserEmail',json.id);
				notify('Message sent');
				modal.close();				
				//window.location.reload();
			}
		},"json");
		
	}
	function sendEmail(btn){
		var form = modal.modal.find('form');				
		if (!form.parsley().validate()) return;
		
		form.loading();
		btn.button('loading');
		modal.block();
		$.post(script,form.serialize(),function(json){
			btn.button('reset');
			form.loading(false);
			modal.release();
			
			if(json.error){						
				error(json.error);
			}
			else{
				$(window).trigger('updateUserEmail',json.id);
				notify('Message sent');
				modal.close();
			}
		},"json");
		
	}
	function select(btn){ 				
		var eId = tId = cId = 0;						
		
		eId = parseInt(btn.attr('data-email-id'));
		tId = parseInt(btn.attr('data-template-id'));
		cId = parseInt(btn.attr('data-content-id'));
		
		if(isNaN(eId)){
			
			modal = new Modal({			
				static: true,
				title: 'Create New Emai From Content/Template',
				content: 'You are not currently editing any emails. Create a new email from the selected content/template?',			
				buttons: new Array(
					$('<button>').addClass('btn btn-default dialog-close').text('Cancel'),
					$('<button>').addClass('btn btn-success').text('Create').click(function(){
						$.post(script,{action: 'createEmail', cId: cId, tId: tId},function(json){			
							if(json.error){ $.error(json.error); }
							else{
								window.location = redirect+'?id='+json.success;
							}							
						},"json");						
					})				
				)
			});									
		}
		else{
			if(isNaN(cId))cId=0;
			if(isNaN(tId))tId=0;
			window.location = redirect+'?id='+eId+'#contentId/'+cId+'/templateId/'+tId;
		}
	}
	function quickCreate(btn){ 
		var title = "New Email";
		$.post(script,{action: 'createEmail', title: title},function(json){			
			if(json.error){ $.error(json.error); }
			else{
				window.location = redirect+'?id='+json.success;
			}			
		},"json");
	}
	function createDialog(){
		quickCreate();
		return;
		
		
		modal = new Modal({			
			static: true,
			title: 'New Email',
			template: selectors.templates.create,			
			buttons: new Array(
				$('<button>').addClass('btn btn-default dialog-close').text('Cancel'),
				$('<button>').addClass('btn btn-success').text('Create').click(function(){ create($(this)); })				
			)
		});
	}
	function create(btn){
		var form = modal.modal.find('form');
		if (!form.parsley().validate()) return;
		
		btn.button('loading');
		$.post(script,form.serialize(),function(json){
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{
				window.location = redirect+'?id='+json.success;
			}
			
		},"json");
	}
});