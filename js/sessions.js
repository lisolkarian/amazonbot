$(document).ready(function () {
    var script = "ajax/php/sessions.php";
    var selectors = {
        container			: ".page-sessions",
				
        item				: '.keyword-item',
        create				: '.create-a',                
        sessions			: '.sessions-container',
        searchForm			: '.search-form',
        search				: '.search-a',
        clear				: '.clear-a',
        productInput		: '[name="filter[pId]"]',
        keywordsSelect		: '[name="filter[search_term]"]',
        
        remove				: '.remove-a',
        snapshot			: '.snapshot-a',
        
        chart				: '.sessions-line-chart',
		chartLegend			: '.chart-legend',		
        
        summary				: '.summary-container',
		
        templates: {                    	        	
        	summary			: '#template-summary',
        	session			: '#template-session',
        	snapshot		: 'ajax/template/sessions/_snapshot.phtml',        	
        },
	}    
    var container = $(selectors.container);
    var form = container.find(selectors.searchForm);
    var modal;
    var pId;
    
    init();
    bind();

    function init() {    	    	    	
    	//loadKeywords();    	
    	google.charts.load('current', {packages: ['corechart', 'line']});
    	//search();
    }

    function bind() {      	
    	$(window).on('ajaxDataTableReady', function(e){ initSessions($(this)); });
    	$(window).on('sessionsUpdated',function(){ search(); })
    	$(window).on('productSelected',function(e, id){ updateProductInput(id); })    	
    	
    	container.on('click',selectors.search, function(e){ e.preventDefault(); search($(this)); });		
		container.on('click',selectors.remove, function(e){ e.preventDefault(); remove($(this)); });
		container.on('click',selectors.snapshot,function(e){ e.preventDefault(); snapshot($(this)); });
		form.on('change','input,select',function(e){ search(); });
    }   
    function updateProductInput(id){    	
    	container.find(selectors.productInput).val(id);
    	loadKeywords();
    	search();
    }
    function loadKeywords(){
    	var pId = container.find(selectors.productInput).val();
    	$.post(script,{action: 'loadKeywords', pId: pId},function(json){
    		if(json.error){ $.error(json.error); }
    		else{
    			var select = container.find(selectors.keywordsSelect);
    			
    			select.html('<option value="">Select...</option>');
    			
    			$.each(json.keywords,function(k,el){
    				select.append($('<option>').val(el.id).text(el.term));
    			});
    		}
    	},"json");
    }
    function reloadItem(id){
    	var item = container.find(selectors.item).filter(function(){ return $(this).attr('data-id')==id; })
    	if(item.length<=0){ $.error("Item not found!"); return; }
    	
    	$.post(script,{action: 'loadSessions', id: id},function(json){
    		if(json.error){ $.error(json.error); }
    		else{
    			var html = Handlebars.compile(container.find(selectors.templates.session).html());
    			item.replaceWith(html(json.item));
    			
    			initSessions();
    		}    		
    	},"json");
    }
    function search() {
    	var params = {};    	
    	params.pageLength = 10;
    	
        var itemTemplate = container.find(selectors.templates.session).html();
        var c = container.find(selectors.sessions);
        var form = container.find(selectors.searchForm);
        
        ajaxDataTableInit(c,form,itemTemplate,script,params);
        getSessionsChart();
    }
    function initSessions(){
    	var c = container.find(selectors.sessions);    	    	    	    	  
    }
    function remove(btn){
    	if(!confirm("Are you sure you want to delete this session?"))return;
    	   
    	btn.button('loading');
    	var id = btn.closest('tr').attr('data-id');
    	$.post(script,{action: 'removeSession', id:id},function(json){
    		btn.button('reset');
    		if(json.error){ $.error(json.error); }
    		else{
    			btn.closest('tr').fadeOut(function(){ $(this).remove(); });
    		}
    		
    	},"json");
    }
    function snapshot(btn){
    	var id = btn.closest('tr').attr('data-id');
    	var msg = btn.attr('data-msg');
    	var type = btn.attr('data-type');
    	
    	modal = new Modal({
			parent: container,
			static: true,
			title: 'Snapshot',
			template: selectors.templates.snapshot,
			templateData: {'id':id, 'msg': msg, 'type': type},
			buttons: new Array($('<button>').addClass('btn btn-default dialog-close').text('Done'))
		});
    }
    
    function getSessionsChart(){
    	var pId = container.find(selectors.productInput).val();
    	
    	var c = container.find(selectors.chart);
    	var form = container.find(selectors.searchForm);
    	var data = form.serializeObject();
    	data.action = 'getSessionsChartData';
    	   	
    	c.loading(true);
    	$.post(script,data,function(json){
    		c.loading(false);
    		if(json.error){ $.error(json.error); }
    		else{
    			var data = new google.visualization.DataTable();
    			data.addColumn('date', 'X');
    			data.addColumn('number', 'Total');
    			data.addColumn('number', 'Valid');	
    			data.addColumn('number', 'Errors');
    			data.addColumn('number', 'Pending');	
    				
    			$.each(json.series,function(n,el){
    				el[0] = new Date(el[0]);
    				data.addRow(el);			
    			})
    			
    			var options = {
    				hAxis: {
    				title: 'Time'
    				},
    				vAxis: {
    				title: 'Sessions'
    				},
    				//colors: ['red','green']
    			};
    		
    			var chart = new google.visualization.LineChart(c[0]);		
    			chart.draw(data, options);    			
    		}
    	},"json");
    }

});