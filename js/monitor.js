$(document).ready(function () {
    var script = "ajax/php/monitor.php";
    var selectors = {
        container				: ".page-monitor",

        system:{
        	container			: '.system-container',
        },
        sessions:{
        	container			: '.sessions-container',
        },        
        stats24:{
        	container			: '.24stats-container',
        },
        users:{
        	container			: '.users-container',
        },

        templates: { 
        	system				: '#template-system',
        	sessions			: '#template-sessions',
        	stats24				: '#template-24stats',
        	users				: '#template-users',
        },               
	}    
    var container = $(selectors.container);
    var modal;
    bind();
	init();

    function init() {
    	google.charts.load('current', {packages: ['corechart', 'line']});
    	//google.charts.setOnLoadCallback(function(){ loadVisits(); });		
    	
		update();		
    }
    function bind() {
    	
    }       
    function update(){
    	processTimestamps();
    	infiniteAsyncLaunch(updateSystem,5);   
    	infiniteAsyncLaunch(updateSessions,5);
    	infiniteAsyncLaunch(update24,30);
    	infiniteAsyncLaunch(users,30);
    }
    function infiniteAsyncLaunch(target,sleep,callback){
    	$.when(target()).done(function(){ 
    		setTimeout(function(){ infiniteAsyncLaunch(target,sleep); },parseInt(sleep)*1000);    		
    		return false; 
    	});    	
    }
    function updateWidget(data,widgetContainer,widgetTemplate,location){
    	if(!$.defined(location))location=false;
    	var dfd = $.Deferred();
    	var c = container.find(widgetContainer);
    	    	
    	$.post(script,data,function(json){
    		if(json.error){ $.error(json.error); }
    		else{    			
    			var template = Handlebars.compile(container.find(widgetTemplate).html());
    			switch(location){
    				case 'append':
    					c.append(template(json));
    					break;
    				case 'prepend':
    					c.prepend(template(json));
    					break;
    				default:
    					c.html(template(json));
    					break;
    			}
    		}
    		dfd.resolve(json);    		
    	},"json");
    	return dfd.promise();
    }   
    function processTimestamps(){
    	updateTimestamps();
    	setTimeout(function(){ processTimestamps(); },1000);
    }
    function updateTimestamps(){
    	$.each($('.human-timestamp:not(".has-init")'),function(){
    		var ts = $(this).attr('data-timestamp');
    		if($.defined(ts)){
    			$(this).text(moment(ts).subtract(7, 'hours').fromNow());    			
    		}    		 
    	});
    }
    function updateSystem(){
    	return updateWidget({'action':'getSystemStats'},selectors.system.container,selectors.templates.system);
    }
    function updateSessions(){
    	return updateWidget({'action':'getSessionsStats'},selectors.sessions.container,selectors.templates.sessions);
    }
    function update24(){
    	return updateWidget({'action':'get24Stats'},selectors.stats24.container,selectors.templates.stats24);
    }
    function users(){
    	$.when(updateWidget({'action':'getUsersStats'},selectors.users.container,selectors.templates.users)).done(function(data){
    		console.log(data.stats);
    		if(!data.stats.users)return;
    		    		  
    		$.each(data.stats.users,function(uId,el){
    			var sessions = el.sessions;    			
    			
    			var chartData = new Array();
    			chartData.push(new Array("Status","Sessions"));
    			
    			$.each(sessions,function(label,num){    				
    				chartData.push(new Array(label,parseInt(num)));
    			});
 
    			 var series = google.visualization.arrayToDataTable(chartData);
    			 //var options = {title: el.username+' Stats',is3D: true};
    			 var options = {is3D: true};
    			 var chart = new google.visualization.PieChart(document.getElementById("user-chart-"+uId));
    			 chart.draw(series, options);        		                                                     		
    		});
         		
    	});
    } 
});