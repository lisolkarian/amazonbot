$(document).ready(function () {
    var script = "ajax/php/keywords.php";
    var selectors = {
        container			: ".page-keywords",
                        
        upload				: '.fileupload',
		uploading			: '.uploading',	
				
        item				: '.keyword-item',
        create				: '.create-a',                
        keywords			: '.keywords-container',
        searchFrom			: '.search-form',
        search				: '.search-a',
        clear				: '.clear-a',
        productInput		: '[name="filter[product_id]"]',
        
        pies				: '.pie',
        
        historyChart		: '.history-line-chart',
        historyForm			: '.history-search-form',
        
        badgesChart			: '.badges-pie-chart',
        badgesForm			: '.badges-search-form',
        
        edit				: '.edit-a',
		remove				: '.remove-a',
		add					: '.add-a',	
		rank				: '.rank-a',
		history				: '.history-a',
		badges				: '.badges-a',
                                
        summary				: '.summary-container',
        
        selectAll			: '.select-all',
        createSessionsSelected: '.keywords-create-sessions-a',
		
        templates: {                    	
        	upload			: 'ajax/template/keywords/_upload.phtml',
        	summary			: '#template-summary',
        	keyword			: '#template-keyword',        
			edit			: 'ajax/template/keywords/_edit.phtml',
			add				: 'ajax/template/keywords/_add.phtml',
			history			: 'ajax/template/keywords/_history.phtml',
			badges			: 'ajax/template/keywords/_badges.phtml',
        },
	}    
    var container = $(selectors.container);    
    var modal;
    var pId;
    
    init();
    bind();

    function init() {    	    	    	
    	google.charts.load('current', {packages: ['corechart', 'line']}); 	
    	//search();
    }

    function bind() {      	
    	$(window).on('ajaxDataTableReady', function(e){ initKeywords($(this)); });
    	$(window).on('sessionsUpdated',function(){ search(); })
    	$(window).on('productSelected',function(e, id){ updateProductInput(id); })    	
    	
    	container.on('change.bs.fileinput',selectors.upload, function(){ importTerms(); });    	
    	
    	container.on('click',selectors.search, function(e){ e.preventDefault(); search($(this)); });
		container.on('click',selectors.add, function(e){ e.preventDefault(); addModal($(this)); });
		container.on('click',selectors.edit, function(e){ e.preventDefault(); editModal($(this)); });
		container.on('click',selectors.remove, function(e){ e.preventDefault(); remove($(this)); });
		container.on('click',selectors.rank, function(e){ e.preventDefault(); rank($(this)); });
		container.on('click',selectors.history, function(e){ e.preventDefault(); history($(this)); });
		container.on('click',selectors.badges, function(e){ e.preventDefault(); badges($(this)); });
		
		container.on('change',selectors.historyForm+' input', function(e){ e.preventDefault(); getHistory(); });
		container.on('change',selectors.badgesForm+' input', function(e){ e.preventDefault(); getBadgesHistory(); });
		
		container.on('click',selectors.createSessionsSelected, function(e){ e.preventDefault(); createSessionsSelected($(this)); });
		container.on('click',selectors.selectAll, function(e){ selectAll($(this)); });
		
    }   
    function updateProductInput(id){    	
    	container.find(selectors.productInput).val(id);    	
    	search();
    }
    
    function selectAll(input){
    	var items = container.find(selectors.item).find('input[type="checkbox"]');
    	if(input.is(':checked')){
    		items.prop('checked',true);
    	}
    	else{
    		items.prop('checked',false);
    	}
    }
    function createSessionsSelected(){
    	var ids = new Array();
    	var selected = container.find(selectors.item).filter(function(){ return $(this).find('input[type="checkbox"]').is(':checked'); })
    	
    	$.each(selected, function(){
    		var el = $(this);
    		ids.push(el.attr('data-id'));
    	});
    	
    	if(ids.length<=0){ $.error("No keywords selected"); return; }
    	
    	$(window).trigger('keywordsCreateSessions',{'ids':ids});
    }
    
    function badges(btn){
    	var id = btn.closest(selectors.item).attr('data-id');
    	
    	modal = new Modal({
			parent: container,
			static: true,
			title: "Badges History",
			size: '50%',
			template: selectors.templates.badges,
			templateData: {id: id},
			callback: function(){ 				
				setTimeout(function(){getBadgesHistory();},300);
				/*
				setTimeout(function(){
					google.charts.load('current', {packages: ['corechart', 'line']});
					google.charts.setOnLoadCallback(getHistory);
				},500);
				*/
			},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Done')			
			)
		});
    }
    /*
    function getBadgesHistory(){
    	var id = modal.id;
    	var form = container.find(selectors.badgesForm);
    	
    	modal.loader();
    	$.post(script,form.serialize(),function(json){
    		modal.loader(false);
    		if(json.error){ $.error(json.error); }
    		else{
    			if(json.series.length<=0){
    				container.find(selectors.badgesChart).html('No data found');
    				return;
    			}
    			    			
    			var data = google.visualization.arrayToDataTable(json.series);
    			    			
    			var options = {};    		
    			var chart = new google.visualization.PieChart(container.find(selectors.badgesChart)[0]);    			
    			chart.draw(data, options);   
    		}
    	},"json");
    }
    */
    function getBadgesHistory(){
    	var id = modal.id;
    	var form = container.find(selectors.badgesForm);
    	
    	modal.loader();
    	$.post(script,form.serialize(),function(json){
    		modal.loader(false);
    		if(json.error){ $.error(json.error); }
    		else{
    			if(!json.chart.labels || json.chart.labels.length<=0){
    				container.find(selectors.historyChart).html('No data found');
    				return;
    			}
    			var data = new google.visualization.DataTable();
    			data.addColumn('date', 'X');
    			data.addColumn('number', 'Bade Found');    			    			

    			$.each(json.chart.series,function(n,el){
    				el[0] = new Date(el[0]);    				    		
    				data.addRow(el);
    			});
    			
    			var options = {
    				hAxis: {title: 'Date'},
    				vAxis: {title: 'Badge Found'},    				
    			};    		
    			var chart = new google.visualization.ScatterChart(container.find(selectors.badgesChart)[0]);    			
    			chart.draw(data, options);   
    		}
    	},"json");
    }
    
    
    function history(btn){
    	var id = btn.closest(selectors.item).attr('data-id');
    	
    	modal = new Modal({
			parent: container,
			static: true,
			title: "Rank History",
			size: '80%',
			template: selectors.templates.history,
			templateData: {id: id},
			callback: function(){ 				
				setTimeout(function(){getHistory();},300);
				/*
				setTimeout(function(){
					google.charts.load('current', {packages: ['corechart', 'line']});
					google.charts.setOnLoadCallback(getHistory);
				},500);
				*/
			},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Done')			
			)
		});
    }
    function getHistory(){
    	var id = modal.id;
    	var form = container.find(selectors.historyForm);
    	
    	modal.loader();
    	$.post(script,form.serialize(),function(json){
    		modal.loader(false);
    		if(json.error){ $.error(json.error); }
    		else{
    			if(json.labels.length<=0){
    				container.find(selectors.historyChart).html('No data found');
    				return;
    			}
    			var data = new google.visualization.DataTable();
    			data.addColumn('date', 'X');
    			data.addColumn('number', 'Rank (scatter)');
    			data.addColumn('number', 'Position (line)');    				

    			$.each(json.series,function(n,el){
    				el[0] = new Date(el[0]);    				    		
    				data.addRow(el);
    			});
    			
    			var options = {
    				hAxis: {title: 'Date'},
    				vAxis: {title: 'Rank / Positions'},    				
    				//colors: ['red','green']
    				series: { 
    			        0: {
    			        	pointSize: 3,     			        	
    			        }, 
    			        1: {
    			            lineWidth: 2,
    			            pointSize: 0,    			            
    			        }
    			    }
    			};    		
    			//var chart = new google.visualization.LineChart(container.find(selectors.historyChart)[0]);
    			var chart = new google.visualization.ScatterChart(container.find(selectors.historyChart)[0]);    			
    			chart.draw(data, options);   
    		}
    	},"json");
    }

    
    function rank(btn){
    	var id = btn.closest(selectors.item).attr('data-id');
    	
    	btn.button('loading');    	
    	$.post(script,{action: 'rankKeyword', id: id},function(json){
    		btn.button('loading');
    		
    		if(json.error){ $.error(json.error); }
    		else{ reloadItem(id); }
    	},"json");
    	
    }
	function importTerms(){		
		var form = container.find(selectors.upload);
		var data = new FormData();
		data.append('file', form.find('input[type="file"]')[0].files[0]);
		data.append('action', 'importTerms');
		form.find('.btn').hide();
		form.find(selectors.uploading).show();
		
		$.ajax({
			url: script,
			data: data,
			processData: false,
			contentType: false,
			dataType: "json",
			type: 'POST',
			success: function(json) {
				form.find('.btn').show();
				form.find(selectors.uploading).hide();												
				modal.modal.find('textarea').val(json.items.join("\r\n"));
			}
		});
	}	    
    function addModal(btn){    		
    	var pId = container.find(selectors.productInput).val();
    	
		modal = new Modal({
			parent: container,
			static: true,
			title: "Add Keyword(s)",
			template: selectors.templates.add,
			templateData: {pId: pId},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Cancel'),
				$('<a>').addClass('btn btn-primary').text(' Save').prepend($('<i class="fa fa-save"></i>')).click(function(){ add($(this)); })
			)
		});
    }
    function add(btn){
    	var form = modal.modal.find('form');
		if(!$.defined(form)){ $.error('Unable to add keywords!'); return; }
		
		modal.block();
		btn.button('loading');
		$.post(script,form.serialize(),function(json){
			modal.release();
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{ search(); modal.close(); }
		},"json");
    }
    function reloadItem(id){
    	var item = container.find(selectors.item).filter(function(){ return $(this).attr('data-id')==id; })
    	if(item.length<=0){ $.error("Item not found!"); return; }
    	
    	$.post(script,{action: 'loadKeyword', id: id},function(json){
    		if(json.error){ $.error(json.error); }
    		else{
    			var html = Handlebars.compile(container.find(selectors.templates.keyword).html());
    			item.replaceWith(html(json.item));
    			
    			initKeywords();
    		}    		
    	},"json");
    }
    function search() {
    	var params = {};    	
    	params.pageLength = 10;
    	
        var itemTemplate = container.find(selectors.templates.keyword).html();
        var c = container.find(selectors.keywords);
        var form = container.find(selectors.searchFrom);
        
        ajaxDataTableInit(c,form,itemTemplate,script,params);        
    }
    function initKeywords(){
    	var c = container.find(selectors.keywords);
    	
    	c.find(selectors.pies).peity("line");    	    	   
    }
    
    function updateSummary(){
    	var c = container.find(selectors.summary);
    	$.post(script,{action: 'getSummary'},function(json){
    		if(json.error){ c.html("Error: "+json.error); }
    		else{
    			var template = Handlebars.compile(container.find(selectors.templates.summary).html())
    			c.html(template(json));
    		}
    		//setTimeout(function(){ updateSummary() },5*1000);
    	},"json")
    }
	function editModal(btn){
		var item = btn.closest('tr');
		var id = item.attr('data-id');
		
		if(item.length<=0){ $.error("Item not found"); return; }
		modal = new Modal({
			parent: container,
			static: true,
			title: "Edit Keyword",
			template: selectors.templates.edit,
			templateData: {id: id, keyword: item.attr('data-keyword')},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Cancel'),
				$('<a>').addClass('btn btn-primary').text(' Save').prepend($('<i class="fa fa-save"></i>')).click(function(){ save($(this),id); })
			)
		});
	}
	function save(btn,id){
		var form = modal.modal.find('form');
		if(!$.defined(form)){ $.error('Unable to save changes!'); return; }
		
		modal.block();
		btn.button('loading');
		$.post(script,form.serialize(),function(json){
			modal.release();
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{ reloadItem(id); modal.close(); }
		},"json");
	}
	function remove(btn){
		var item = btn.closest(selectors.item);
		var id = item.attr('data-id');	
		if(!$.defined(id)){ $.error('Unable to delete keyword!'); return; }
		if(!confirm('Are you sure you want to delete this keyword?'))return;
		
		btn.button('loading');
		$.post(script,{action: 'removeKeyword', id: id},function(json){
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{ item.fadeOut(); }
		},"json");
	}
});