$(document).ready(function () {
    var script = "ajax/php/bots.php";
    var selectors = {
        container			: ".page-bots",
		
        create				: '.create-a',
        remove				: '.remove-a',
        snapshot			: '.snapshot-a',
        sessions			: '.sessions-container',
        searchFrom			: '.search-form',
        search				: '.search-a',
        clear				: '.clear-a',
        
        summary				: '.summary-container',
		
        templates: {            
        	snapshot		: 'ajax/template/bots/_snapshot.phtml',
        	summary			: '#template-summary',
        	session			: '#template-session',
        },
	}    
    var container = $(selectors.container);
    var snapshotFolder = '/app/snapshots/';
    var modal;

    init();
    bind();

    function init() {	    	
    	updateSummary();
    	search();
    }

    function bind() {
		container.find(selectors.create).click(function(e){ e.preventDefault(); create($(this)); });					
		container.find(selectors.search).click(function(e){ e.preventDefault(); search($(this)); });
		container.on('click',selectors.clear, function(e){ e.preventDefault(); clear($(this)); });
		container.on('click',selectors.remove,function(e){ e.preventDefault(); remove($(this)); });
		container.on('click',selectors.snapshot,function(e){ e.preventDefault(); snapshot($(this)); });			
    }
    
    function clear(btn){
    	if(!confirm("Are you sure you want to remove current sessions?"))return;
    	btn.button('loading');
    	$.post(script,{action: 'clearData'},function(json){
    		window.location.reload();    		
    	},"json");
    }
    function search() {
    	var params = {};    	
    	params.pageLength = 10;
    	
        var itemTemplate = container.find(selectors.templates.session).html();
        var c = container.find(selectors.sessions);
        var form = container.find(selectors.searchFrom);
        
        ajaxDataTableInit(c,form,itemTemplate,script,params);        
    }
    function updateSummary(){
    	var c = container.find(selectors.summary);
    	$.post(script,{action: 'getSummary'},function(json){
    		if(json.error){ c.html("Error: "+json.error); }
    		else{
    			var template = Handlebars.compile(container.find(selectors.templates.summary).html())
    			c.html(template(json));
    		}
    		setTimeout(function(){ updateSummary() },5*1000);
    	},"json")
    }
    function snapshot(btn){
    	var id = btn.closest('tr').attr('data-id');
    	var msg = btn.attr('data-msg');
    	var type = btn.attr('data-type');
    	
    	modal = new Modal({
			parent: container,
			static: true,
			title: 'Snapshot',
			template: selectors.templates.snapshot,
			templateData: {'id':id, 'msg': msg, 'type': type},
			buttons: new Array($('<button>').addClass('btn btn-default dialog-close').text('Done'))
		});
    }
    function remove(btn){
    	if(!confirm("Are you sure you want to delete this session?"))return;
    	   
    	btn.button('loading');
    	var id = btn.closest('tr').attr('data-id');
    	$.post(script,{action: 'removeSession', id:id},function(json){
    		btn.button('reset');
    		if(json.error){ $.error(json.error); }
    		else{
    			btn.closest('tr').fadeOut(function(){ $(this).remove(); });
    		}
    		
    	},"json");
    }
	function create(btn){		
		var form = btn.closest('form');			
		btn.button('loading');
		
		$.post(script,form.serialize(),function(json){			
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{ window.location.reload(); }
		},"json");
	}	
});