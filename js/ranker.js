$(document).ready(function () {
    var script = "ajax/php/ranker.php";
    var selectors = {
        container			: ".page-ranker",
                        
        upload				: '.fileupload',
		uploading			: '.uploading',	
				
        item				: '.keyword-item',
        create				: '.create-a',                
        keywords			: '.keywords-container',
        searchFrom			: '.search-form',
        search				: '.search-a',
        clear				: '.clear-a',
        select				: '.select-a',
        selectedCounter		: '.selected-counter',
        
        productList			: '.product-list',
        productItem			: '.product-item',  
        productToggle		: '.product-toggle-a',
        productSelect		: '.product-select-a',
        
        
        rankSelected		: '.rank-selected-a',  
        scheduleInput		: '.schedule-input',
        
        pies				: '.pie',
        
        historyChart		: '.history-line-chart',
        historyForm			: '.history-search-form',
        
        badgesChart			: '.badges-pie-chart',
        badgesForm			: '.badges-search-form',
        
        edit				: '.edit-a',
		remove				: '.remove-a',
		add					: '.add-a',	
		rank				: '.rank-a',
		history				: '.history-a',
		badges				: '.badges-a',
		
		selectAll			: '.select-all',
                                        
		
        templates: {        
        	productItem		: '#template-product-item',
        	upload			: 'ajax/template/ranker/_upload.phtml',
        	summary			: '#template-summary',
        	keyword			: '#template-keyword',        
			edit			: 'ajax/template/ranker/_edit.phtml',
			add				: 'ajax/template/ranker/_add.phtml',
			history			: 'ajax/template/ranker/_history.phtml',
			badges			: 'ajax/template/ranker/_badges.phtml',
        },
	}    
    var container = $(selectors.container);    
    var modal;
    var pId;
    var selected = {};
    
    init();
    bind();

    function init() {    	    	    	     
    	search();
    }

    function bind() {      	
    	$(window).on('ajaxDataTableReady', function(e){ initKeywords($(this)); });
    	        	
    	container.on('change.bs.fileinput',selectors.upload, function(){ importTerms(); });    	
    	
    	container.on('click',selectors.search, function(e){ e.preventDefault(); search($(this)); });
		container.on('click',selectors.add, function(e){ e.preventDefault(); addModal($(this)); });
		container.on('click',selectors.edit, function(e){ e.preventDefault(); editModal($(this)); });
		container.on('click',selectors.remove, function(e){ e.preventDefault(); remove($(this)); });
		container.on('click',selectors.rank, function(e){ e.preventDefault(); rank($(this)); });
		container.on('click',selectors.history, function(e){ e.preventDefault(); history($(this)); });
		container.on('click',selectors.badges, function(e){ e.preventDefault(); badges($(this)); });
		container.on('click',selectors.select, function(e){ select($(this)); });
		
		
		container.on('keyup',selectors.scheduleInput, function(e){ e.preventDefault(); updateScheduleInput($(this)); });
		container.on('change',selectors.historyForm+' input', function(e){ e.preventDefault(); getHistory(); });
		container.on('change',selectors.badgesForm+' input', function(e){ e.preventDefault(); getBadgesHistory(); });
		
		container.on('click',selectors.rankSelected, function(e){ e.preventDefault(); rankSelected($(this)); });
		container.on('click',selectors.selectAll, function(e){ selectAll($(this)); });		
		
		container.on('click',selectors.productToggle, function(e){ toggleProduct($(this)); });
		container.on('click',selectors.productSelect, function(e){ e.preventDefault(); e.stopPropagation(); selectProductAll($(this)); });
		
    }  
    function select(btn){
    	var item = btn.closest(selectors.item);
    	var on = btn.is(':checked');
    	var id = item.attr('data-id');
    	
    	if(on)
    		selected[id] = item;
    	else
    		delete selected[id];
    	    
    	updateSelectedCounter();
    }
    function updateSelectedCounter(){
    	container.find(selectors.selectedCounter).text(Object.keys(selected).length);
    }
    function toggleProduct(btn){
    	var item = btn.closest(selectors.productItem);    	    	
    	var open = item.find('.collapse.in').length<1; 
    	var asin = item.attr('data-asin');
    	var select = item.find(selectors.productSelect);
    	
    	if(open){
    		select.fadeIn();
    		loadProductKeywords(asin);
    	}
    	else{
    		select.fadeOut();
    	}
    }
    function updateScheduleInput(obj){
    	var value = parseInt(obj.val());
    	
    	if(isNaN(value)) obj.val(0);    	
    	if(value<0)obj.val(0);
    }
    function selectAll(input){
    	var items = container.find(selectors.item).find('input[type="checkbox"]');
    	if(input.is(':checked')){
    		items.prop('checked',true);
    	}
    	else{
    		items.prop('checked',false);
    	}
    }
    function selectProductAll(btn){
    	var items = btn.closest(selectors.productItem).find('input[type="checkbox"]');    	    	
    	
    	if(!btn.hasClass('on')){
    		btn.html('<small>Unselect All</small>');
    		items.prop('checked',true);    		    	
    	}
    	else{
    		btn.html('<small>Select All</small>');
    		items.prop('checked',false);
    	}
    	$.each(items,function(){ select($(this)); })
    	btn.toggleClass('on');
    	updateSelectedCounter();
    }
    function importTerms(){		
		var form = container.find(selectors.upload);
		var data = new FormData();
		data.append('file', form.find('input[type="file"]')[0].files[0]);
		data.append('action', 'importKeywords');
		form.find('.btn').hide();
		form.find(selectors.uploading).show();
		
		$.ajax({
			url: script,
			data: data,
			processData: false,
			contentType: false,
			dataType: "json",
			type: 'POST',
			success: function(json) {
				form.find('.btn').show();
				form.find(selectors.uploading).hide();												
				modal.modal.find('textarea').val(json.items.join("\r\n"));
			}
		});
	}
    function addModal(btn){    		
    	var pId = container.find(selectors.productInput).val();
    	
		modal = new Modal({
			parent: container,
			static: true,
			title: "Add Keyword(s)",
			template: selectors.templates.add,
			templateData: {pId: pId},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Cancel'),
				$('<a>').addClass('btn btn-primary').text(' Save').prepend($('<i class="fa fa-save"></i>')).click(function(){ add($(this)); })
			)
		});
    }
    function add(btn){
    	var form = modal.modal.find('form');
		if(!$.defined(form)){ $.error('Unable to add keywords!'); return; }
		if(!form.valid())return;
		
		modal.block();
		btn.button('loading');
		$.post(script,form.serialize(),function(json){
			modal.release();
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{ search(); modal.close(); }
		},"json");
    }
    function reloadItem(id){
    	var item = container.find(selectors.item).filter(function(){ return $(this).attr('data-id')==id; })
    	if(item.length<=0){ $.error("Item not found!"); return; }
    	
    	$.post(script,{action: 'loadKeyword', id: id},function(json){
    		if(json.error){ $.error(json.error); }
    		else{
    			var html = Handlebars.compile(container.find(selectors.templates.keyword).html());
    			item.replaceWith(html(json.item));
    			
    			initKeywords();
    		}    		
    	},"json");
    }
    function search() {
        $.post(script,{action: 'getUniqueProducts', ajax: '1'}, function(json){
        	if(json.error){ $.error(json.error); }
        	else{
        		var c = container.find(selectors.productList);
        		var html = Handlebars.compile(container.find(selectors.templates.productItem).html());
        		c.html('');
        		$.each(json.items,function(n,el){        			        		
        			c.append(html(el));
        		});        		        		        	
        	}
        	
        },"json");
        
        //ajaxDataTableInit(c,form,itemTemplate,script,params);        
    }
    function loadProductKeywords(asin){
    	var params = {};    	
    	params.pageLength = 10;    	    	
    	
    	var item = container.find(selectors.productItem).filter(function(){ return $(this).attr('data-asin')==asin; });
    	        
        $.post(script,{action: 'loadProductKeywords', 'asin': asin}, function(json){
        	if(json.error){ $.error(json.error); }
        	else{
        		var c = item.find(selectors.keywords);
        		var html = Handlebars.compile(container.find(selectors.templates.keyword).html());
        		c.html('');
        		$.each(json.items,function(n,el){           			
        			c.append(html(el));
        		});
        		initKeywords();
        	}
        	
        },"json");
        
    }
    function rankSelected(){
    	var selected = container.find(selectors.item).filter(function(){ return $(this).find('input[type="checkbox"]').is(':checked'); })
    	$.each(selected, function(){
    		var el = $(this);
    		
    		el.find(selectors.rank).click();
    	})    	
    }
    function rank(btn){    	
    	if(btn.hasClass('checking')){ return; }
    	
    	var id = btn.closest(selectors.item).attr('data-id');
    	
    	btn.button('loading');
    	btn.addClass('checking');
    	$.post(script,{action: 'rankKeyword', id: id},function(json){
    		btn.button('loading');
    		btn.removeClass('checking');
    		
    		if(json.error){ $.error(json.error); }
    		else{ reloadItem(id); }
    	},"json");
    	
    }		   
    
    
    function initKeywords(){
    	var c = container.find(selectors.keywords);
    	
    	c.find(selectors.pies).peity("line");
    	
    	var keywords = c.find(selectors.item);
    	$.each(keywords,function(){ var el = $(this); if(selected[el.attr('data-id')]) el.find('input[type="checkbox"]').prop('checked',true); })
    }
    
    function updateSummary(){
    	var c = container.find(selectors.summary);
    	$.post(script,{action: 'getSummary'},function(json){
    		if(json.error){ c.html("Error: "+json.error); }
    		else{
    			var template = Handlebars.compile(container.find(selectors.templates.summary).html())
    			c.html(template(json));
    		}
    		//setTimeout(function(){ updateSummary() },5*1000);
    	},"json")
    }
	function editModal(btn){
		var item = btn.closest(selectors.item);
		var id = item.attr('data-id');
		
		if(item.length<=0){ $.error("Item not found"); return; }
		modal = new Modal({
			parent: container,
			static: true,
			title: "Edit Keyword",
			template: selectors.templates.edit,
			templateData: {id: id, keyword: item.attr('data-keyword'), schedule: item.attr('data-schedule')},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Cancel'),
				$('<a>').addClass('btn btn-primary').text(' Save').prepend($('<i class="fa fa-save"></i>')).click(function(){ save($(this),id); })
			)
		});
	}
	function save(btn,id){
		var form = modal.modal.find('form');
		if(!$.defined(form)){ $.error('Unable to save changes!'); return; }
		
		modal.block();
		btn.button('loading');
		$.post(script,form.serialize(),function(json){
			modal.release();
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{ reloadItem(id); modal.close(); }
		},"json");
	}
	function remove(btn){
		var item = btn.closest(selectors.item);
		var id = item.attr('data-id');	
		if(!$.defined(id)){ $.error('Unable to delete keyword!'); return; }
		if(!confirm('Are you sure you want to delete this keyword?'))return;
		
		btn.button('loading');
		$.post(script,{action: 'removeKeyword', id: id},function(json){
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{ item.fadeOut(); }
		},"json");
	}    
    function badges(btn){
    	var id = btn.closest(selectors.item).attr('data-id');
    	
    	modal = new Modal({
			parent: container,
			static: true,
			title: "Badges History",
			size: '50%',
			template: selectors.templates.badges,
			templateData: {id: id},
			callback: function(){ 				
				setTimeout(function(){getBadgesHistory();},300);
				/*
				setTimeout(function(){
					google.charts.load('current', {packages: ['corechart', 'line']});
					google.charts.setOnLoadCallback(getHistory);
				},500);
				*/
			},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Done')			
			)
		});
    }
    function getBadgesHistory(){
    	var id = modal.id;
    	var form = container.find(selectors.badgesForm);
    	
    	modal.loader();
    	$.post(script,form.serialize(),function(json){
    		modal.loader(false);
    		if(json.error){ $.error(json.error); }
    		else{
    			if(json.series.length<=0){
    				container.find(selectors.badgesChart).html('No data found');
    				return;
    			}
    			    			
    			var data = google.visualization.arrayToDataTable(json.series);
    			    			
    			var options = {};    		
    			var chart = new google.visualization.PieChart(container.find(selectors.badgesChart)[0]);    			
    			chart.draw(data, options);   
    		}
    	},"json");
    }
    
    
    function history(btn){
    	var id = btn.closest(selectors.item).attr('data-id');
    	
    	modal = new Modal({
			parent: container,
			static: true,
			title: "Rank History",
			size: '80%',
			template: selectors.templates.history,
			templateData: {id: id},
			callback: function(){ 				
				//setTimeout(function(){getHistory();},300);				
				setTimeout(function(){
					google.charts.load('current', {packages: ['corechart', 'line']});
					google.charts.setOnLoadCallback(getHistory);
				},500);				
			},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Done')			
			)
		});
    }
    function getHistory(){
    	var id = modal.id;
    	var form = container.find(selectors.historyForm);
    	
    	modal.loader();
    	$.post(script,form.serialize(),function(json){
    		modal.loader(false);
    		if(json.error){ $.error(json.error); }
    		else{
    			if(json.labels.length<=0){
    				container.find(selectors.historyChart).html('No data found');
    				return;
    			}
    			var data = new google.visualization.DataTable();
    			data.addColumn('date', 'X');    			
    			data.addColumn('number', 'Rank');    				

    			$.each(json.series,function(n,el){
    				el[0] = new Date(el[0]);    				    		
    				data.addRow(el);
    			});
    			
    			var options = {
    				hAxis: {title: 'Date'},
    				vAxis: {title: 'Rank'},    				
    				//colors: ['red','green']
    				series: { 
    			        0: {
    			        	pointSize: 3,     			        	    			        
    			            lineWidth: 2,    			                			           
    			        }
    			    }
    			};    		
    			var chart = new google.visualization.LineChart(container.find(selectors.historyChart)[0]);
    			//var chart = new google.visualization.ScatterChart(container.find(selectors.historyChart)[0]);    			
    			chart.draw(data, options);   
    		}
    	},"json");
    }    
});