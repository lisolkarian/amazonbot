$(document).ready(function () {
    var script = "ajax/php/fivek/index.php";
    var selectors = {
        container			: ".page-fivek",
				
        item				: 'tr.search-item',                       
        searches			: '.searches-container',
        searchForm			: '.search-form',
        search				: '.search-a',
        create				: '.create-a',                
        remove				: '.remove-a',
        results				: '.results-a',
        redo				: '.redo-a',
        
        quick				: '.quick-a',
        quickSearch			: '.quick-search-a',
        quickResult			: '.quick-result',
        quickFound			: '.quick-result-found',
        quickNotFound		: '.quick-result-notfound',
        quickError			: '.quick-result-error',
        
        upload				: '.fileupload',
		uploading			: '.uploading',
                
        templates: {                    	        	        	
        	item			: '#template-search',        	
        	create			: 'ajax/template/fivek/_create.phtml',
        	results			: 'ajax/template/fivek/_results.phtml',
        	quick			: 'ajax/template/fivek/_quick.phtml',
        },
	}    
    var container = $(selectors.container);
    var form = container.find(selectors.searchForm);
    var modal;
    var pId;
    
    init();
    bind();

    function init() {    	    	    	    	    	   
    	search();
    	update();
    }

    function bind() { 
    	container.on('change.bs.fileinput',selectors.upload, function(){ importKeywords(); });
    	
    	container.on('click',selectors.create, function(e){ e.preventDefault(); createModal($(this)); });
    	container.on('click',selectors.results, function(e){ e.preventDefault(); resultsModal($(this)); });
    	container.on('click',selectors.redo, function(e){ e.preventDefault(); redo($(this)); });
    	
    	container.on('click',selectors.quick, function(e){ e.preventDefault(); quickModal($(this)); });
    	container.on('click',selectors.quickSearch, function(e){ e.preventDefault(); quickSearch($(this)); });    	
    	
    	container.on('click',selectors.search, function(e){ e.preventDefault(); search($(this)); });		
		container.on('click',selectors.remove, function(e){ e.preventDefault(); remove($(this)); });		
		form.on('change','input,select',function(e){ search(); });
    }
    function update(){
    	var ids = new Array();
    	var items = container.find(selectors.item);
    	$.each(items,function(){ ids.push($(this).attr('data-id')); });
    	
    	$.post(script,{action: 'updateSearches', ids: ids}, function(json){
    		if(json.error){ $.error(json.error); }
    		else{
    			$.each(json.items,function(k,v){
    				var id = v.id;
    				
    				var item = container.find(selectors.item).filter(function(){ return $(this).attr('data-id')==id; })
    		    	if(item.length>0){    				
    		    		var html = Handlebars.compile(container.find(selectors.templates.item).html());
    		    		item.replaceWith(html(v));
    		    	}
    			})
    			setTimeout(function(){ update(); },500);
    		}
    	},"json");    	
    }
    function quickModal(){    	
    	modal = new Modal({
			parent: container,
			static: true,
			title: "Quick Check",			
			template: selectors.templates.quick,			
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Done')				
			)
		});
    }
    function quickSearch(btn){    	    	
    	var form = modal.modal.find('form');
    	if(!form.valid()) return;
    	
    	var res = container.find(selectors.quickResult);
    	res.hide();
    	
    	modal.block();
    	btn.button('loading');
    	$.post(script,form.serialize(),function(json){
    		modal.release();
    		btn.button('reset');
    		
    		if(json.error){ $.error(json.error); }
    		else{    			
    			if(json.result == 'error') container.find(selectors.quickError).fadeIn();
    			if(json.result == '1') container.find(selectors.quickFound).fadeIn();
    			if(json.result == '0') container.find(selectors.quickNotFound).fadeIn();
    		}
    	},"json");
    	
    }
    
    function redo(btn){
    	var id = btn.closest(selectors.item).attr('data-id');
    	if(!confirm("Are you sure you want to reset and rerun this search?"))return;
    	
    	$.post(script,{action: 'redoSearch', id:id},function(json){
    		if(json.error){ $.error(json.error); }
    		else{
    			reloadItem(id);
    		}
    	},"json");
    	
    }
    function resultsModal(btn){
    	var id = btn.closest(selectors.item).attr('data-id');
    	modal = new Modal({
			parent: container,
			static: true,
			title: "Results",
			size: '80%',
			template: selectors.templates.results,
			templateData: {id:id},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Done')				
			)
		});
    }
    function importKeywords(){		
		var form = container.find(selectors.upload);
		var data = new FormData();
		data.append('file', form.find('input[type="file"]')[0].files[0]);
		data.append('action', 'importKeywords');
		form.find('.btn').hide();
		form.find(selectors.uploading).show();
		
		$.ajax({
			url: script,
			data: data,
			processData: false,
			contentType: false,
			dataType: "json",
			type: 'POST',
			success: function(json) {
				form.find('.btn').show();
				form.find(selectors.uploading).hide();												
				modal.modal.find('textarea').val(json.items.join("\r\n"));
			}
		});
	}	    
    function createModal(btn){    	
		modal = new Modal({
			parent: container,
			static: true,
			title: "Create New Search",
			template: selectors.templates.create,			
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Cancel'),
				$('<a>').addClass('btn btn-primary').text('Create').click(function(){ create($(this)); })
			)
		});
    }
    function create(btn){
    	var form = modal.modal.find('form');    		
		if(!form.valid()) return;
		
		modal.block();
		btn.button('loading');
		$.post(script,form.serialize(),function(json){
			modal.release();
			btn.button('reset');
			if(json.error){ $.error(json.error); }
			else{ search(); modal.close(); }
		},"json");
    }
    
    function reloadItem(id){
    	var item = container.find(selectors.item).filter(function(){ return $(this).attr('data-id')==id; })
    	if(item.length<=0){ $.error("Item not found!"); return; }
    	
    	$.post(script,{action: 'loadSearch', id: id},function(json){
    		if(json.error){ $.error(json.error); }
    		else{
    			var html = Handlebars.compile(container.find(selectors.templates.item).html());
    			item.replaceWith(html(json.item));    			    			
    		}    		
    	},"json");
    }
    function search() {
    	var params = {};    	
    	params.pageLength = 10;
    	
        var itemTemplate = container.find(selectors.templates.item).html();
        var c = container.find(selectors.searches);
        var form = container.find(selectors.searchForm);
        
        ajaxDataTableInit(c,form,itemTemplate,script,params);        
    }    
    function remove(btn){
    	if(!confirm("Are you sure you want to delete this search?"))return;
    	   
    	btn.button('loading');
    	var id = btn.closest('tr').attr('data-id');
    	$.post(script,{action: 'removeSearch', id:id},function(json){
    		btn.button('reset');
    		if(json.error){ $.error(json.error); }
    		else{
    			btn.closest('tr').fadeOut(function(){ $(this).remove(); });
    		}
    		
    	},"json");
    }
});