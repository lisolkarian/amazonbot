$(document).ready(function () {
    var script = "ajax/php/products.php";
    var selectors = {
        container			: ".page-products",
		
        list				: '.products-list',
        add					: '.add-a',
        item				: '.products-item',        
        remove				: '.remove-a',
        title				: '.product-title',   
        
        salesrank			: '.salesrank-history-a',
        salesrankChart		: '.history-line-chart',
        salesrankForm		: '.history-search-form',
        
        rankRefresh			: '.rank-refresh-a',
                
        templates: {                    	
        	product			: '#template-products-item',   
        	create			: 'ajax/template/products/_create.phtml',
        	salesrankHistory: 'ajax/template/products/_salesrank_history.phtml',
        },
	}    
    var container = $(selectors.container);    
    var details;
    var modal;
    
    init();
    bind();

    function init() {    	
    	loadProducts();    	
    }

    function bind() {
		container.find(selectors.add).click(function(e){ e.preventDefault(); e.stopPropagation(); addDialog($(this)); });
		container.on('click',selectors.remove,function(e){ e.preventDefault(); e.stopPropagation(); remove($(this)); });		
		//container.on('click',selectors.item,function(e){ e.preventDefault(); loadProduct($(this)); });
		
		container.on('click',selectors.salesrank, function(e){ e.preventDefault(); salesrankHistory($(this)); });
		container.on('change',selectors.salesrankForm+' input', function(e){ e.preventDefault(); salesrankGetHistory(); });
		
		container.on('click',selectors.rankRefresh,function(e){ e.preventDefault(); refreshRank($(this)); });						
    }
    function salesrankHistory(btn){
    	var id = btn.closest(selectors.item).attr('data-id');
    	
    	modal = new Modal({
			parent: container,
			static: true,
			title: "SalesRank History",
			size: '80%',
			template: selectors.templates.salesrankHistory,
			templateData: {id: id},
			callback: function(){ 			
				setTimeout(function(){
					google.charts.load('current', {packages: ['corechart', 'line']});
					google.charts.setOnLoadCallback(salesrankGetHistory);
				},500);				
			},
			buttons: new Array(				
				$('<a>').addClass('btn btn-default dialog-close').text('Done')			
			)
		});
    }
    function salesrankGetHistory(){
    	var id = modal.id;
    	var form = container.find(selectors.salesrankForm);
    	
    	modal.loader();
    	$.post(script,form.serialize(),function(json){
    		modal.loader(false);
    		if(json.error){ $.error(json.error); }
    		else{
    			if(json.labels.length<=0){
    				container.find(selectors.salesrankChart).html('No data found');
    				return;
    			}
    			var data = new google.visualization.DataTable();
    			data.addColumn('date', 'X');    			
    			data.addColumn('number', 'Rank');    				

    			$.each(json.series,function(n,el){
    				el[0] = new Date(el[0]);    				    		
    				data.addRow(el);
    			});
    			
    			var options = {
    				hAxis: {title: 'Date'},
    				vAxis: {title: 'Rank'},    				
    				//colors: ['red','green']
    				series: { 
    			        0: {
    			        	pointSize: 3,     			        	    			        
    			            lineWidth: 2,    			                			           
    			        }
    			    }
    			};    		
    			var chart = new google.visualization.LineChart(container.find(selectors.salesrankChart)[0]);
    			//var chart = new google.visualization.ScatterChart(container.find(selectors.historyChart)[0]);    			
    			chart.draw(data, options);   
    		}
    	},"json");
    }  
    
    function refreshRank(btn){    	
    	var id = btn.closest(selectors.item).attr('data-id');
    	    	    	
    	btn.button('loading');
    	$.post(script,{action: 'updateSalesrank',id: id}, function(json){
    		btn.button('reset');
    		if(json.error){ $.error(json.error); }
    		else{
    			reloadItem(id);
    		}
    	},"json");    	
    }
    function reloadItem(id){
    	var el = container.find(selectors.item).filter(function(){ return $(this).attr('data-id')==id; });
    	if(el.length<=0) return;
    	
    	el.loading();
    	$.post(script,{action: 'loadProduct', id: id},function(json){
    		el.loading(false);
    		if(json.error){ $.error(json.error); }
    		else{    			    
    			
    			console.log(json.product);
    			
    			var html = Handlebars.compile(container.find(selectors.templates.product).html());               			        		
    			el.replaceWith(html(json.product));
    		}
    	},"json");
    }

    function loadProducts(){
    	$.post(script,{action: 'getProducts'}, function(json){
    		if(json.error){ $.error(json.error); }
    		else{
    			$.each(json.products,function(n,el){
    				var c = container.find(selectors.list);    				    				
        			var html = Handlebars.compile(container.find(selectors.templates.product).html());               			        		
        			c.append(html(el));
    			});    			
    		}    		
    	},"json");
    }
    function addDialog(){
    	modal = new Modal({
			parent: container,
			static: true,
			title: 'Add New Product',
			template: selectors.templates.create,			
			buttons: new Array(				
				$('<button>').addClass('btn btn-default dialog-close').text('Cancel'),
				$('<button>').addClass('btn btn-success').text('Add').click(function(e){e.preventDefault(); addProduct($(this)); })
			)
		});
    }
    function addProduct(btn){
    	var form = modal.modal.find('form');
    	if(!form.valid())return;
    	
    	modal.block();
    	btn.button('loading');
    	$.post(script,form.serialize(),function(json){
    		modal.release();
    		btn.button('reset');
    		if(json.error){ $.error(json.error); }
    		else{
    			window.location.reload();
    		}
    	},"json");    	
    }
    function remove(btn){
    	if(!confirm("Are you sure you want to delete this product?"))return;
    	   
    	btn.button('loading');
    	var id = btn.closest(selectors.item).attr('data-id');
    	$.post(script,{action: 'removeProduct', id:id},function(json){
    		btn.button('reset');
    		if(json.error){ $.error(json.error); }
    		else{
    			btn.closest(selectors.item).fadeOut(function(){ $(this).remove(); });
    		}
    		
    	},"json");
    }
});